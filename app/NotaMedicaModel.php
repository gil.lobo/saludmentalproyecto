<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NotaMedicaModel extends Model
{
    use SoftDeletes; //Implementamos
    protected $table = 'nota_medica';
    protected $dates = ['deleted_at']; //Registramos la nueva columna

    protected $fillable = [
      'user_id','user_id_registra','evolucion','resultados_lab','diagnostico','pronostico','indicaciones'
    ];

    public function scopeNotas($query, $idUser)
    {
      return $query->join('users', 'users.id', '=', 'nota_medica.user_id')
                    ->where('user_id_registra', '=', $idUser);
    }

    public function scopeNotasPac($query, $idUser)
    {
      return $query->join('users', 'users.id', '=', 'nota_medica.user_id')
                    ->where('user_id', '=', $idUser);
    }

    public function scopeNotasPac2($query, $idUser)
    {
      return $query->join('users', 'users.id', '=', 'nota_medica.user_id')
                    ->where('nota_medica.id', '=', $idUser);
    }

}
