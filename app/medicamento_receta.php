<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class medicamento_receta extends Model
{
      protected $table = 'medicamento_receta';

      protected $fillable = [
        'receta_id','cat_medicamentos_id','cindicaciones'
      ];

      public function scopeRecetado($query, $idUser)
      {
        return $query->join('cat_medicamentos', 'medicamento_receta.cat_medicamentos_id', '=', 'cat_medicamentos.id')
                      ->where('medicamento_receta.receta_id', '=', $idUser);
      }
}
