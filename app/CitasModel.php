<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class CitasModel extends Model
{
    use SoftDeletes; //Implementamos

      protected $table = 'citas';

      protected $dates = ['deleted_at']; //Registramos la nueva columna

      protected $fillable = [
          'user_id', 'user_id_agenda', 'fch_cita'
      ];

      public function usuarioAgendado() {
        return $this->hasOne('App\User', 'id', 'user_id_agenda'); // Le indicamos que se va relacionar con el atributo id
    }

    public function usuarioCitado() {
      return $this->hasOne('App\User', 'id', 'user_id'); // Le indicamos que se va relacionar con el atributo id
    }

    public function scopeCitasUsuario($query, $idUser)
    {
      return $query->join('users', 'users.id', '=', 'citas.user_id')
                    ->where('user_id_agenda', '=', $idUser)
                    ->whereDate('fch_cita', Carbon::now()->format('Y-m-d'));
    }

}
