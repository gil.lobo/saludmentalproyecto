<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PersonasModel extends Model
{
      use SoftDeletes; //Implementamos
      protected $table = 'personas';
     protected $dates = ['deleted_at']; //Registramos la nueva columna

     protected $fillable = [
         'name', 'email', 'avatar', 'password', 'paterno', 'materno', 'nombre', 'fch_nacimiento',
     ];
}
