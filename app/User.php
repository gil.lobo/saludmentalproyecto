<?php

namespace App;

use Caffeinated\Shinobi\Concerns\HasRolesAndPermissions;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Nicolaslopezj\Searchable\SearchableTrait;


use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable, HasRolesAndPermissions;
    use SearchableTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

     use SoftDeletes; //Implementamos

    protected $dates = ['deleted_at']; //Registramos la nueva columna

    protected $fillable = [
        'name', 'email', 'avatar', 'password', 'paterno', 'materno', 'nombre', 'fch_nacimiento',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
          'name' => 9,
            'paterno' => 10,
            'materno' => 10,
            'nombre' => 10,

        ],
    ];


    public function agenda()
    {
      return $this->belongsTo('App\configura_agenda');
    }
    public function citas()
    {
      return $this->hasMany('App\CitasModel', 'user_id_agendado', 'id');
    }
}
