<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cat_medicamentos extends Model
{
  protected $table = 'cat_medicamentos';

  // protected $dates = ['deleted_at']; //Registramos la nueva columna

  public function medica()
  {
      return $this->belongsToMany('App\RecetaModel');
  }
}
