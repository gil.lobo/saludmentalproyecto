<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Crypt;
use App\User;
use App\NotaMedicaModel;
use App\receta;
use DB;
use Illuminate\Http\Request;
use Elibyy\TCPDF\Facades\TCPDF;

class GeneradorController extends Controller
{
  public function imprimirNota($id){
      $id = Crypt::decrypt($id);
      $notas = NotaMedicaModel::select('name', 'user_id_registra', 'paterno', 'materno', 'nombre', 'nota_medica.id', 'nota_medica.created_at', 'nota_medica.evolucion', 'nota_medica.diagnostico', 'nota_medica.pronostico', 'nota_medica.indicaciones')->NotasPac2($idUser = $id)->first();
      $usuario = User::find($notas->user_id_registra);
       // dd($usuario);
        $view = \View::make('pdfs.ejemplo', compact('notas', 'usuario'));
        $html = $view->render();
        // dd($html);
        $pdf = new TCPDF();
        $pdf::SetTitle('Nota médica');
        $pdf::AddPage();
        $pdf::writeHTML($html, true, false, true, false, '');

        $pdf::Output('notaMedica.pdf', 'I');
        // $pdf::Output(public_path().'/downloads/nota.pdf', 'F');
       // $pdf = \PDF::loadView('pdfs.ejemplo', compact('notas', 'usuario'));
       // // $pdf->save(public_path().'/downloads/my_stored_file.pdf');
       // return $pdf->stream('ejemplo.pdf');
  }

  public function imprimirReceta($id){
      $id = Crypt::decrypt($id);
      $notas = receta::select('name', 'user_id_registra', 'paterno', 'materno', 'nombre', 'receta.id', 'receta.created_at', 'receta.diagnostico', 'receta.indicaciones')->RecetasPac2($idUser = $id)->first();
      $usuario = User::find($notas->user_id_registra);
      $medicamentos = receta::select(DB::raw("CONCAT(cnombre_medicamento, ' ', cpresentacion) AS cnombre_medicamento_1"), 'medicamento_receta.cindicaciones')->MedicamentosRecetados($idUser = $id)->get();
       // dd($usuario);
        $view = \View::make('pdfs.receta', compact('notas', 'usuario', 'medicamentos'));
        $html = $view->render();
        // dd($html);
        $pdf = new TCPDF();
        $pdf::SetTitle('Receta');
        $pdf::AddPage();
        $pdf::writeHTML($html, true, false, true, false, '');

        $pdf::Output('Receta.pdf', 'I');
        // $pdf::Output(public_path().'/downloads/nota.pdf', 'F');
       // $pdf = \PDF::loadView('pdfs.ejemplo', compact('notas', 'usuario'));
       // // $pdf->save(public_path().'/downloads/my_stored_file.pdf');
       // return $pdf->stream('ejemplo.pdf');
  }

  /**
   *
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function enviaEmalReceta(Request $request, $id)
  {
    $id = Crypt::decrypt($id);
    $notas = receta::select('email', 'paterno', 'materno', 'nombre', 'receta.id')->RecetasPac2($idUser = $id)->first();

    try {
      \Mail::to($notas->email)->send(new \App\Mail\EmergencyCallReceived($notas));
      return back()->with('info', 'La receta fué enviada con exito');
    } catch (\Exception $e) {
      return back()->with('error', 'Ocurrió un error intente nuevamente');
    }


  }

}
