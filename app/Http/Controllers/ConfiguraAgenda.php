<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{User, configura_agenda, agenda_dias};


class ConfiguraAgenda extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $configuracion = configura_agenda::select('configura_agenda.id', 'name', 'hora_inicio', 'hora_final')->join('users', 'users.id', '=', 'user_id')->get();
      // $tareas = $configuracion->user;;
        // dd($configuracion);
        return view('configuraAgenda.index', compact('configuracion'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dias = agenda_dias::pluck('dia', 'id');
         // dd($dias);
        $usuarios = User::pluck('name', 'id');
        return view('configuraAgenda.create', compact('usuarios', 'dias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $idUser = \Auth::user()->id;
        $Configuracion = configura_agenda::create([
            'user_id' => $request->user_id,
            'user_id_registra' => $idUser,
            'fch_inicio' => $request->fch_inicio,
            'fch_final' => $request->fch_final,
            'hora_inicio' => $request->hora_inicio,
            'hora_final' => $request->hora_final,
            'intervalo' => $request->intervalo,
            'cantidad' => $request->cantidad
          ]);
         // dd($Configuracion);
         $Configuracion->dias()->sync($request->get('dias'));
        return redirect()->route('configuraAgenda.edit', $Configuracion->id)
              ->with('info', 'Configuración guardada con exito');
    // dias
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $agenda = configura_agenda::select('configura_agenda.id', 'name', 'hora_inicio', 'hora_final', 'fch_inicio', 'fch_final', 'cantidad')->join('users', 'users.id', '=', 'user_id')->find($id);
        // $agenda = configura_agenda::find($id);
        return view('configuraAgenda.show', compact('agenda'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $usuarios = User::pluck('name', 'id');
      $agenda = configura_agenda::find($id);
      $dias = agenda_dias::pluck('dia', 'id');

      return view('configuraAgenda.edit', compact('agenda', 'dias', 'usuarios'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $Configuracion = configura_agenda::where('id', $id)->update([
         'fch_inicio' => $request->input('fch_inicio'),
         'fch_final' => $request->input('fch_final'),
         'hora_inicio' => $request->input('hora_inicio'),
         'hora_final' => $request->input('hora_final'),
         'intervalo' => $request->input('intervalo'),
         'cantidad' => $request->input('cantidad')
       ]);
     $Configuracion = configura_agenda::find($id);
       $Configuracion->dias()->sync($request->get('dias'));
      return redirect()->route('configuraAgenda.edit', $Configuracion->id)
            ->with('info', 'Configuración actualizada con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
