<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\User;
use App\NotaMedicaModel;
use App\CitasModel;
Use DB;

class NotaMedicaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $notas = NotaMedicaModel::select('name', 'paterno', 'materno', 'nombre', 'nota_medica.id', 'nota_medica.created_at')->Notas($idUser = \Auth::user()->id)->get();
      return view('notas.index', compact('notas'));
    }

    public function notaPaciente($id)
    {
      $paciente = User::find($id);

      $notas = NotaMedicaModel::select('name', 'paterno', 'materno', 'nombre', 'nota_medica.id', 'nota_medica.created_at')->NotasPac($idUser = $id)->get();
       // dd($paciente);
       return view('notas.notaspac', compact('notas', 'paciente'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

      $idCita = Crypt::decrypt($request->idpaciente);
      $idPaciente = CitasModel::select('user_id')->find($idCita);
      session(['idPaciente' => $idPaciente->user_id]);
      session(['idCita' => $idCita]);
      // dd(session('idPaciente'));
      $citado = User::find($idPaciente->user_id);
       // dd($citado);
      return view('notas.create', compact('citado'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createNotaPac(Request $request)
    {

      $idPaciente = Crypt::decrypt($request->idpaciente);
      // $idPaciente = CitasModel::select('user_id')->find($idCita);
      session(['idPaciente' => $idPaciente]);
      session()->forget('idCita');
      // dd(session('idPaciente'));
      $citado = User::find($idPaciente);
       // dd($citado);
      return view('notas.create', compact('citado'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $idUser = \Auth::user()->id;
      // dd(session());
      $idCitado = session( 'idPaciente' );
      // $idCita = session( 'idCita' );

      try {
        DB::beginTransaction();
        $Configuracion = NotaMedicaModel::create([
            'user_id' => $idCitado,
            'user_id_registra' => $idUser,
            'evolucion' => $request->evolucion,
            'diagnostico' => $request->diagnostico,
            'pronostico' => $request->pronostico,
            'indicaciones' => $request->indicaciones
          ]);
          if (session()->has('idCita')) {
            $idCita = session( 'idCita' );
            $Cita = CitasModel::where('id', $idCita)->update([
              'iatendido' => 1
            ]);
          }

          DB::commit();
          $idNota = Crypt::encrypt($Configuracion->id);
          return redirect()->route('nota.edit', $idNota)
                ->with('info', 'Nota guardada con exito');
      } catch (\Exception $e) {
        DB::rollBack();
        return back()->with('error', 'Ocurrió un error intente nuevamente');
      }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $id = Crypt::decrypt($id);
      $nota = NotaMedicaModel::find($id);
      return view('notas.show', compact('nota'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $nota = NotaMedicaModel::find(Crypt::decrypt($id));
      if (session('idPaciente')) {
        $idPaciente = session('idPaciente');
      }else {
        $idPaciente = $nota->user_id;
      }

      // dd(session('idPaciente'));
      $citado = User::find($idPaciente);

      // $dias = agenda_dias::pluck('dia', 'id');

      return view('notas.edit', compact('nota', 'citado'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      $Configuracion = NotaMedicaModel::where('id', $id)->update([
        'evolucion' => $request->input('evolucion'),
        'diagnostico' => $request->input('diagnostico'),
        'pronostico' => $request->input('pronostico'),
        'indicaciones' => $request->input('indicaciones')
      ]);
      // dd($Configuracion);
      $idNota = Crypt::encrypt($Configuracion);
      return redirect()->route('nota.edit', $idNota)
            ->with('info', 'Nota actualizada con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
