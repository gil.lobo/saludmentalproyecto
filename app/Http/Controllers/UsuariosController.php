<?php

namespace App\Http\Controllers;

use App\User;
use DB;
use Caffeinated\Shinobi\Models\Role;
use Illuminate\Http\Request;

class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $usuarios = User::get();
      // dd($roles);
       return view('usuarios.index', compact('usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $roles = Role::get();
      return view('usuarios.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      try {
        DB::beginTransaction();
        $usuario->create($request->all());

        $usuario->roles()->sync($request->get('roles'));
        DB::commit();
        return redirect()->route('usuarios.edit', $usuario->id)
              ->with('info', 'Usuario actualizado con exito');
      } catch (\Exception $e) {
        DB::rollBack();
        return back()->with('error', 'Ocurrió un error intente nuevamente');
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $usuario = User::find($id);
      return view('usuarios.show', compact('usuario'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $usuario = User::find($id);
      $roles = Role::get();
      // dd($usuario);
      return view('usuarios.edit', compact('usuario', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $usuario)
    {
      try {
        DB::beginTransaction();
        $usuario->update($request->all());

        $usuario->roles()->sync($request->get('roles'));
        DB::commit();
        return redirect()->route('usuarios.edit', $usuario->id)
              ->with('info', 'Usuario actualizado con exito');
      } catch (\Exception $e) {
        DB::rollBack();
        return back()->with('error', 'Ocurrió un error intente nuevamente');
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function search(Request $request)
    {
      $users = User::select(DB::raw("CONCAT(paterno, ' ', materno, ' ', nombre, ' usuario: ', name) AS text"), 'id')->search($request->q)->get();
      return \Response::json($users);
    }
  }
