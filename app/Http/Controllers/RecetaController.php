<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use DB;
use App\{User, receta, cat_medicamentos, medicamento_receta};

class RecetaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $receta = receta::select('name', 'paterno', 'materno', 'nombre', 'receta.id', 'receta.created_at')->Recetas($idUser = \Auth::user()->id)->get();
      return view('receta.index', compact('receta'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      $medicamentos = cat_medicamentos::select(DB::raw("CONCAT(cnombre_medicamento, ' ', cpresentacion) AS cnombre_medicamento_1"), 'id')->pluck('cnombre_medicamento_1','id');
      // dd($medicamentos);
      $idPaciente = Crypt::decrypt($request->idpaciente);
      session(['idPaciente' => $idPaciente]);
      // dd(session('idPaciente'));
      $citado = User::find($idPaciente);
      $recetado = medicamento_receta::select('cnombre_medicamento', 'cpresentacion', 'cat_medicamentos_id', 'cindicaciones')
                                      ->Recetado($idUser = 0)->get();

       // dd($citado);
      return view('receta.create', compact('citado', 'medicamentos', 'recetado'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $idUser = \Auth::user()->id;
      $idCitado = session( 'idPaciente' );
      // dd($idUser);
      try {
        DB::beginTransaction();
        $Configuracion = receta::create([
            'user_id' => $idCitado,
            'user_id_registra' => $idUser,
            'diagnostico' => $request->diagnostico,
            'indicaciones' => $request->indicaciones
          ]);
          $datos_a_insertar = array();

          /* añadimos los datos al array */

          foreach ($request->Indicacion as $key => $sport)
          {
              $datos_a_insertar[$key]['receta_id'] = $Configuracion->id;
              $datos_a_insertar[$key]['cat_medicamentos_id'] = $request->medicamentos[$key];
              $datos_a_insertar[$key]['cindicaciones'] = $sport;

          }
          // dd($datos_a_insertar);

         /* declaramos el array */
         medicamento_receta::insert($datos_a_insertar);
         // $Configuracion->medicamentos()->attach($request->get('medicamentos'), ['cindicaciones' => $request->get('Indicacion')]);

         $Configuracion->medicamentos()->sync($request->get('medicamentos'));

          DB::commit();
          $idNota = Crypt::encrypt($Configuracion->id);
          return redirect()->route('receta.edit', $idNota)
               ->with('info', 'Receta guardada con exito');
      } catch (\Exception $e) {
        DB::rollBack();
        return back()->with('error', 'Ocurrió un error intente nuevamente');
      }


       // $medicamentos = cat_medicamentos::select(DB::raw("CONCAT(cnombre_medicamento, ' ', cpresentacion) AS cnombre_medicamento_1"))->MedicamentosRecetados('cnombre_medicamento_1','id');
       // $nombre = "Edgar Roberto Arteaga Hinojosa";

       // \Mail::to('beto.87.05.05@gmail.com')->send(new \App\Mail\EmergencyCallReceived($nombre));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $id = Crypt::decrypt($id);
      $nota = receta::find($id);
      $medicamentos = receta::select(DB::raw("CONCAT(cnombre_medicamento, ' ', cpresentacion) AS cnombre_medicamento_1"))->MedicamentosRecetados($idUser = $id)->get();
      // dd($medicamentos);
      return view('receta.show', compact('nota', 'medicamentos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $idPaciente = session('idPaciente');
      $medicamentos = cat_medicamentos::select(DB::raw("CONCAT(cnombre_medicamento, ' ', cpresentacion) AS cnombre_medicamento_1"), 'id')->pluck('cnombre_medicamento_1','id');
      $citado = User::find($idPaciente);
      $receta = receta::find(Crypt::decrypt($id));
      $recetado = medicamento_receta::select('cnombre_medicamento', 'cpresentacion', 'cat_medicamentos_id', 'cindicaciones')
                                      ->Recetado($idUser = Crypt::decrypt($id))->get();
      return view('receta.edit', compact('receta', 'citado', 'medicamentos', 'recetado'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      try {
        DB::beginTransaction();

        $Configuracion = receta::where('id', $id)->update([
          'diagnostico' => $request->input('diagnostico'),
          'indicaciones' => $request->input('indicaciones')
        ]);
        $Configuracion = receta::find($id);

        // dd($datos_a_insertar);

         /* declaramos el array */
         $Configuracion->medicamentos()->sync($request->get('medicamentos'));
         $medica = medicamento_receta::select('id')->where('receta_id', $id)->get();
         // dd($medica);
         /* añadimos los datos al array */
         $i = 0;
         foreach ($medica as $key)
         {
              // dd($sport);
             $Configuracion = medicamento_receta::where('id', $key->id)->update([
               'cindicaciones' => $request->Indicacion[$i]
             ]);
             $i++;
         }
         DB::commit();
         $idNota = Crypt::encrypt($Configuracion->id);
         return redirect()->route('receta.edit', $idNota)
              ->with('info', 'Configuración guardada con exito');
      } catch (\Exception $e) {
        DB::rollBack();
        return back()->with('error', 'Ocurrió un error intente nuevamente');
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



}
