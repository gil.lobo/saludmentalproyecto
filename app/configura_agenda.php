<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class configura_agenda extends Model
{
  protected $table = 'configura_agenda';

  protected $fillable = [
    'user_id','user_id_registra','fch_inicio','fch_final','hora_inicio','hora_final','intervalo','cantidad'
  ];
  public function user()
  {
    return $this->belongsTo(User::class);
  }

  public function dias()
  {
      return $this->belongsToMany('App\agenda_dias', 'dias_agenda');
  }
}
