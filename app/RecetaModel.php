<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RecetaModel extends Model
{
  use SoftDeletes; //Implementamos
  protected $table = 'receta';
  protected $dates = ['deleted_at']; //Registramos la nueva columna

  protected $fillable = [
    'user_id','user_id_registra','diagnostico','indicaciones'
  ];


  public function medicamentos()
  {
      return $this->belongsToMany('App\cat_medicamentos', 'medicamento_receta');
  }

  public function scopeRecetas($query, $idUser)
  {
    return $query->join('users', 'users.id', '=', 'receta.user_id')
                  ->where('user_id_registra', '=', $idUser);
  }

  public function scopeRecetasPaciente($query, $idUser)
  {
    return $query->join('users', 'users.id', '=', 'receta.user_id')
                  ->where('user_id', '=', $idUser);
  }
}
