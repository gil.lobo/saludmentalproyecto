<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class MedicamentosModel extends Model
{
  // use SoftDeletes; //Implementamos

    protected $table = 'cat_medicamentos';

    // protected $dates = ['deleted_at']; //Registramos la nueva columna

    public function medica()
    {
        return $this->belongsToMany('App\RecetaModel');
    }


}
