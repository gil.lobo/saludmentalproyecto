<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmergencyCallReceived extends Mailable
{
    use Queueable, SerializesModels;
    public $notas;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($notas)
    {
        $this->notas = $notas;
        // dd($nombre);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      return $this->subject('Receta')->view("mails/receta");
    }
}
