<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class agenda_dias extends Model
{
  protected $table = 'agenda_dias';

  public function links()
  {
      return $this->belongsToMany('App\configura_agenda');
  }
}
