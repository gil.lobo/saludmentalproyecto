<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiasAgendaModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dias_agenda', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->unsignedBigInteger('configura_agenda_id')->index();
          $table->foreign('configura_agenda_id')->references('id')->on('configura_agenda')->onDelete('cascade');
          $table->unsignedBigInteger('agenda_dias_id')->index();
          $table->foreign('agenda_dias_id')->references('id')->on('agenda_dias')->onDelete('cascade');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dias_agenda');
    }
}
