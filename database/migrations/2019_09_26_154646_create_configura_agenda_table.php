<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfiguraAgendaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configura_agenda', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('user_id')->index();
            $table->unsignedInteger('user_id_registra')->index();
            $table->date('fch_inicio');
            $table->date('fch_final');
            $table->time('hora_inicio');
            $table->time('hora_final');
            $table->integer('intervalo');
            $table->integer('cantidad');
            $table->softDeletes(); //Nueva línea, para el borrado lógico
            $table->timestamps();

            $table->foreign('user_id')
                  ->references('id')->on('users')
                  ->onDelet('cascade');

            $table->foreign('user_id_registra')
                  ->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configura_agenda');
    }
}
