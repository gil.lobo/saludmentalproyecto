<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NotaMedica extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('nota_medica', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('user_id')->index();
            $table->unsignedInteger('user_id_registra')->index();
            $table->text('evolucion');
            $table->text('resultados_lab')->nullable();
            $table->string('diagnostico');
            $table->string('pronostico');
            $table->text('indicaciones');
            $table->softDeletes(); //Nueva línea, para el borrado lógico
            $table->timestamps();
            $table->foreign('user_id')
                  ->references('id')->on('users')
                  ->onDelet('cascade');
            $table->foreign('user_id_registra')
                  ->references('id')->on('users')
                  ->onDelet('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::dropIfExists('nota_medica');
    }
}
