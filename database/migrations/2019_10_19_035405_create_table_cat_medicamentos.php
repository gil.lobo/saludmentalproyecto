<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableCatMedicamentos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cat_medicamentos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('cclave');
            $table->text('cnombre_medicamento');
            $table->text('cpresentacion');
            $table->integer('iactivo');
          //  $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cat_medicamentos');
    }
}
