<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableMedicamentoReceta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicamento_receta', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('receta_id')->index();
            $table->unsignedInteger('cat_medicamentos_id')->index();
            $table->timestamps();
            $table->softDeletes(); //Nueva línea, para el borrado lógico
            $table->foreign('receta_id')
                  ->references('id')->on('receta')
                  ->onDelet('cascade');
            $table->foreign('cat_medicamentos_id')
                  ->references('id')->on('cat_medicamentos')
                  ->onDelet('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medicamento_receta');
    }
}
