<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableCitas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('citas', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->unsignedInteger('user_id')->index();
          $table->unsignedInteger('user_id_agenda')->index();
          $table->dateTime('fch_cita');
          $table->string('codigo', 7);
          $table->integer('iatendido')->default(0);
          $table->softDeletes(); //Nueva línea, para el borrado lógico
          $table->timestamps();
          $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelet('cascade');

          $table->foreign('user_id_agenda')
                ->references('id')->on('users')
                ->onDelet('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('citas');
    }
}
