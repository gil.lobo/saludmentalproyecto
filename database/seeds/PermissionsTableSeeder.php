<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create([
          'name' => 'Navegar Usuarios',
          'slug' => 'users.index',
          'description' => 'Lista y navega todos los usuarios del sistema',

        ]);
        Permission::create([
          'name' => 'ver detalle de Usuarios',
          'slug' => 'users.show',
          'description' => 'Ver detalle de todos los usuarios del sistema',

        ]);
        Permission::create([
          'name' => 'Editar Usuarios',
          'slug' => 'users.edit',
          'description' => 'Edita todos los usuarios del sistema',

        ]);
        Permission::create([
          'name' => 'Eliminar Usuarios',
          'slug' => 'users.destroy',
          'description' => 'Eliminar todos los usuarios del sistema',

        ]);

        // Roles
        Permission::create([
          'name' => 'Navegar Roles',
          'slug' => 'roles.index',
          'description' => 'Lista y navega todos los roles del sistema',

        ]);
        Permission::create([
          'name' => 'ver detalle de Roles',
          'slug' => 'roles.show',
          'description' => 'Ver detalle de todos los roles del sistema',

        ]);
        Permission::create([
          'name' => 'Editar Roles',
          'slug' => 'roles.edit',
          'description' => 'Edita todos los roles del sistema',

        ]);
        Permission::create([
          'name' => 'Crear Roles',
          'slug' => 'roles.create',
          'description' => 'Crea todos los roles del sistema',

        ]);
        Permission::create([
          'name' => 'Eliminar Roles',
          'slug' => 'roles.destroy',
          'description' => 'Eliminar todos los roles del sistema',

        ]);

        // Agenda
        Permission::create([
          'name' => 'Navegar Configuración de agenda',
          'slug' => 'configuraAgenda.index',
          'description' => 'Lista y navega todas las Configuraciones de agenda del sistema',

        ]);
        Permission::create([
          'name' => 'ver detalle de Configuración de agenda',
          'slug' => 'configuraAgenda.show',
          'description' => 'Ver detalle de todas las Configuraciones de agenda del sistema',

        ]);
        Permission::create([
          'name' => 'Editar Configuración de agenda',
          'slug' => 'configuraAgenda.edit',
          'description' => 'Crea todas las Configuraciones de agenda del sistema',

        ]);
        Permission::create([
          'name' => 'Crear Configuración de agenda',
          'slug' => 'configuraAgenda.create',
          'description' => 'Edita todas las Configuraciones de agenda del sistema',

        ]);
        Permission::create([
          'name' => 'Eliminar Configuración de agenda',
          'slug' => 'configuraAgenda.destroy',
          'description' => 'Eliminar todas las Configuraciones de agenda del sistema',

        ]);

        // Citas
        Permission::create([
          'name' => 'Navegar Citas',
          'slug' => 'citas.index',
          'description' => 'Lista y navega todos las citas del sistema',

        ]);
        Permission::create([
          'name' => 'ver detalle de Citas',
          'slug' => 'citas.show',
          'description' => 'Ver detalle de las citas del sistema',

        ]);
        Permission::create([
          'name' => 'Editar Citas',
          'slug' => 'citas.edit',
          'description' => 'Edita las citas del sistema',

        ]);
        Permission::create([
          'name' => 'Registrar Citas',
          'slug' => 'citas.create',
          'description' => 'Registra citas en el sistema',

        ]);
        // Permission::create([
        //   'name' => 'Eliminar Roles',
        //   'slug' => 'roles.destroy',
        //   'description' => 'Eliminar todos los roles del sistema',
        //
        // ]);

        // notas
        Permission::create([
          'name' => 'Navegar Notas',
          'slug' => 'nota.index',
          'description' => 'Lista y navega las notas del sistema',

        ]);
        Permission::create([
          'name' => 'ver detalle de Notas',
          'slug' => 'nota.show',
          'description' => 'Ver detalle de las notas del sistema',

        ]);
        Permission::create([
          'name' => 'Editar Notas',
          'slug' => 'nota.edit',
          'description' => 'Edita las notas del sistema',

        ]);
        Permission::create([
          'name' => 'Registrar Notas',
          'slug' => 'nota.create',
          'description' => 'Registra notas en el sistema',

        ]);
        // Permission::create([
        //   'name' => 'Eliminar Notas',
        //   'slug' => 'nota.destroy',
        //   'description' => 'Eliminar notas del sistema',
        //
        // ]);

        // Receta
        Permission::create([
          'name' => 'Navegar Recetas',
          'slug' => 'receta.index',
          'description' => 'Lista y navega las recetas del sistema',

        ]);
        Permission::create([
          'name' => 'ver detalle de Receta',
          'slug' => 'receta.show',
          'description' => 'Ver detalle de las receta del sistema',

        ]);
        Permission::create([
          'name' => 'Editar Receta',
          'slug' => 'receta.edit',
          'description' => 'Edita las receta del sistema',

        ]);
        Permission::create([
          'name' => 'Registrar Receta',
          'slug' => 'receta.create',
          'description' => 'Registra receta en el sistema',

        ]);
        // Permission::create([
        //   'name' => 'Eliminar Notas',
        //   'slug' => 'nota.destroy',
        //   'description' => 'Eliminar notas del sistema',
        //
        // ]);
    }
}
