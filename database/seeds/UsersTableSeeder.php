<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Role;
use App\User;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $usuario = User::create([
        // 'name' => $idUser,
        'paterno' => 'Ramos',
        'materno' => 'Benítez',
        'nombre' => 'Dra. Alma Delia',
        'email' => 'dra.ramosb@gmail.com',
        'email_verified_at'=> Carbon::now(),
        'password'=> Hash::make('secret'),
        // 'fch_nacimiento'=> ''
      ]);
      $usuario->roles()->sync(3);

      $usuario = User::create([
        // 'name' => $idUser,
        'paterno' => 'Juárez',
        'materno' => 'González',
        'nombre' => 'Dr. Eric Jonathan',
        'email' => 'ejuarez.innn@gmail.com',
        'email_verified_at'=> Carbon::now(),
        'password'=> Hash::make('secret'),
        // 'fch_nacimiento'=> ''
      ]);
      $usuario->roles()->sync(3);

      $usuario = User::create([
        // 'name' => $idUser,
        'paterno' => 'González',
        'materno' => 'Pulido',
        'nombre' => 'Dra. Alma Liliana',
        'email' => 'gopa780426@hotmail.com',
        'email_verified_at'=> Carbon::now(),
        'password'=> Hash::make('secret'),
        // 'fch_nacimiento'=> ''
      ]);
      $usuario->roles()->sync(3);

      $usuario = User::create([
        // 'name' => $idUser,
        'paterno' => 'Castañeda',
        'materno' => 'González',
        'nombre' => 'Dr. Héctor Octavio',
        'email' => 'castaneda.hector@gmail.com',
        'email_verified_at'=> Carbon::now(),
        'password'=> Hash::make('secret'),
        // 'fch_nacimiento'=> ''
      ]);
      $usuario->roles()->sync(3);

      $usuario = User::create([
        // 'name' => $idUser,
        'paterno' => 'Ángeles',
        'materno' => 'González',
        'nombre' => 'Psic. Javier',
        'email' => 'javo_angeles@hotmail.com',
        'email_verified_at'=> Carbon::now(),
        'password'=> Hash::make('secret'),
        // 'fch_nacimiento'=> ''
      ]);
      $usuario->roles()->sync(3);

      $usuario = User::create([
        // 'name' => $idUser,
        'paterno' => 'Contreras',
        'materno' => 'Torres',
        'nombre' => 'Pisc. José Luis',
        'email' => 'psicjlcontreras@hotmail.com',
        'email_verified_at'=> Carbon::now(),
        'password'=> Hash::make('secret'),
        // 'fch_nacimiento'=> ''
      ]);
      $usuario->roles()->sync(3);

      $usuario = User::create([
        // 'name' => $idUser,
        'paterno' => 'García',
        'materno' => 'Victoria',
        'nombre' => 'LCH Rocío',
        'email' => 'rocio.gvictoria@gmail.com',
        'email_verified_at'=> Carbon::now(),
        'password'=> Hash::make('secret'),
        // 'fch_nacimiento'=> ''
      ]);
      $usuario->roles()->sync(3);

      $usuario = User::create([
        // 'name' => $idUser,
        'paterno' => 'Hernández',
        'materno' => 'González',
        'nombre' => 'Psic. Sofía ',
        'email' => 'npsic.sofia.hdez.glez@gmail.com',
        'email_verified_at'=> Carbon::now(),
        'password'=> Hash::make('secret'),
        // 'fch_nacimiento'=> ''
      ]);
      $usuario->roles()->sync(3);

      $usuario = User::create([
        // 'name' => $idUser,
        'paterno' => 'López',
        'materno' => 'Jiménez',
        'nombre' => 'Psic. Adriana',
        'email' => 'adriana_38@hotmail.com',
        'email_verified_at'=> Carbon::now(),
        'password'=> Hash::make('secret'),
        // 'fch_nacimiento'=> ''
      ]);
      $usuario->roles()->sync(3);

      $usuario = User::create([
        // 'name' => $idUser,
        'paterno' => 'Palacios',
        'materno' => 'Bustamante',
        'nombre' => 'Psic. Alejandra',
        'email' => 'a.palacios.bustamante@gmail.com',
        'email_verified_at'=> Carbon::now(),
        'password'=> Hash::make('secret'),
        // 'fch_nacimiento'=> ''
      ]);
      $usuario->roles()->sync(3);

      $usuario = User::create([
        // 'name' => $idUser,
        'paterno' => 'Pérez',
        'materno' => 'Carrillo',
        'nombre' => 'Psic Paula Catalina',
        'email' => 'psic.paukat@gmail.com',
        'email_verified_at'=> Carbon::now(),
        'password'=> Hash::make('secret'),
        // 'fch_nacimiento'=> ''
      ]);
      $usuario->roles()->sync(3);

      $usuario = User::create([
        // 'name' => $idUser,
        'paterno' => 'Pérez',
        'materno' => 'Vázquez',
        'nombre' => 'Mtra. Elvia',
        'email' => 'elvia_abich@hotmail.com',
        'email_verified_at'=> Carbon::now(),
        'password'=> Hash::make('secret'),
        // 'fch_nacimiento'=> ''
      ]);
      $usuario->roles()->sync(3);

      $usuario = User::create([
        // 'name' => $idUser,
        'paterno' => 'Reyna',
        'materno' => 'González',
        'nombre' => 'Psic Olga Gabriela',
        'email' => 'sicologia_olga@yahoo.com.mx',
        'email_verified_at'=> Carbon::now(),
        'password'=> Hash::make('secret'),
        // 'fch_nacimiento'=> ''
      ]);
      $usuario->roles()->sync(3);

      $usuario = User::create([
        // 'name' => $idUser,
        'paterno' => 'Román',
        'materno' => 'Toledo',
        'nombre' => 'Psic. Lizette América',
        'email' => 'lizetteromanssm@gmail.com',
        'email_verified_at'=> Carbon::now(),
        'password'=> Hash::make('secret'),
        // 'fch_nacimiento'=> ''
      ]);
      $usuario->roles()->sync(3);

      $usuario = User::create([
        // 'name' => $idUser,
        'paterno' => 'Ruíz',
        'materno' => 'Bedolla',
        'nombre' => 'Psic Fabiola',
        'email' => 'fabyrube@hotmail.com',
        'email_verified_at'=> Carbon::now(),
        'password'=> Hash::make('secret'),
        // 'fch_nacimiento'=> ''
      ]);
      $usuario->roles()->sync(3);

      $usuario = User::create([
        // 'name' => $idUser,
        'paterno' => 'Ruíz',
        'materno' => 'Pérez',
        'nombre' => 'Psic. Ruth',
        'email' => 'violetha01@hotmail.com',
        'email_verified_at'=> Carbon::now(),
        'password'=> Hash::make('secret'),
        // 'fch_nacimiento'=> ''
      ]);
      $usuario->roles()->sync(3);

      $usuario = User::create([
        // 'name' => $idUser,
        'paterno' => 'Vargas',
        'materno' => 'Gama',
        'nombre' => 'Psic. Nancy Verónica',
        'email' => 'psicotf@yahoo.com.mx',
        'email_verified_at'=> Carbon::now(),
        'password'=> Hash::make('secret'),
        // 'fch_nacimiento'=> ''
      ]);
      $usuario->roles()->sync(3);

      $usuario = User::create([
        // 'name' => $idUser,
        'paterno' => 'Salgado',
        'materno' => 'Moctezuma',
        'nombre' => 'Nutr. Saori Guadalupe',
        'email' => 'saori.salgado21@gmail.com',
        'email_verified_at'=> Carbon::now(),
        'password'=> Hash::make('secret'),
        // 'fch_nacimiento'=> ''
      ]);
      $usuario->roles()->sync(3);

      $usuario = User::create([
        // 'name' => $idUser,
        'paterno' => 'Dávila',
        'materno' => 'Sosa',
        'nombre' => 'Psic. Jaime Antonio',
        'email' => 'sosa01_j@yahoo.com.mx',
        'email_verified_at'=> Carbon::now(),
        'password'=> Hash::make('secret'),
        // 'fch_nacimiento'=> ''
      ]);
      $usuario->roles()->sync(3);

      $usuario = User::create([
        // 'name' => $idUser,
        'paterno' => 'Sánchez',
        'materno' => 'Ávila',
        'nombre' => 'Blanca Luz',
        'email' => 'luzblanca_sanchez@hotmail.com',
        'email_verified_at'=> Carbon::now(),
        'password'=> Hash::make('secret'),
        // 'fch_nacimiento'=> ''
      ]);
      $usuario->roles()->sync(1);

      $usuario = User::create([
        // 'name' => $idUser,
        'paterno' => 'Dávila',
        'materno' => 'Sánchez',
        'nombre' => 'Blanca Areli',
        'email' => 'davilaareli@gmail.com',
        'email_verified_at'=> Carbon::now(),
        'password'=> Hash::make('secret'),
        // 'fch_nacimiento'=> ''
      ]);
      $usuario->roles()->sync(1);

      $usuario = User::create([
        // 'name' => $idUser,
        'paterno' => 'López',
        'materno' => 'Aguirre',
        'nombre' => 'Gilberto',
        'email' => 'beto.87.05.05@gmail.com',
        'email_verified_at'=> Carbon::now(),
        'password'=> Hash::make('secret'),
        // 'fch_nacimiento'=> ''
      ]);
      $usuario->roles()->sync(1);

      $usuario = User::create([
        // 'name' => $idUser,
        'paterno' => 'Arteaga',
        'materno' => 'Hinojosa',
        'nombre' => 'Edgar Roberto',
        'email' => 'edgar.arteaga619@gmail.com',
        'email_verified_at'=> Carbon::now(),
        'password'=> Hash::make('secret'),
        // 'fch_nacimiento'=> ''
      ]);
      $usuario->roles()->sync(1);

      $usuario = User::create([
        'name' => 'López Aguirre Gilberto',
        'paterno' => 'López',
        'materno' => 'Aguirre',
        'nombre' => 'Gilberto',
        'email' => 'gil.lopez0915@gmail.com',
        'email_verified_at'=> Carbon::now(),
        'password'=> Hash::make('secret'),
        // 'fch_nacimiento'=> ''
      ]);
      $usuario->roles()->sync(2);
    }
}
