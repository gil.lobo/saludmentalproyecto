<?php

use Illuminate\Database\Seeder;

use App\agenda_dias;
class DiasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      agenda_dias::create([
        'dia' => 'Lunes',
      ]);
      agenda_dias::create([
        'dia' => 'Martes',
      ]);
      agenda_dias::create([
        'dia' => 'Miércoles',
      ]);
      agenda_dias::create([
        'dia' => 'Jueves',
      ]);
      agenda_dias::create([
        'dia' => 'Viernes',
      ]);
      agenda_dias::create([
        'dia' => 'Sábado',
      ]);
      agenda_dias::create([
        'dia' => 'Domingo',
      ]);
    }
}
