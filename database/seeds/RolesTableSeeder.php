<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Role::create([
        'name' => 'Admin',
        'slug' => 'admin',
        'special' => 'all-access',

      ]);

      Role::create([
        'name' => 'Paciente',
        'slug' => 'paciente',

      ]);

      Role::create([
        'name' => 'Psicólogo(a)',
        'slug' => 'psicologo',

      ]);

      Role::create([
        'name' => 'Psiquiatra',
        'slug' => 'psiquiatra',

      ]);

      Role::create([
        'name' => 'Neuropsicólogo(a)',
        'slug' => 'neuropsicologa',

      ]);

      Role::create([
        'name' => 'Comunicación  Humana',
        'slug' => 'comunicacion  humana',

      ]);
    }
}
