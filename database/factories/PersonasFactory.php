<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\PersonasModel;
use Faker\Generator as Faker;

$factory->define(PersonasModel::class, function (Faker $faker) {
    return [
      'name' => $faker->name,
      'email' => $faker->unique()->safeEmail,
      'email_verified_at' => now(),
      'password' => Hash::make('secret'), // password

    ];
});
