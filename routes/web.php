<?php
use Illuminate\Routing\RouteFileRegistrar;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('mental-hoy.index');
});

Route::get('/adicciones', function () {
    return view('mental-hoy.adicciones');
})->name('adicciones');

Route::get('/adolescentes', function () {
    return view('mental-hoy.adolescentes');
})->name('adolescentes');
Route::get('/about', function () {
    return view('mental-hoy.about');
});
Route::get('/adriana', function () {
    return view('mental-hoy.adriana');
})->name('adriana');
Route::get('/eric', function () {
    return view('mental-hoy.eric');
})->name('eric');

Route::get('/javier', function () {
    return view('mental-hoy.javier');
})->name('javier');
Route::get('/jose_luis', function () {
    return view('mental-hoy.jose_luis');
})->name('jose_luis');

Route::get('/somos', function () {
    return view('mental-hoy.somos');
})->name('somos');
Route::get('/servicios', function () {
    return view('mental-hoy.servicios');
})->name('servicios');
Route::get('/como_funciona', function () {
    return view('mental-hoy.como_funciona');
})->name('como_funciona');
Route::get('/preguntas', function () {
    return view('mental-hoy.preguntas');
})->name('preguntas');
Route::get('/blog', function () {
    return view('mental-hoy.blog');
})->name('blog');

Route::get('/adolescentes', function () {
    return view('mental-hoy.adolescentes');
})->name('adolescentes');
Route::get('/america', function () {
    return view('mental-hoy.america');
})->name('america');
Route::get('/ruth', function () {
    return view('mental-hoy.ruth');
})->name('ruth');
Route::get('/liliana', function () {
    return view('mental-hoy.liliana');
})->name('liliana');
Route::get('/paula', function () {
    return view('mental-hoy.paula');
})->name('paula');
Route::get('/nancy', function () {
    return view('mental-hoy.nancy');
})->name('nancy');

Route::get('/adulto', function () {
    return view('mental-hoy.adulto');
})->name('adulto');
Route::get('/hector', function () {
    return view('mental-hoy.hector');
})->name('hector');
Route::get('/sofia', function () {
    return view('mental-hoy.sofia');
})->name('sofia');


Route::get('/alma', function () {
    return view('mental-hoy.alma');
})->name('alma');
Route::get('/jaime', function () {
    return view('mental-hoy.jaime');
})->name('jaime');
Route::get('/elvia', function () {
    return view('mental-hoy.elvia');
})->name('elvia');
Route::get('/saori', function () {
    return view('mental-hoy.saori');
})->name('saori');

Route::get('/alejandra', function () {
    return view('mental-hoy.alejandra');
})->name('alejandra');
Route::get('/rocio', function () {
    return view('mental-hoy.rocio');
})->name('rocio');


Route::get('/olga', function () {
    return view('mental-hoy.olga');
})->name('olga');
Route::get('/fabiola', function () {
    return view('mental-hoy.fabiola');
})->name('fabiola');

Route::get('/tdah', function () {
    return view('mental-hoy.tdah');
})->name('tdah');
Route::get('/desarrollo_inf', function () {
    return view('mental-hoy.desarrollo_inf');
})->name('desarrollo_inf');
Route::get('/emociones', function () {
    return view('mental-hoy.emociones');
})->name('emociones');
Route::get('/alimenticios', function () {
    return view('mental-hoy.alimenticios');
})->name('alimenticios');
Route::get('/esquizofrenia', function () {
    return view('mental-hoy.esquizofrenia');
})->name('esquizofrenia');
Route::get('/enlace', function () {
    return view('mental-hoy.enlace');
})->name('enlace');
Route::get('/auditiva', function () {
    return view('mental-hoy.auditiva');
})->name('auditiva');
Route::get('/familiar', function () {
    return view('mental-hoy.familiar');
})->name('familiar');

Route::get('/pago', function () {
    return view('mental-hoy.pago');
})->name('pago');
 Route::get('/instrucciones', function () {
     return view('mental-hoy.instrucciones');
 })->name('instrucciones');


Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');
// Route::get('/instrucciones', 'HomeControllerPaciente@index')->name('instrucciones');
Route::middleware(['auth'])->group(function(){
  // Roles
  Route::resource('configuraAgenda', 'ConfiguraAgenda');
  Route::get('Videollamada', 'VideollamadaController@index')->name('Videollamada.index');
  Route::get('Videollamada/saveURL', 'VideollamadaController@saveURL')->name('Videollamada.saveURL');
  Route::post('roles/store', 'RoleController@store')
          ->name('roles.store')
          ->middleware('can:roles.create');

  Route::get('roles', 'RoleController@index')
          ->name('roles.index')
          ->middleware('can:roles.index');

  Route::get('roles/create', 'RoleController@create')
          ->name('roles.create')
          ->middleware('can:roles.create');

  Route::put('roles/{role}', 'RoleController@update')
          ->name('roles.update')
          ->middleware('can:roles.edit');

  Route::get('roles/{role}', 'RoleController@show')
          ->name('roles.show')
          ->middleware('can:roles.show');

  Route::delete('roles/{store}', 'RoleController@destroy')
          ->name('roles.destroy')
          ->middleware('can:roles.destroy');

  Route::get('roles/{role}/edit', 'RoleController@edit')
          ->name('roles.edit')
          ->middleware('can:roles.edit');
          // Usuarios
  Route::post('usuarios/store', 'UsuariosController@store')
          ->name('usuarios.store')
          ->middleware('can:usuarios.edit');

  Route::get('usuarios', 'UsuariosController@index')
          ->name('usuarios.index')
          ->middleware('can:usuarios.index');

  Route::get('usuarios/create', 'UsuariosController@create')
          ->name('usuarios.create')
          ->middleware('can:usuarios.create');

  Route::put('usuarios/{usuario}', 'UsuariosController@update')
          ->name('usuarios.update')
          ->middleware('can:usuarios.update');

  Route::get('usuarios/{usuario}', 'UsuariosController@show')
          ->name('usuarios.show')
          ->middleware('can:usuarios.show');

  Route::delete('usuarios/{store}', 'UsuariosController@destroy')
          ->name('usuarios.destroy')
          ->middleware('can:usuarios.destroy');

  Route::get('usuarios/{usuario}/edit', 'UsuariosController@edit')
          ->name('usuarios.edit')
          ->middleware('can:usuarios.edit');

  Route::get('search', 'UsuariosController@search')
          ->name('search.search');
  // Permisos
  Route::post('permisos/store', 'UsuariosController@store')
          ->name('permisos.store')
          ->middleware('can:permisos.edit');

  Route::get('permisos', 'UsuariosController@index')
          ->name('permisos.index')
          ->middleware('can:permisos.index');

  Route::get('permisos/create', 'UsuariosController@create')
          ->name('permisos.create')
          ->middleware('can:permisos.create');

  Route::put('permisos/{usuario}', 'UsuariosController@update')
          ->name('permisos.update')
          ->middleware('can:permisos.update');

  Route::get('permisos/{usuario}', 'UsuariosController@show')
          ->name('permisos.show')
          ->middleware('can:permisos.show');

  Route::delete('permisos/{store}', 'UsuariosController@destroy')
          ->name('permisos.destroy')
          ->middleware('can:permisos.destroy');

  Route::get('permisos/{usuario}/edit', 'UsuariosController@edit')
          ->name('permisos.edit')
          ->middleware('can:permisos.edit');
          // citas
  // Route::resource('pacientes', 'pacientes');
  Route::get('pacientes', 'pacientes@index')
          ->name('pacientes.index')
          ->middleware('can:citas.index');

  Route::get('nota', 'NotaMedicaController@index')
          ->name('nota.index')
          ->middleware('can:nota.index');

  Route::get('nota/{idpaciente}', 'NotaMedicaController@create')
          ->name('nota.create')
          ->middleware('can:nota.create');

  Route::get('nota/{idpaciente}/create', 'NotaMedicaController@createNotaPac')
          ->name('nota.createNotaPac')
          ->middleware('can:nota.create');

  Route::post('nota/store', 'NotaMedicaController@store')
          ->name('nota.store')
          ->middleware('can:nota.create');

  Route::get('nota/{nota}/edit', 'NotaMedicaController@edit')
          ->name('nota.edit')
          ->middleware('can:nota.edit');

  Route::put('nota/{nota}', 'NotaMedicaController@update')
          ->name('nota.update')
          ->middleware('can:nota.edit');

  Route::get('nota/{nota}/muestra', 'NotaMedicaController@notaPaciente')
          ->name('nota.muestra')
          ->middleware('can:nota.index');

  Route::get('nota/{nota}/show', 'NotaMedicaController@show')
          ->name('nota.show')
          ->middleware('can:nota.show');

  Route::get('receta', 'RecetaController@index')
          ->name('receta.index')
          ->middleware('can:receta.index');

  Route::get('receta/{idpaciente}', 'RecetaController@create')
          ->name('receta.create')
          ->middleware('can:receta.create');

  Route::post('receta/store', 'RecetaController@store')
          ->name('receta.store')
          ->middleware('can:receta.create');

  Route::get('receta/{receta}/edit', 'RecetaController@edit')
          ->name('receta.edit')
          ->middleware('can:receta.edit');

  Route::put('receta/{receta}', 'RecetaController@update')
          ->name('receta.update')
          ->middleware('can:receta.edit');

  Route::get('receta/{receta}/show', 'RecetaController@show')
          ->name('receta.show')
          ->middleware('can:receta.show');

  Route::get('/imprimir/{nota}', 'GeneradorController@imprimirNota')
          ->name('imprimir.imprimirNota');

  Route::get('/imprimir/{nota}/receta', 'GeneradorController@imprimirReceta')
          ->name('imprimir.imprimirReceta');

  Route::post('/envia-email/{receta}/receta', 'GeneradorController@enviaEmalReceta')
          ->name('envia-email.enviaEmalReceta');

});


Route::get('calendario', function () {
    return view('calendario.index');
});

Route::get('/register/verify/{code}', 'Auth\RegisterController@verify');

Route::get('auth/{provider}', 'Auth\SocialAuthController@redirectToProvider')->name('social.auth');
Route::get('auth/{provider}/callback', 'Auth\SocialAuthController@handleProviderCallback');
