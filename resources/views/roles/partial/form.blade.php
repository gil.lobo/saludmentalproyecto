<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      {{ Form:: label('name', 'Nombre')}}
      {{ Form:: text('name', null, ['class' => 'form-control'])}}
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      {{ Form:: label('slug', 'URL Amigable')}}
      {{ Form:: text('slug', null, ['class' => 'form-control'])}}
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {{ Form:: label('description', 'Descripción')}}
      {{ Form:: textarea('description', null, ['rows' => 4, 'cols' => 50,'class' => 'form-control'])}}
    </div>
  </div>
</div>
<hr>
<h3>Permiso Especial</h3>
<div class="row">
  <div class="col-md-4">
    <div class="form-group">

      <label>{{ Form::radio('special', 'all-access')}} Acceso Total</label>
      <label>{{ Form::radio('special', 'no-access')}} Ningún Acceso</label>
    </div>
  </div>
</div>
<hr>
<h3>Lista de permisos</h3>
<div class="row">
  <div class="form-group">
    <ul class="list-unstyled">
      @foreach ($permissions as $permission)
        <li>
          <label>
            {{Form::checkbox('permissions[]', $permission->id, null)}}
            {{$permission->name}}
            <em>{{$permission->description ?: 'Sin Descripción'}}</em>
          </label>
        </li>
      @endforeach
    </ul>
  </div>
</div>
<div class="row">
  <div class="form-group">
    {{Form::submit('Guardar', ['class' => 'btn btn-primary'])}}
  </div>
</div>
