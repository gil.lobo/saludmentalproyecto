@extends('admin.layout')
@push('stylesheets')
  <link rel="stylesheet" href="{{url('adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
@endpush
@section('header-text')
  <h1>Roles</h1>
@endsection
@section('main')

  <div class="col-lg-12">
    <div class="card card-info card-outline">
      <div class="card-header">
        <h5 class="m-0">Rol</h5>
      </div>
      <div class="card-body">
        <p><strong>Nombre: </strong> {{$role->name}}</p>
        <p><strong>Slug: </strong> {{$role->slug}}</p>
        <p><strong>Descripción: </strong> {{$role->description}}</p>


      </div>
    </div>

  </div>
@endsection
