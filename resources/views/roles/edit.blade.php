@extends('admin.layout')

@section('header-text')
  <h1>Roles</h1>
@endsection
@section('main')

  <div class="col-lg-12">
    <div class="card card-info card-outline">
      <div class="card-header">
        <h5 class="m-0">Rol</h5>
      </div>
      <div class="card-body">
        {!!
          Form::model(
            $role,
            [
              'route' => [
                'roles.update',
                 $role->id,

               ],
               'method' => 'put'
            ]
          )
        !!}
        @include('roles.partial.form')
        {!!
          Form::close()
        !!}
      </div>
    </div>

  </div>
@endsection
