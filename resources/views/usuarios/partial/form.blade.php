<div class="row">
  <div class="col-md-3">
    <div class="form-group">
      {{ Form:: label('name', 'Usuario')}}
      {{ Form:: text('name', null, ['class' => 'form-control'])}}
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      {{ Form:: label('paterno', 'Apellido Paterno')}}
      {{ Form:: text('paterno', null, ['class' => 'form-control'])}}
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      {{ Form:: label('materno', 'Apellido Materno')}}
      {{ Form:: text('materno', null, ['class' => 'form-control'])}}
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      {{ Form:: label('nombre', 'Nombre')}}
      {{ Form:: text('nombre', null, ['class' => 'form-control'])}}
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-3">
    <div class="form-group">
      {{ Form:: label('fch_nacimiento', 'Fecha de Nacimiento')}}
      {{ Form:: text('fch_nacimiento', null, ['class' => 'form-control'])}}
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      {{ Form:: label('email', 'Correo Electrónico')}}
      {{ Form:: text('password', null, ['class' => 'form-control'])}}
    </div>
  </div>
  <div class="col-md-4">
    {{ Form:: label('password', 'Contraseña')}}
    <div class="input-group">
      {{ Form::password('password', array('id' => 'txtPassword', 'class' => 'form-control', 'autocomplete' => 'off')) }}
      <div class="input-group-append">
        <button id="show_password" class="btn btn-primary" type="button" onclick="mostrarPassword()"> <span class="fa fa-eye-slash icon"></span> </button>
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">


    </div>
  </div>
</div>
<hr>
<h3>Lista de permisos</h3>
<div class="row">
  <div class="form-group">
    <ul class="list-unstyled">
      @foreach ($roles as $rol)
        <li>
          <label>
            {{Form::checkbox('roles[]', $rol->id, null)}}
            {{$rol->name}}
            <em>{{$rol->description ?: 'Sin Descripción'}}</em>
          </label>
        </li>
      @endforeach
    </ul>
  </div>
</div>
<div class="row">
  <div class="form-group">
    {{Form::submit('Guardar', ['class' => 'btn btn-primary'])}}
  </div>
</div>
