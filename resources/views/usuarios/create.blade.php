@extends('admin.layout')
@push('stylesheets')
  <link rel="stylesheet" href="{{url('adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
@endpush
@section('header-text')
  <h1>Roles</h1>
@endsection
@section('main')

  <div class="col-lg-12">
    <div class="card card-info card-outline">
      <div class="card-header">
        <h5 class="m-0">Rol</h5>
      </div>
      <div class="card-body">
        {!!
          Form::open([
            'route' => [
              'usuarios.store'
            ]
          ])
        !!}
        @include('usuarios.partial.form')
        {!!
          Form::close()
        !!}
      </div>
    </div>

  </div>
@endsection
@section('script_page')
<script type="text/javascript">
  // $("#example1").DataTable();
  function mostrarPassword(){
		var cambio = document.getElementById("txtPassword");
		if(cambio.type == "password"){
			cambio.type = "text";
			$('.icon').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
		}else{
			cambio.type = "password";
			$('.icon').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
		}
	}

	$(document).ready(function () {
	//CheckBox mostrar contraseña
	$('#ShowPassword').click(function () {
		$('#password').attr('type', $(this).is(':checked') ? 'text' : 'password');
	});
});
</script>
@endsection
