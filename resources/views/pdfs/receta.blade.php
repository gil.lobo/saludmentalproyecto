  {{-- <head> --}}
    <style>
    p {
      text-align: justify;
      text-justify: inter-word;
    }
			.firma {border-top:1px solid black;
					text-align: center;
			}
		</style>

    <div class="container-fluid">

      <table width="100%">
        <tr>
          <td ><img src="adminlte/img/saludmentalhoy.PNG" alt="60"></td>
          <td align="center"><p><b>Receta de Salud Mental</b></p></td>
          <td align="right"><img src="adminlte/img/saludmentalhoy.PNG" alt="60"></td>
        </tr>
      </table>
      <p><b>Fecha de la registro: </b>{{$notas->created_at}}</p>
      <label ><b>Paciente: </b>{{$notas->paterno}} {{$notas->materno}} {{$notas->nombre}}</label>
      <br>
      <br>
      <table >
        <tr>
          <td><b>Diagnóstico</b></td>
        </tr>
        <tr border="1">
          <td ><p>{{$notas->diagnostico}}</p></td>
        </tr>
      </table>

      <p><b>Medicamentos</b></p>
      <ol>
        @foreach ($medicamentos as $element)
          <li>
            {{$element->cnombre_medicamento_1}} {{$element->cindicaciones}}
          </li>
        @endforeach
      </ol>
      <table >
        <tr>
          <td><b>Indicaciones médicas</b></td>
        </tr>

        <tr border="1">
          <td ><p>{{$notas->indicaciones}}</p></td>
        </tr>
      </table>

      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size:10px;">

    <tr>
      <td ></td>
      <td class="firma" >Nombre, Firma y Cédula del Médico Tratante</td>
      <td ></td>

    </tr>
    </div>
