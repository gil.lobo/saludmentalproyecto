@extends('admin.layout')
@push('stylesheets')
  <link rel="stylesheet" href="{{url('adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
@endpush
@section('header-text')
  <h1>Nota Médica</h1>
@endsection
@section('main')

  <div class="col-lg-12">
    <div class="card card-info card-outline">
      <div class="card-header">
        <h5 class="m-0">Nota</h5>
      </div>
      <div class="card-body">
        <p><strong>evolucion: </strong> {{$nota->evolucion}}</p>
        <p><strong>Diagnóstico: </strong> {{$nota->diagnostico}}</p>
        <p><strong>Pronostico: </strong> {{$nota->pronostico}}</p>
        <p><strong>Indicaciones Médicas: </strong> {{$nota->indicaciones}}</p>

      </div>
    </div>

  </div>
@endsection
