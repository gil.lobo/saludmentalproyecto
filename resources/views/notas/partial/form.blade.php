<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      {{ Form:: label('pronostico', 'Motivo de consulta')}}
      {{ Form:: text('pronostico', null, ['class' => 'form-control'])}}
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      {{ Form:: label('diagnostico', 'Diagnóstico')}}
      {{ Form:: text('diagnostico', null, ['class' => 'form-control'])}}
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="form-group">

      {{ Form:: label('evolucion', 'Descripción o evolución del cuadro clínico')}}
      {{ Form:: textarea('evolucion', null, [ 'rows' => 4, 'cols' => 50, 'class' => 'form-control'])}}
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {{ Form:: label('indicaciones', 'Indicaciones Médicas')}}
      {{ Form:: textarea('indicaciones', null, [ 'rows' => 4, 'cols' => 50, 'class' => 'form-control'])}}
    </div>
  </div>
</div>
<div class="row">
  <div class="form-group">
    {{Form::submit('Guardar', ['class' => 'btn btn-primary'])}}
  </div>
</div>
