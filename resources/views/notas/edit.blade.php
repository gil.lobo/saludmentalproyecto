@extends('admin.layout')

@section('header-text')
  <h1>Nota Médica</h1>
@endsection
@section('main')

  <div class="col-lg-12">
    <div class="card card-info card-outline">
      <div class="card-header">
        <h5 class="m-0">Paciente {{ $citado->name }}</h5>
      </div>
      <div class="card-body">
        {!!
          Form::model(
            $nota,
            [
              'route' => [
                'nota.update',
                 $nota->id
               ],
               'id' => 'customerform',
               'method' => 'put'
            ]
          )
        !!}
        {{-- <a href="{!! route('roles.create') !!}" class="btn btn-success">Crear Nuevo Rol</a> --}}
        @include('notas.partial.form')
        {!!
          Form::close()
        !!}
      </div>
    </div>

  </div>
@endsection
@section('script_page')
<script type="text/javascript">
$(function () {
  // $("#example1").DataTable();
  if ($("#customerform").length > 0) {
      $("#customerform").validate({
      rules: {
        pronostico: {
          required: true,
           // maxlength: 50
        },

      diagnostico: {
          required: true,
          // maxlength:12,
      },
      evolucion: {
              required: true,
              maxlength: 23,
          },
      indicaciones: {
              required: true,
              maxlength: 15,

          },
      },
      messages: {

        pronostico: {
          required: "Por favor ingrese el motivo de consulta",
          // maxlength: "Your last name maxlength should be 50 characters long."
        },
        diagnostico: {
          required: "Por favor ingrese el diagnóstico de consulta",
          // maxlength: "Your last name maxlength should be 50 characters long."
        },
        evolucion: {
          required: "Por favor ingrese la descripció o evaluación de la consulta",
          maxlength: "Se necesitan mínimo 23 caracteres para continuar"
        },
        indicaciones: {
          required: "Por favor ingrese sus indicaciones",
          maxlength: "Se necesitan mínimo 15 caracteres para continuar"
        },

      },
      })
    }
});
</script>
@endsection
