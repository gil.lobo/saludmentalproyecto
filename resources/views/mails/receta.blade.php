<html lang="es">
<head>
    <meta charset="utf-8">
</head>
<body>
    <h2>Hola {{ $notas->paterno }} {{ $notas->materno }} {{ $notas->nombre }}, gracias por confiar en <strong>Salud Mental Hoy</strong> !</h2>
    <p>Tu receta está a la mano.</p>
    <p>Para ello simplemente debes hacer click en el siguiente enlace:</p>

    <a href="{!! url('/imprimir/'.Crypt::encrypt($notas->id).'/receta') !!}">
        Descargar Receta
    </a>
</body>
</html>
