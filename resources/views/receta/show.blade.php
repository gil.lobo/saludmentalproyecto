@extends('admin.layout')
@push('stylesheets')
  <link rel="stylesheet" href="{{url('adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
@endpush
@section('header-text')
  <h1>Receta</h1>
@endsection
@section('main')

  <div class="col-lg-12">
    <div class="card card-info card-outline">
      <div class="card-header">
        <h5 class="m-0">Receta</h5>
      </div>
      <div class="card-body">
        <p><strong>Diagnóstico: </strong> {{$nota->diagnostico}}</p>
        <p><b>Medicamentos</b></p>
        <ol>
          @foreach ($medicamentos as $element)
            <li>
              {{$element->cnombre_medicamento_1}}
            </li>
          @endforeach
        </ol>

        <p><strong>Descripción: </strong> {{$nota->indicaciones}}</p>


      </div>
    </div>

  </div>
@endsection
