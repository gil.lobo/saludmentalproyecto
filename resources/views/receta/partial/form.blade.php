<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {{ Form:: label('diagnostico', 'Diagnóstico')}}
      {{ Form:: text('diagnostico', null, ['class' => 'form-control'])}}
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {{ Form:: label('medicamentos', 'Seleccione los medicamentos')}}
      {{Form::select('medicamentos[]',$medicamentos,null, ['id' => 'medicamentos', 'class' => ['selectpicker', 'form-control'], 'data-live-search' => 'true', 'multiple' => true])}}
      </div>
  </div>
</div>
<div class="row" id="r">
  @if(!$recetado->isEmpty())
    @foreach ($recetado as $element)
      <br>
      <div class="col-md-12">
        <div class="form-group" id="Medica{{$element->cat_medicamentos_id}}">
          <label for="ticket-name" class=" control-label">{{$element->cnombre_medicamento}} {{$element->cpresentacion}}</label>
          <br>
          <label class="col-sm-4 control-label">Indicaciones(Dosis, Via, hora.)</label>
          <input type="text" class="form-control" name="Indicacion[]" value="{{$element->cindicaciones}}"/>
        </div>
      </div>
      @endforeach
  
  @endif

</div>
<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {{ Form:: label('indicaciones', 'Indicaciones Médicas')}}
      {{ Form:: textarea('indicaciones', null, [ 'rows' => 4, 'cols' => 50, 'class' => 'form-control'])}}
    </div>
  </div>
</div>
<div class="row">
  <div class="form-group">
    {{Form::submit('Guardar', ['class' => 'btn btn-primary'])}}
  </div>
</div>
