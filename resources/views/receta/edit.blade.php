@extends('admin.layout')
@push('stylesheets')
  <link rel="stylesheet" href="{{url('adminlte/plugins/selectpicker/css/bootstrap-select.min.css')}}">
@endpush
@section('header-text')
  <h1>Actualizar Receta</h1>
@endsection
@section('main')

  <div class="col-lg-12">
    <div class="card card-info card-outline">
      <div class="card-header">
        <h5 class="m-0">Paciente <b>{{ $citado->name }}</b></h5>
      </div>
      <div class="card-body">

        {!!
          Form::model(
            $receta,
            [
              'route' => [
                'receta.update',
                 $receta->id,

               ],
               'method' => 'put'
            ]
          )
        !!}
        {{-- <a href="{!! route('roles.create') !!}" class="btn btn-success">Crear Nuevo Rol</a> --}}
        @include('receta.partial.form')
        {!!
          Form::close()
        !!}
        {!!
          Form::open([
            'route' => [
              'envia-email.enviaEmalReceta',
              Crypt::encrypt($receta->id)
            ],
            'method' => 'POST'
          ])
        !!}
        <button type="submit" class="btn btn-danger btn-sm">Enviar por email</button>
        {!!
          Form::close()
        !!}
      </div>
    </div>

  </div>
@endsection
@push('scripts')
  <script src="{{url('adminlte/plugins/selectpicker/js/bootstrap-select.min.js')}}"></script>
@endpush
@section('script_page')
<script type="text/javascript">
$(function () {
  // $("#example1").DataTable();
  $('#medicamentos').selectpicker();

  $("select").on("changed.bs.select", function(e, clickedIndex, newValue, oldValue) {
    if (newValue == true) {
      let indice = clickedIndex+1;
      // console.log(clickedIndex);
      let text = $("#medicamentos option[value='"+indice+"']").text();
      contenedor = $("#r"); //ID del contenedor
      $(contenedor).append('<br><div class="col-md-12"><div class="form-group" id="Medica' + indice + '"><label for="ticket-name" class=" control-label">' + text + '</label><br><label class="col-sm-4 control-label">Indicaciones(Dosis, Via, hora.)</label><input type="text" class="form-control" name="Indicacion[]"/></div></div>');

    } else if (newValue == false) {
       let indice = clickedIndex+1;
      let eliminaDiv = '#Medica' + indice;

      $(eliminaDiv).remove().remove();

    }
  });

  if ($("#customerform").length > 0) {
      $("#customerform").validate({
      rules: {
        diagnostico: {
          required: true,
           // maxlength: 50
        },

      // medicamentos: {
      //     required: true,
      //     // maxlength:12,
      // },

      indicaciones: {
              required: true,
              minlength: 15,

          },
      },
      messages: {

        diagnostico: {
          required: "Por favor ingrese el motivo de consulta",
          // maxlength: "Your last name maxlength should be 50 characters long."
        },
        // medicamentos: {
        //   required: "Por favor ingrese el diagnóstico de consulta",
        //   // maxlength: "Your last name maxlength should be 50 characters long."
        // },

        indicaciones: {
          required: "Por favor ingrese sus indicaciones",
          minlength: "Se necesitan mínimo 15 caracteres para continuar"
        },

      },
      })
    }
});
</script>
@endsection
