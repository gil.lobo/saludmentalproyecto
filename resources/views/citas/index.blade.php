
@extends('admin.layout')
@push('stylesheets')
  <link rel="stylesheet" href="{{url('adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
  <link rel="stylesheet" href="{{url('adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.css')}}">
@endpush
@section('header-text')
  <h1>Citas</h1>
@endsection
@section('main')

  <div class="col-lg-12">
    <div class="card card-info card-outline">
      <div class="card-header">
        <h5 class="m-0">Pacientes Citados</h5>
      </div>
      <div class="card-body">
         {{-- @can ('roles.create')
          <a href="{!! route('usuarios.create') !!}" class="btn btn-success">Crear Nuevo Usuario</a>
         @endcan --}}

        <hr>
        <table id="example2" class="table table-bordered table-hover display" style="width:100%">
                <thead>
                <tr>
                  <th>Pacientes</th>
                  <th>Fecha y hora</th>
                  <th>&nbsp;</th>
                  <th>&nbsp;</th>
                  <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($citas as $element)
                  <tr>

                    <td>{{$element->name}}</td>
                    <td>{{$element->fch_cita}}</td>
                    <td>
                        @can ('nota.create')
                        <a href="{!! route('nota.create', Crypt::encrypt($element->id_cita) ) !!}" class="btn btn-info btn-sm">Realizar nota</a></td>
                       @endcan
                      </td>
                    <td>
                      <a href="{!!url('Videollamada#'.$element->codigo)!!}" class="btn btn-primary btn-sm">Hacer Videollamada</a>
                    </td>
                      <td>
                        @can ('receta.create')
                          <a href="{!! route('receta.create', Crypt::encrypt($element->id) ) !!}" class="btn btn-success btn-sm">Realizar receta</a></td>
                        @endcan

                                               {{-- @can ('roles.destroy')
                        {!!
                          Form::open([
                            'route' => [
                              'usuarios.destroy',
                              $element->id
                            ],
                            'method' => 'DELETE'
                          ])
                        !!}
                        <button type="submit" class="btn btn-danger btn-sm">Eliminar</button>
                        {!!
                          Form::close()
                        !!}
                       @endcan --}}
                    </td>
                  </tr>
                @endforeach

                </tbody>

              </table>
      </div>
    </div>

  </div>
@endsection
@push('scripts')
  <script src="{{url('adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
  <script src="{{url('adminlte/plugins/datatables-responsive/js/dataTables.responsive.js')}}"></script>
  <script src="{{url('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
  <script src="{{url('adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.js')}}"></script>
@endpush
@section('script_page')
<script type="text/javascript">
$(function () {
  // $("#example1").DataTable();
  $('#example2').DataTable({
    "responsive": true,
    "paging": true,
    "lengthChange": false,
    "searching": false,
    "ordering": true,
    "info": true,
    "autoWidth": false,
    "language":{
      "info": "Total&nbsp;de páginas _PAGE_ de _PAGES_",
      "emptyTable": "Sin registros",
      "paginate": {
        "first": "Primera",
        "last": "Última",
        "next": "Siguiente",
        "previous": "Anterior",
        "infoFiltered": "(filtrados de _MAX_ total encontrados)",

        }
      }
  });
});
</script>
@endsection
