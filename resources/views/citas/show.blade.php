@extends('admin.layout')
@push('stylesheets')
  <link rel="stylesheet" href="{{url('adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
@endpush
@section('header-text')
  <h1>Usuarios</h1>
@endsection
@section('main')

  <div class="col-lg-12">
    <div class="card card-info card-outline">
      <div class="card-header">
        <h5 class="m-0">Usuario</h5>
      </div>
      <div class="card-body">
        <p><strong>Nombre: </strong> {{$usuario->name}}</p>
        <p><strong>Correo: </strong> {{$usuario->email}}</p>
      </div>
    </div>

  </div>
@endsection
