@extends('admin.layout')

@section('header-text')
  <h1>Usuaro</h1>
@endsection
@section('main')

  <div class="col-lg-12">
    <div class="card card-info card-outline">
      <div class="card-header">
        <h5 class="m-0">Usuario</h5>
      </div>
      <div class="card-body">
        {!!
          Form::model(
            $usuario,
            [
              'route' => [
                'usuarios.update',
                 $usuario->id,

               ],
               'method' => 'put'
            ]
          )
        !!}
        @include('usuarios.partial.form')
        {!!
          Form::close()
        !!}
      </div>
    </div>

  </div>
@endsection
