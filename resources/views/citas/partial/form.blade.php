<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {{ Form:: label('name', 'Nombre')}}
      {{ Form:: text('name', null, ['class' => 'form-control'])}}
    </div>
  </div>
</div>
<hr>
<h3>Lista de permisos</h3>
<div class="row">
  <div class="form-group">
    <ul class="list-unstyled">
      @foreach ($roles as $rol)
        <li>
          <label>
            {{Form::checkbox('roles[]', $rol->id, null)}}
            {{$rol->name}}
            <em>{{$rol->description ?: 'Sin Descripción'}}</em>
          </label>
        </li>
      @endforeach
    </ul>
  </div>
</div>
<div class="row">
  <div class="form-group">
    {{Form::submit('Guardar', ['class' => 'btn btn-primary'])}}
  </div>
</div>
