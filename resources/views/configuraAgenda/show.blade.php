@extends('admin.layout')
@section('header-text')
  <h1>Agenda</h1>
@endsection
@section('main')

  <div class="col-lg-12">
    <div class="card card-info card-outline">
      <div class="card-header">
        <h5 class="m-0">Agenda</h5>
      </div>
      <div class="card-body">
        <p><strong>Usuario: </strong> {{$agenda->name}}</p>
        <p><strong>Horario: </strong> {{$agenda->hora_inicio}} - {{$agenda->hora_final}}</p>
        <p><strong>Intervalo de tiempo: </strong> De: {{$agenda->fch_inicio}} A: {{$agenda->fch_final}}</p>
        <p><strong>Cabtidad de consultas: </strong> {{$agenda->cantidad}}</p>
      </div>
    </div>

  </div>
@endsection
