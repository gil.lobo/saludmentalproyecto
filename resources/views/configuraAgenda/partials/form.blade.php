<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {{ Form:: label('user_id', 'Usuario a configurar')}}
      {{Form::select('user_id',$usuarios, null, ['class' => 'form-control','placeholder' => 'Seleccione'])}}
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-3">
    <div class="form-group">
      {{ Form:: label('fch_inicio', 'Fecha de Inicio')}}
      {{ Form:: text('fch_inicio', null, ['class' => ['form-control', 'fch'], 'placeholder'=>'00/00/0000'])}}
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      {{ Form:: label('fch_final', 'Fecha de Final')}}
      {{ Form:: text('fch_final', null, ['class' => ['form-control', 'fch'], 'placeholder'=>'00/00/0000'])}}
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      {{ Form:: label('hora_inicio', 'Hora de Inicio')}}
      {{ Form:: text('hora_inicio', null, ['class' => ['form-control', 'hr'], 'placeholder'=>'00:00'])}}
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      {{ Form:: label('hora_final', 'Hora Final')}}
      {{ Form:: text('hora_final', null, ['class' => ['form-control', 'hr'], 'placeholder'=>'00:00'])}}
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-3">
    <div class="form-group">
      {{ Form:: label('intervalo', 'Intervalo por consulta')}}
      {{Form::select('intervalo',
        [
          '1' => '30 minutos',
          '2' => '35 minutos',
          '3' => '40 minutos',
          '4' => '45 minutos',
          '5' => '50 minutos'
        ], null, ['class' => 'form-control','placeholder' => 'Seleccione'])}}
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      {{ Form:: label('cantidad', 'Cantidad de consultas por días')}}
      {{ Form:: text('cantidad', null, ['class' => 'form-control'])}}
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      {{ Form:: label('dias', 'Seleccione los días en que estará disponible la agenda')}}
      {{Form::select('dias[]',$dias,null, ['class' => ['selectpicker', 'form-control'],  'multiple' => true, 'title' => 'Seleccione Días'])}}
      </div>
  </div>
</div>
<div class="row">
  <div class="form-group">
    {{Form::submit('Guardar', ['class' => 'btn btn-primary'])}}
  </div>
</div>
