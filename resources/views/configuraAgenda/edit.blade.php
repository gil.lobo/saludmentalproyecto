@extends('admin.layout')
@push('stylesheets')
  <link rel="stylesheet" href="{{url('adminlte/plugins/selectpicker/css/bootstrap-select.min.css')}}">
@endpush
@section('header-text')
  <h1>Configuración de Agenda</h1>
@endsection
@section('main')

  <div class="col-lg-12">
    <div class="card card-info card-outline">
      <div class="card-header">
        <h5 class="m-0">Agenda</h5>
      </div>
      <div class="card-body">
        {!!
          Form::model(
            $agenda,
            [
              'route' => [
                'configuraAgenda.update',
                 $agenda->id,

               ],
               'method' => 'put'
            ]
          )
        !!}
        @include('configuraAgenda.partials.form')
        {!!
          Form::close()
        !!}
      </div>
    </div>

  </div>
@endsection
@push('scripts')

  <script src="{{url('adminlte/plugins/selectpicker/js/bootstrap-select.min.js')}}"></script>

@endpush
@section('script_page')
<script type="text/javascript">
$(function () {
  // $("#example1").DataTable();
  $("#select2").selectpicker('render');
});

</script>
@endsection
