@extends('admin.layout')
@push('stylesheets')
  <link rel="stylesheet" href="{{url('adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
  <link rel="stylesheet" href="{{url('adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.css')}}">
@endpush
@section('header-text')
  <h1>Agenda</h1>
@endsection
@section('main')

  <div class="col-lg-12">
    <div class="card card-info card-outline">
      <div class="card-header">
        <h5 class="m-0">Configurar Agenda</h5>
      </div>
      <div class="card-body">
        <a href="{!! route('configuraAgenda.create') !!}" class="btn btn-success">Nueva Agenda</a>
        <hr>
        <table id="example2" class="table table-bordered table-hover display" style="width:100%">
                <thead>
                <tr>
                  <th>Usuario</th>
                  <th>Horario</th>
                  <th></th>
                  <th></th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($configuracion as $element)
                    <tr>
                      <td>{{$element->name}}</td>
                      <td>{{$element->hora_inicio}} - {{$element->hora_final}}
                      </td>
                      <td>
                        {{-- @can ('roles.show') --}}
                          <a href="{!! route('configuraAgenda.show', $element->id) !!}" class="btn btn-info btn-sm">Ver</a>
                        {{-- @endcan --}}
                      </td>
                      <td>

                        {{-- @can ('roles.edit') --}}
                          <a href="{!! route('configuraAgenda.edit', $element->id) !!}" class="btn btn-primary btn-sm">Editar</a>
                        {{-- @endcan --}}
                        </td>
                        <td>

                        {{-- @can ('roles.destroy') --}}
                          {!!
                            Form::open([
                              'route' => [
                                'configuraAgenda.destroy',
                                $element->id
                              ],
                              'method' => 'DELETE'
                            ])
                          !!}
                          <button type="submit" class="btn btn-danger btn-sm">Eliminar</button>
                          {!!
                            Form::close()
                          !!}
                        {{-- @endcan --}}
                      </td>

                    </tr>
                  @endforeach



                </tbody>

              </table>
      </div>
    </div>

  </div>
@endsection
@push('scripts')

  <script src="{{url('adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
  <script src="{{url('adminlte/plugins/datatables-responsive/js/dataTables.responsive.js')}}"></script>
  <script src="{{url('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
  <script src="{{url('adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.js')}}"></script>
@endpush
@section('script_page')
<script type="text/javascript">
$(function () {
  // $("#example1").DataTable();
  $('#example2').DataTable({
    "responsive": true,
    "paging": true,
    "lengthChange": false,
    "searching": false,
    "ordering": true,
    "info": true,
    "autoWidth": false,
    "language":{
      "info": "Total&nbsp;de páginas _PAGE_ de _PAGES_",
      "emptyTable": "Sin Registros",
      "infoEmpty":      "Nada que mostrar",
      "paginate": {
        "first": "Primera",
        "last": "Última",
        "next": "Siguiente",
        "previous": "Anterior",
        "infoFiltered": "(filtrados de _MAX_ total encontrados)",

        }
      }
  });
});
$('#mySelect2').trigger('change');
</script>
@endsection
