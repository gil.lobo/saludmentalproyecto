@extends('admin.layout')
@push('stylesheets')
  <!-- Select2 -->
  <link rel="stylesheet" href="{{url('adminlte/plugins/selectpicker/css/bootstrap-select.min.css')}}">

@endpush

@section('header-text')
  <h1>Configurar Agenda</h1>
@endsection
@section('main')

  <!-- /.col-md-6 -->
  <div class="col-lg-12">
    <div class="card">
      <div class="card-header">
        <h5 class="m-0">Configuración</h5>
      </div>
      <div class="card-body">
        {!!
          Form::open([
            'route' => [
              'configuraAgenda.store'
            ]
          ])
        !!}
        @include('configuraAgenda.partials.form')
        {!!
          Form::close()
        !!}

      </div>
    </div>
  </div>
@endsection
@push('scripts')
  <script src="{{url('adminlte/plugins/jquery-mask/jquery.mask.js')}}"></script>
  <script src="{{url('adminlte/plugins/selectpicker/js/bootstrap-select.min.js')}}"></script>
@endpush

@section('script_page')
<script type="text/javascript">
$('.fch').mask('00/00/0000');
$('.hr').mask('00:00');
// $('#timepicker2').datetimepicker({
//   viewMode: 'days',
//   format: 'DD/MM/YYYY'
// });
// $('#timepicker3').datetimepicker({
//   format: 'LT'
// });
// $('#timepicker4').datetimepicker({
//   format: 'LT'
// });

</script>
@endsection
