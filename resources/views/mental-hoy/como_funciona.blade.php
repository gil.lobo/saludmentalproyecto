<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Etrain</title>
    <link rel="icon" href="adminlte/plugins/mental/img/favicon.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/bootstrap.min.css">
    <!-- animate CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/animate.css">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/owl.carousel.min.css">
    <!-- themify CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/themify-icons.css">
    <!-- flaticon CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/flaticon.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/magnific-popup.css">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/slick.css">
    <!-- style CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <!--::header part start::-->
    <header class="main_menu home_menu">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand" href="/"> <img src="adminlte/plugins/mental/img/logo.png" alt="logo"> </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse main-menu-item justify-content-end"
                            id="navbarSupportedContent">
                            <ul class="navbar-nav align-items-center">
                                <li class="nav-item active">
                                    <a class="nav-link" href="/">Inicio</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('somos')}}">¿Quiénes somos?</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('servicios')}}">Clínicas y Servicios</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('como_funciona')}}">Cómo funciona el Centro</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('preguntas')}}">Preguntas frecuentes</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('blog')}}">Blog y Testimonios</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/">Aviso de Privacidad</a>
                                </li>
                                <li class="d-none d-lg-block">
                                    <a class="btn_1" href="{{route('login')}}">Iniciar Sesión</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- Header part end-->

    <!-- banner part start-->
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <div class="whole-wrap">
        <div class="container box_1170">
            <div class="section-top-border">
                <h3 class="mb-30">Conozca el sitio</h3>
                <div class="row">
                    <div class="col-lg-12">
                        <blockquote class="generic-blockquote">
                            Conozca nuestras 8 Clínicas y 3 Servicios dando <b><a href="servicios.html">click</a></b> en cada uno de ellos.
                            <br/>
                            <br/>
                            Conozca los padecimientos que se atienden en cada uno de ellos
                            <br/>
                            <br/>
                            Conozca a los clínicos, lea cuidadosamente su perfil profesional, escuche sus videos
                            <br/>
                            <br/>
                            Regístrese como <b><a href="#">usuario</a></b> y solicite su cita con el clínico de su elección desde la seguridad y privacidad de su hogar
                        </blockquote>
                    </div>
                </div>
            </div>
    <!-- End Sample Area -->
            </div>
            </div>
        <div class="container box_1170">
            <h3 class="text-heading">Haga una cita</h3>
            <p class="sample-text" style="font-size: 15px;">
                Usted podrá programar su cita con 24 horas de anticipación. Coloque su nombre, sexo, edad y lugar de residencia (Estado y Municipio) en el día y hora de la agenda del clínico elegido. Una vez seleccionada la cita, confirme mediante el consentimiento informado marcando “Acepto” y realice el pago correspondiente, de lo contrario su cita se anulará.
<br/>
                            <br/>Una vez pagada su sesión, recibirá la confirmación de su cita con una clave, por favor consérvela para cualquier aclaración.
<br/>
                            <br/>Brindamos atención únicamente a mexicanos que residan en cualquier lugar del mundo, debido a que los padecimientos mentales tienen una importante relación con los aspectos social y cultural; los profesionales de Salud Mental Hoy  compartimos la idiosincrasia propia de la cultura mexicana.
.</p>
        </div>
        <div class="container box_1170">
<div class="col-md-12 mt-sm-30">
<br/>
<br/>
                        <h3 class="mb-20">Algunas limitantes:</h3>
                        <div class="">
                            <ul class="unordered-list">
                                <li style="font-size: 15px;">La señal de internet, tanto del usuario como de nosotros puede presentar fallas de manera inesperada.</li>
<br/>
                                <li style="font-size: 15px;">Algunos diagnósticos y tratamientos son complejos, imprecisos o imposibles en línea.</li>
<br/>
                                <li style="font-size: 15px;">Algunos datos clínicos como movimientos del tronco inferior, tonalidad de la piel en rostro o manos; dilatación de las pupilas, gesticulaciones, son imperceptibles con la modalidad on line.</li>
<br/>
                                <li style="font-size: 15px;">Puede ser necesario, en algunos casos, información adicional, por lo que el profesional podrá solicitarle estudios de laboratorio o gabinete que Salud Mental Hoy  no realiza.</li>
<br/>
                                <li style="font-size: 15px;">En algunos casos el usuario puede requerir, de manera adicional, atención personalizada, por lo que el clínico de Salud Mental Hoy se lo hará saber oportunamente y le explicara las razones por las cuales necesita una consulta presencial con otro profesional de la salud ubicado de manera cercana a su lugar de residencia.</li>

                            </ul>
                        </div>
                    </div>
        </div>
        <div class="container box_1170">
<div class="col-md-12 mt-sm-30">
<br/>
<br/>
                        <h3 class="mb-20">DERECHOS Y COMPROMISOS DEL PACIENTE:</h3>
                        <br>
                        <h5>Derechos del Paciente:</h5>
                        <div class="">
                            <ul class="unordered-list">
                                <li style="font-size: 15px;">1.	Recibir un servicio de calidad y con profesionalismo.</li>
<br/>
                                <li style="font-size: 15px;">2.	Ser atendido (a) sin prejuicios ni discriminación por algún motivo.</li>
<br/>
                                <li style="font-size: 15px;">3.	Ser recibido (a) con puntualidad.</li>
<br/>
                                <li style="font-size: 15px;">4.	Que se le asegure la total confidencialidad de la información que provea, de acuerdo con la Ley Federal de Protección de Datos Personales en Posesión de Particulares y en su caso, la Ley de Transparencia.</li>
<br/>
                                <li style="font-size: 15px;">5.	Ser informado (a), con al menos un día de anticipación, de la suspensión de una sesión, o en un caso extremo con al menos dos horas antes de la hora fijada, de no ser así, podrá reprogramar su consulta o si lo prefiere se reembolsara su pago.</li>
                                <br/>
                                <li style="font-size: 15px;">6.	Solicitar y recibir la información que le permita comprender su padecimiento y las acciones médicas y/o psicológicas que se requieran.</li>
                                <br/>
                                <li style="font-size: 15px;">7.	Al término de la primera sesión, el clínico que usted haya elegido para su consulta,  le ofrecerá una primera hipótesis diagnóstica y un plan de tratamiento, que puede incluir la referencia con algún otro clínico.</li>


                            </ul>
                        </div>
                        <br>
                        <h5>Compromisos del Paciente:</h5>
                        <div class="">
                            <ul class="unordered-list">
                                <li style="font-size: 15px;">1.	Asistir puntualmente a las sesiones.</li>
<br/>
                                <li style="font-size: 15px;">2.	Avisar, con al menos 24 hrs. de anticipación la inasistencia a las sesiones, de lo contrario perderá su consulta y el pago que haya hecho por ella.</li>
<br/>
                                <li style="font-size: 15px;">3.	Responsabilizarse de realizar las acciones que lo lleven a su propio beneficio o el de su familia, realizando en todo lo posible las actividades asignadas como tareas en casa.</li>

                            </ul>
                        </div>
                    </div>
        </div>
        <br>
    <!-- Start Align Area -->
    <div class="whole-wrap">



















    </div>
    <!-- End Align Area -->

    <!-- footer part start-->
    <div class="container">
            <div class="row">
                <div class="col-xs-4 col-sm-4 col-xl-4">
                    <div class="learning_member_text">
                       <p style= "text-align: justify;">Si usted sufre <b>ideas de suicidio,</b> debe saber que en su primer consulta, el clínico le pedirá un número de contacto de algún familiar como requisito para recibir tratamiento.
                        </p>
                    </div>
                </div>
                <div class="col-xs-2 col-sm-2 col-xl-2">
                    <div class="learning_member_text">
                        <h5><i class="fa fa-exclamation-triangle"></i> <b>¡Atención!</b></h5>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-xl-6">
                    <div class="learning_member_text">
                       <p style= "text-align: justify;">Si se encuentra en una crisis que le pone a usted o a otra
                            persona en peligro <b>NO USE ESTE SITIO.</b> Llame al 911 en
                            México o a los números de emergencia de su lugar de
                            residenciao a SAPTEL Sistema Nacional de Apoyo,
                            Consejo Psicológico e Intervención en Crisis (55) 52
                            59-8121 Servicio Gratuito las 24 hrs. <a href="http://www.saptel.org.mx/" class="genric-btn success radius">SAPTEL</a>
                        </p>
                    </div>
                </div>
            </div>
            <br/>
            <br/>
            <br/>
        </div>
    <!-- footer part end-->

    <!-- jquery plugins here-->
    <!-- jquery -->
    <script src="js/jquery-1.12.1.min.js"></script>
    <!-- popper js -->
    <script src="js/popper.min.js"></script>
    <!-- bootstrap js -->
    <script src="js/bootstrap.min.js"></script>
    <!-- easing js -->
    <script src="js/jquery.magnific-popup.js"></script>
    <!-- swiper js -->
    <script src="js/swiper.min.js"></script>
    <!-- swiper js -->
    <script src="js/masonry.pkgd.js"></script>
    <!-- particles js -->
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.nice-select.min.js"></script>
    <!-- swiper js -->
    <script src="js/slick.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/waypoints.min.js"></script>
    <!-- custom js -->
    <script src="js/custom.js"></script>
</body>

</html>
