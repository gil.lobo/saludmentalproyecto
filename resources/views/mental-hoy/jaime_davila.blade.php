<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Etrain</title>
    <link rel="icon" href="adminlte/plugins/mental/img/favicon.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/bootstrap.min.css">
    <!-- animate CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/animate.css">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/owl.carousel.min.css">
    <!-- themify CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/themify-icons.css">
    <!-- flaticon CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/flaticon.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/magnific-popup.css">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/slick.css">
    <!-- style CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/style.css">
</head>

<body>
    <!--::header part start::-->
    <header class="main_menu home_menu">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand" href="/"> <img src="adminlte/plugins/mental/img/logo.png" alt="logo"> </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse main-menu-item justify-content-end"
                            id="navbarSupportedContent">
                            <ul class="navbar-nav align-items-center">
                              <li class="nav-item active">
                                  <a class="nav-link" href="/">Inicio</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="{{route('somos')}}">¿Quiénes somos?</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="{{route('servicios')}}">Clínicas y Servicios</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="{{route('como_funciona')}}">Cómo funciona el Centro</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="{{route('preguntas')}}">Preguntas frecuentes</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="{{route('blog')}}">Blog y Testimonios</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="/">Aviso de Privacidad</a>
                              </li>
                              <li class="d-none d-lg-block">
                                  <a class="btn_1" href="{{route('login')}}">Iniciar Sesión</a>
                              </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- Header part end-->

    <!-- breadcrumb start-->
<br/>
<br/>
<br/>

    <!--================ Start Course Details Area =================-->
    <section class="course_details_area section_padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 course_details_left">
                    <div class="content_wrapper">
                        <div class="content">
                            <p><img src="adminlte/plugins/mental/img/equipo_1.png" alt="#" style="width:200px; float:left; padding-right: 15px;">
                                <h3 class="title_top">Jaime Antonino Dávila Sosa<br>Ced. Prof. 1507836</h3>
                                Soy  Psicólogo Clínico hace más de 30 años por la Universidad Autónoma de México (UNAM). Fui Responsable del Programa de Terapia Familiar en el Hospital Psiquiátrico Infantil Dr. Juan N. Navarro” de la Ciudad de México.
Especialista en Prevención del Comportamiento Adictivo (UNAM)
Experto Universitario en Inteligencia Emocional (UNIR Universidad de la Rioja).
Me he capacitado con grandes exponentes de la Terapia Familiar Sistémica como L. Boscolo; G. Nardone; Tom Andersen; Salvador Minuchín; Pegy Penn; Harlene Anderson; Jefrey Zeig; Cloé Madanes; Karin Schlanger, entre otros.
Tengo amplia experiencia en Psicología de la Salud: Depresión, Ansiedad, Familias Disfuncionales; Acompañamiento durante el divorcio; Conflictos de relación de hijos adultos con sus padres; Abuso Sexual; Estrés Postraumático
</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 course_details_left">
                    <div class="content_wrapper">

                        <div class="content">
                          <p><img src="adminlte/plugins/mental/img/equipo_sofia.png" alt="#" style="width:200px; float:left; padding-right: 15px;">
                            <h3 class="title_top">Sofía Hernández González<br>Ced. Prof. 08788173</h3>
                            Soy Licenciada en Psicología y Maestra en Psicología con residencia en Neuropsicología Clínica egresada de la Facultad de Psicología de la UNAM. Durante mi maestría realicé una estancia formativa con el Dr. Jordi Peña-Casanova en el Hospital del Mar - Parc du Salut MAR en Barcelona, España en el Servicio de Neurología en la Sección de Neurología de la Conducta y Demencias
Desde hace 10 años inicié mi formación y práctica neuropsicológica enfocándola a la atención de adultos mayores con deterioros cognitivos y enfermedades neurodegenerativas en el Instituto Nacional de Neurología y Neurocirugía MVS. Me especializo en el diagnóstico de este tipo de enfermedades y en la intervención neuropsicológica a través del diseño e implementación de planes de Estimulación Cognitiva, Entrenamiento y/o Rehabilitación.  Para problemas de memoria, alteraciones del lenguaje, aislamiento social,  cambios de personalidad y estado de ánimo  en adultos mayores.
De dos años a la fecha soy la responsable de la Unidad de Neuromodulación (Estimulación Magnética Transcraneal) de Grupo Médico Carracci y soy la coordinadora de los Diplomados de Neuropsicología de la Universidad de las Américas campus CDMX.
</p>
                        </div>
                    </div>
                </div>


                <div class="col-lg-4 right-contents">
                    <div class="sidebar_top">
                        <ul>
                            <li>
                                <a class="justify-content-between d-flex" href="#">
                                    <p>Egresado</p>
                                    <span class="color">U.N.A.M.</span>
                                </a>
                            </li>
                            <li>
                                <a class="justify-content-between d-flex" href="#">
                                    <p>Ced. Profr. </p>
                                    <span>1507836</span>
                                </a>
                            </li>
                            <li>
                                <a class="justify-content-between d-flex" href="#">
                                    <p>Especialista en: </p>
                                    <span>Psicología Clínica</span>
                                </a>
                            </li>

                        </ul>
                        <a href="#" class="btn_1 d-block">Haga una cita con Psic. Jaime</a>
                    </div>

                    <h4 class="title">Opiniones</h4>
                    <div class="content">
                        <div class="review-top row pt-40">
                            <div class="col-lg-12">
                                <h6 class="mb-15">Acerca de Jaime</h6>
                                <div class="d-flex flex-row reviews justify-content-between">
                                    <span>Atención</span>
                                    <div class="rating">
                                            <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="adminlte/plugins/mental/img/icon/star.svg" alt=""></a>
                                        </div>
                                    <span>Excelente</span>
                                </div>
                                <div class="d-flex flex-row reviews justify-content-between">
                                    <span>Resultados</span>
                                    <div class="rating">
                                            <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="adminlte/plugins/mental/img/icon/star.svg" alt=""></a>
                                        </div>
                                    <span>Excelente</span>
                                </div>
                                <div class="d-flex flex-row reviews justify-content-between">
                                    <span>Lo Recomienda</span>
                                    <div class="rating">
                                            <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="adminlte/plugins/mental/img/icon/star.svg" alt=""></a>
                                        </div>
                                    <span>Por supuesto</span>
                                </div>

                            </div>
                        </div>
                        <div class="comments-area mb-30">
                            <div class="comment-list">
                                <div class="single-comment single-reviews justify-content-between d-flex">
                                    <div class="user justify-content-between d-flex">
                                        <div class="thumb">
                                            <img src="adminlte/plugins/mental/img/cource/cource_1.png" alt="">
                                        </div>
                                        <div class="desc">
                                            <h5><a href="#">Juanito Perz</a>
                                            </h5>
                                            <div class="rating">
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/star.svg" alt=""></a>
                                            </div>
                                            <p class="comment">
                                                Excelente médico, me ha ayudado mucho
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="comment-list">
                                <div class="single-comment single-reviews justify-content-between d-flex">
                                    <div class="user justify-content-between d-flex">
                                        <div class="thumb">
                                            <img src="adminlte/plugins/mental/img/cource/cource_2.png" alt="">
                                        </div>
                                        <div class="desc">
                                            <h5><a href="#">Rosario Tijeras</a>
                                            </h5>
                                            <div class="rating">
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/star.svg" alt=""></a>
                                            </div>
                                            <p class="comment">
                                                El mejor, siempre que hablo con el me siento en confianza y siento que me entiende
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="comment-list">
                                <div class="single-comment single-reviews justify-content-between d-flex">
                                    <div class="user justify-content-between d-flex">
                                        <div class="thumb">
                                            <img src="adminlte/plugins/mental/img/cource/cource_3.png" alt="">
                                        </div>
                                        <div class="desc">
                                            <h5><a href="#">Maria Luna</a>
                                            </h5>
                                            <div class="rating">
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/star.svg" alt=""></a>
                                            </div>
                                            <p class="comment">
                                                Me ha ayudado mucho con mis niños, han mejorado mucho en la escuela
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================ End Course Details Area =================-->
     <div class="container" id="agenda">
<div class="row">
              <div class="col">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">Horarios</h6>
                  </div>
                  <div class="card-body p-0 pb-3 text-center">
                    <table class="table mb-0">
                      <thead class="bg-light">
                        <tr>
                          <th scope="col" class="border-0"></th>
                          <th scope="col" class="border-0">Lunes</th>
                          <th scope="col" class="border-0">Martes</th>
                          <th scope="col" class="border-0">Miércoles</th>
                          <th scope="col" class="border-0">Jueves</th>
                          <th scope="col" class="border-0">Viernes</th>
                          <th scope="col" class="border-0">Sábado</th>
                          <th scope="col" class="border-0">Domingo</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>7:00</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>8:15</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>9:30</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>10:45</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>12:00</td>
                          <td><a class="btn_2" href="#">Agendar</a></td>
                          <td><a class="btn_2" href="#">Agendar</a></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>13:15</td>
                          <td><a class="btn_2" href="#">Agendar</a></td>
                          <td><a class="btn_2" href="#">Agendar</a></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>14:30</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>15:45</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>17:00</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td><a class="btn_2" href="#">Agendar</a></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>18:15</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td><a class="btn_2" href="#">Agendar</a></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>19:30</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>20:45</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>22:00</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            </div>
<section>
   <!-- <div class="container py-5">

   FOR DEMO PURPOSE
    <header class="text-center mb-5 text-white">
      <div class="row">
        <div class="col-lg-7 mx-auto">
          <h1>Precios por sesión</h1>
        </div>
      </div>
    </header>
 END -->



    <!-- <div class="row text-center align-items-end">
      Pricing Table
      <div class="col-lg-4 mb-5 mb-lg-0">
        <div class="bg-white p-5 rounded-lg shadow">
          <h1 class="h6 text-uppercase font-weight-bold mb-4">Básico</h1>
          <h2 class="h1 font-weight-bold">$500<span class="text-small font-weight-normal ml-2">/ sesión</span></h2>

          <div class="custom-separator my-4 mx-auto bg-primary"></div>

     <ul class="list-unstyled my-5 text-small text-left">
            <li class="mb-3">
              <i class="fa fa-check mr-2 text-primary"></i> Lorem ipsum dolor sit amet</li>
            <li class="mb-3">
              <i class="fa fa-check mr-2 text-primary"></i> Sed ut perspiciatis</li>
            <li class="mb-3">
              <i class="fa fa-check mr-2 text-primary"></i> At vero eos et accusamus</li>
            <li class="mb-3 text-muted">
              <i class="fa fa-times mr-2"></i>
              <del>Nam libero tempore</del>
            </li>
            <li class="mb-3 text-muted">
              <i class="fa fa-times mr-2"></i>
              <del>Sed ut perspiciatis</del>
            </li>
            <li class="mb-3 text-muted">
              <i class="fa fa-times mr-2"></i>
              <del>Sed ut perspiciatis</del>
            </li>
          </ul>
          <a href="#" class="btn btn-primary btn-block p-2 shadow rounded-pill">Reservar</a>
        </div>
      </div>
    END -->


      <!-- Pricing Table
      <div class="col-lg-4 mb-5 mb-lg-0">
        <div class="bg-white p-5 rounded-lg shadow">
          <h1 class="h6 text-uppercase font-weight-bold mb-4">4 Sesiones</h1>
          <h2 class="h1 font-weight-bold">$450<span class="text-small font-weight-normal ml-2">/ sesión</span></h2>

          <div class="custom-separator my-4 mx-auto bg-primary"></div>

          <ul class="list-unstyled my-5 text-small text-left font-weight-normal">
            <li class="mb-3">
              <i class="fa fa-check mr-2 text-primary"></i> Lorem ipsum dolor sit amet</li>
            <li class="mb-3">
              <i class="fa fa-check mr-2 text-primary"></i> Sed ut perspiciatis</li>
            <li class="mb-3">
              <i class="fa fa-check mr-2 text-primary"></i> At vero eos et accusamus</li>
            <li class="mb-3">
              <i class="fa fa-check mr-2 text-primary"></i> Nam libero tempore</li>
            <li class="mb-3">
              <i class="fa fa-check mr-2 text-primary"></i> Sed ut perspiciatis</li>
            <li class="mb-3 text-muted">
              <i class="fa fa-times mr-2"></i>
              <del>Sed ut perspiciatis</del>
            </li>
          </ul>
          <a href="#" class="btn btn-primary btn-block p-2 shadow rounded-pill">Reservar</a>
        </div>
      </div>
       END -->


      <!-- Pricing Table-->
      <div class="col-lg-4">
        <div class="bg-white p-5 rounded-lg shadow">
          <h1 class="h6 text-uppercase font-weight-bold mb-4">Tratamiento completo</h1>
          <h2 class="h1 font-weight-bold">$400<span class="text-small font-weight-normal ml-2">/ sesión</span></h2>

          <div class="custom-separator my-4 mx-auto bg-primary"></div>

          <!--<ul class="list-unstyled my-5 text-small text-left font-weight-normal">
            <li class="mb-3">
              <i class="fa fa-check mr-2 text-primary"></i> Lorem ipsum dolor sit amet</li>
            <li class="mb-3">
              <i class="fa fa-check mr-2 text-primary"></i> Sed ut perspiciatis</li>
            <li class="mb-3">
              <i class="fa fa-check mr-2 text-primary"></i> At vero eos et accusamus</li>
            <li class="mb-3">
              <i class="fa fa-check mr-2 text-primary"></i> Nam libero tempore</li>
            <li class="mb-3">
              <i class="fa fa-check mr-2 text-primary"></i> Sed ut perspiciatis</li>
            <li class="mb-3">
              <i class="fa fa-check mr-2 text-primary"></i> Sed ut perspiciatis</li>
          </ul>-->
          <a href="#" class="btn btn-primary btn-block p-2 shadow rounded-pill">Reservar</a>
        </div>
      </div>
      <!-- END -->

    </div>
  </div>
</section>
    <!-- footer part start-->
    <footer class="footer-area">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-sm-6 col-md-4 col-xl-3">
                    <div class="single-footer-widget footer_1">
                        <a href="index.html"> <img src="adminlte/plugins/mental/img/logo.png" alt=""> </a>
                        <p>But when shot real her. Chamber her one visite removal six
                            sending himself boys scot exquisite existend an </p>
                        <p>But when shot real her hamber her </p>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-xl-4">
                    <div class="single-footer-widget footer_2">
                        <h4>Newsletter</h4>
                        <p>Stay updated with our latest trends Seed heaven so said place winged over given forth fruit.
                        </p>
                        <form action="#">
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control" placeholder='Enter email address'
                                        onfocus="this.placeholder = ''"
                                        onblur="this.placeholder = 'Enter email address'">
                                    <div class="input-group-append">
                                        <button class="btn btn_1" type="button"><i class="ti-angle-right"></i></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="social_icon">
                            <a href="#"> <i class="ti-facebook"></i> </a>
                            <a href="#"> <i class="ti-twitter-alt"></i> </a>
                            <a href="#"> <i class="ti-instagram"></i> </a>
                            <a href="#"> <i class="ti-skype"></i> </a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-sm-6 col-md-4">
                    <div class="single-footer-widget footer_2">
                        <h4>Contact us</h4>
                        <div class="contact_info">
                            <p><span> Address :</span> Hath of it fly signs bear be one blessed after </p>
                            <p><span> Phone :</span> +2 36 265 (8060)</p>
                            <p><span> Email : </span>info@colorlib.com </p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="copyright_part_text text-center">
                        <div class="row">
                            <div class="col-lg-12">
                                <p class="footer-text m-0"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="ti-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- footer part end-->

    <!-- jquery plugins here-->
    <!-- jquery -->
    <script src="adminlte/plugins/mental/js/jquery-1.12.1.min.js"></script>
    <!-- popper js -->
    <script src="adminlte/plugins/mental/js/popper.min.js"></script>
    <!-- bootstrap js -->
    <script src="adminlte/plugins/mental/js/bootstrap.min.js"></script>
    <!-- easing js -->
    <script src="adminlte/plugins/mental/js/jquery.magnific-popup.js"></script>
    <!-- swiper js -->
    <script src="adminlte/plugins/mental/js/swiper.min.js"></script>
    <!-- swiper js -->
    <script src="adminlte/plugins/mental/js/masonry.pkgd.js"></script>
    <script src="adminlte/plugins/mental/js/jquery.nice-select.min.js"></script>
    <!-- particles js -->
    <script src="adminlte/plugins/mental/js/owl.carousel.min.js"></script>
    <!-- swiper js -->
    <script src="adminlte/plugins/mental/js/slick.min.js"></script>
    <script src="adminlte/plugins/mental/js/jquery.counterup.min.js"></script>
    <script src="adminlte/plugins/mental/js/waypoints.min.js"></script>
    <!-- custom js -->
    <script src="adminlte/plugins/mental/js/custom.js"></script>
</body>

</html>
