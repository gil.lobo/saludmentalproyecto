<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Etrain</title>
    <link rel="icon" href="adminlte/plugins/mental/img/favicon.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/bootstrap.min.css">
    <!-- animate CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/animate.css">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/owl.carousel.min.css">
    <!-- themify CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/themify-icons.css">
    <!-- flaticon CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/flaticon.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/magnific-popup.css">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/slick.css">
    <!-- style CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/adminlte/plugins/mental/css/font-awesome.min.css">
</head>

<body>
    <!--::header part start::-->
    <header class="main_menu home_menu">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand" href="index.html"> <img src="adminlte/plugins/mental/img/logo.png" alt="logo"> </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse main-menu-item justify-content-end"
                            id="navbarSupportedContent">
                            <ul class="navbar-nav align-items-center">
                                <li class="nav-item active">
                                    <a class="nav-link" href="index.html">Inicio</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="somos.html">¿Quiénes somos?</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="servicios.html">Clínicas y Servicios</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="index.html">Cómo funciona el Centro</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="index.html">Preguntas frecuentes</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="index.html">Blog y Testimonios</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="index.html">Aviso de Privacidad</a>
                                </li>
                                <li class="d-none d-lg-block">
                                    <a class="btn_1" href="#">Registro</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- Header part end-->

    <!-- breadcrumb start-->
<br/>
<br/>
<br/>

    <!--================ Start Course Details Area =================-->
    <section class="course_details_area section_padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 course_details_left">
                    <div class="content_wrapper">
                        <div class="content">
                            <p style= "text-align: justify;"><img src="adminlte/plugins/mental/img/jose_luis.png" alt="#" style="width:200px; float:left; padding-right: 15px;">
                                <h3 class="title_top">José Luis Contreras Torres<br>Ced. Prof. 5107469</h3>
                                Soy Psicólogo Clínico egresado de la Universidad Autónoma del Estado de Morelos, con más de 20 años de experiencia; fui Profesor de materias de Narcomenudeo y Drogas en la Academia Estatal de Seguridad Pública del Estado de Morelos. Psicólogo Responsable del “Programa de Tratamiento Ambulatorio de Adicciones” en el CERESO Morelos; Especialista en Adicciones en los Servicios de Salud de Morelos y actualmente Responsable de Tratamiento de Adicciones del Programa de Justicia Terapéutica de Tribunal de Control, Juicio Oral y Ejecución de Sentencias del Dtto. II de Morelos.
Experto y Diplomado en Psicodiagnóstico y Psicopatología (Universidad La Salle). Especialista en Prevención del Comportamiento Adictivo (UNAM). Con amplia experiencia en Intervención en crisis, Tratamiento ambulatorio de Adicciones, Tratamiento a Familiares de Usuarios y Comunidades Terapéuticas.
En mi trabajo clínico he realizado arriba de 5,500 valoraciones psicológicas con su respectivo pronóstico y recomendaciones para el tratamiento y he brindado tratamiento psicológico a más de 500 personas.
</p>
                        </div>
                    </div>
                    <br/>
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h5 class="m-0">Horarios</h5>
                    <br/>
                    <h6 class="m-0">Clínica de las Adicciones</h6>
                  </div>
                  <div class="card-body p-0 pb-3 text-center">
                    <table class="table mb-0">
                      <thead class="bg-light">
                        <tr>
                          <th scope="col" class="border-0"></th>
                          <th scope="col" class="border-0">LU</th>
                          <th scope="col" class="border-0">MA</th>
                          <th scope="col" class="border-0">MI</th>
                          <th scope="col" class="border-0">JU</th>
                          <th scope="col" class="border-0">VI</th>
                          <th scope="col" class="border-0">SA</th>
                          <th scope="col" class="border-0">DO</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>7:00</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>8:15</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>9:30</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>10:45</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>12:00</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>13:15</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>14:30</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>15:45</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>17:00</td>
                          <td><a class="genric-btn info radius small" href="#">Agendar</a></td>
                          <td></td>
                          <td><a class="genric-btn info radius small" href="#">Agendar</a></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>18:15</td>
                          <td><a class="genric-btn info radius small" href="#">Agendar</a></td>
                          <td></td>
                          <td><a class="genric-btn info radius small" href="#">Agendar</a></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>19:30</td>
                          <td><a class="genric-btn info radius small" href="#">Agendar</a></td>
                          <td></td>
                          <td><a class="genric-btn info radius small" href="#">Agendar</a></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>20:45</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>22:00</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>

                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="video-responsive">
<iframe src="https://www.youtube.com/embed/B7qxwdHTGhg" frameborder="0" allowfullscreen></iframe>
</div>
                </div>
                <div class="col-lg-6 course_details_left">
                    <div class="content_wrapper">

                        <div class="content">
                          <p style= "text-align: justify;"><img src="adminlte/plugins/mental/img/sofia.png" alt="#" style="width:200px; float:left; padding-right: 15px;">
                            <h3 class="title_top">Sofía Hernández González<br>Ced. Prof. 08788173</h3>
                            Soy Licenciada en Psicología y Maestra en Psicología con residencia en Neuropsicología Clínica egresada de la Facultad de Psicología de la UNAM. Durante mi maestría realicé una estancia formativa con el Dr. Jordi Peña-Casanova en el Hospital del Mar - Parc du Salut MAR en Barcelona, España en el Servicio de Neurología en la Sección de Neurología de la Conducta y Demencias
Desde hace 10 años inicié mi formación y práctica neuropsicológica enfocándola a la atención de adultos mayores con deterioros cognitivos y enfermedades neurodegenerativas en el Instituto Nacional de Neurología y Neurocirugía MVS. Me especializo en el diagnóstico de este tipo de enfermedades y en la intervención neuropsicológica a través del diseño e implementación de planes de Estimulación Cognitiva, Entrenamiento y/o Rehabilitación.  Para problemas de memoria, alteraciones del lenguaje, aislamiento social,  cambios de personalidad y estado de ánimo  en adultos mayores.
De dos años a la fecha soy la responsable de la Unidad de Neuromodulación (Estimulación Magnética Transcraneal) de Grupo Médico Carracci y soy la coordinadora de los Diplomados de Neuropsicología de la Universidad de las Américas campus CDMX.</p>
                        </div>
                    </div>
                                        <br/>
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h5 class="m-0">Horarios</h5>
                    <br/>
                    <h6 class="m-0">Clínica del Adulto Mayor</h6>
                  </div>
                  <div class="card-body p-0 pb-3 text-center">
                    <table class="table mb-0">
                      <thead class="bg-light">
                        <tr>
                          <th scope="col" class="border-0"></th>
                          <th scope="col" class="border-0">LU</th>
                          <th scope="col" class="border-0">MA</th>
                          <th scope="col" class="border-0">MI</th>
                          <th scope="col" class="border-0">JU</th>
                          <th scope="col" class="border-0">VI</th>
                          <th scope="col" class="border-0">SA</th>
                          <th scope="col" class="border-0">DO</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>7:00</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>8:15</td>
                          <td><a class="genric-btn info radius small" href="#">Agendar</a></td>
                          <td></td>
                          <td><a class="genric-btn info radius small" href="#">Agendar</a></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>9:30</td>
                          <td></td>
                          <td></td>
                          <td><a class="genric-btn info radius small" href="#">Agendar</a></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>10:45</td>
                          <td></td>
                          <td></td>
                          <td><a class="genric-btn info radius small" href="#">Agendar</a></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>12:00</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>13:15</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>14:30</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>15:45</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>17:00</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>18:15</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>19:30</td>
                          <td></td>
                          <td><a class="genric-btn info radius small" href="#">Agendar</a></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>20:45</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>22:00</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>

                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="video-responsive">
<iframe src="https://www.youtube.com/embed/B7qxwdHTGhg" frameborder="0" allowfullscreen></iframe>
</div>
                </div>


               <!-- <div class="col-lg-4 right-contents">

                    <h4 class="title">Opiniones</h4>
                    <div class="content">
                        <div class="review-top row pt-40">
                            <div class="col-lg-12">
                                <h6 class="mb-15">Acerca de Jaime</h6>
                                <div class="d-flex flex-row reviews justify-content-between">
                                    <span>Atención</span>
                                    <div class="rating">
                                            <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="adminlte/plugins/mental/img/icon/star.svg" alt=""></a>
                                        </div>
                                    <span>Excelente</span>
                                </div>
                                <div class="d-flex flex-row reviews justify-content-between">
                                    <span>Resultados</span>
                                    <div class="rating">
                                            <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="adminlte/plugins/mental/img/icon/star.svg" alt=""></a>
                                        </div>
                                    <span>Excelente</span>
                                </div>
                                <div class="d-flex flex-row reviews justify-content-between">
                                    <span>Lo Recomienda</span>
                                    <div class="rating">
                                            <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="adminlte/plugins/mental/img/icon/star.svg" alt=""></a>
                                        </div>
                                    <span>Por supuesto</span>
                                </div>

                            </div>
                        </div>
                        <div class="comments-area mb-30">
                            <div class="comment-list">
                                <div class="single-comment single-reviews justify-content-between d-flex">
                                    <div class="user justify-content-between d-flex">
                                        <div class="thumb">
                                            <img src="adminlte/plugins/mental/img/cource/cource_1.png" alt="">
                                        </div>
                                        <div class="desc">
                                            <h5><a href="#">Juanito Perz</a>
                                            </h5>
                                            <div class="rating">
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/star.svg" alt=""></a>
                                            </div>
                                            <p class="comment">
                                                Excelente médico, me ha ayudado mucho
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="comment-list">
                                <div class="single-comment single-reviews justify-content-between d-flex">
                                    <div class="user justify-content-between d-flex">
                                        <div class="thumb">
                                            <img src="adminlte/plugins/mental/img/cource/cource_2.png" alt="">
                                        </div>
                                        <div class="desc">
                                            <h5><a href="#">Rosario Tijeras</a>
                                            </h5>
                                            <div class="rating">
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/star.svg" alt=""></a>
                                            </div>
                                            <p class="comment">
                                                El mejor, siempre que hablo con el me siento en confianza y siento que me entiende
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="comment-list">
                                <div class="single-comment single-reviews justify-content-between d-flex">
                                    <div class="user justify-content-between d-flex">
                                        <div class="thumb">
                                            <img src="adminlte/plugins/mental/img/cource/cource_3.png" alt="">
                                        </div>
                                        <div class="desc">
                                            <h5><a href="#">Maria Luna</a>
                                            </h5>
                                            <div class="rating">
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/star.svg" alt=""></a>
                                            </div>
                                            <p class="comment">
                                                Me ha ayudado mucho con mis niños, han mejorado mucho en la escuela
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>-->
            </div>
        </div>
    </section>
    <!--================ End Course Details Area =================-->
     <div class="container" id="agenda">
<div class="row">
              <div class="col-md-6">

              </div>
            </div>
            </div>
<section>
   <!-- <div class="container py-5">

   FOR DEMO PURPOSE
    <header class="text-center mb-5 text-white">
      <div class="row">
        <div class="col-lg-7 mx-auto">
          <h1>Precios por sesión</h1>
        </div>
      </div>
    </header>
 END -->



    <!-- <div class="row text-center align-items-end">
      Pricing Table
      <div class="col-lg-4 mb-5 mb-lg-0">
        <div class="bg-white p-5 rounded-lg shadow">
          <h1 class="h6 text-uppercase font-weight-bold mb-4">Básico</h1>
          <h2 class="h1 font-weight-bold">$500<span class="text-small font-weight-normal ml-2">/ sesión</span></h2>

          <div class="custom-separator my-4 mx-auto bg-primary"></div>

     <ul class="list-unstyled my-5 text-small text-left">
            <li class="mb-3">
              <i class="fa fa-check mr-2 text-primary"></i> Lorem ipsum dolor sit amet</li>
            <li class="mb-3">
              <i class="fa fa-check mr-2 text-primary"></i> Sed ut perspiciatis</li>
            <li class="mb-3">
              <i class="fa fa-check mr-2 text-primary"></i> At vero eos et accusamus</li>
            <li class="mb-3 text-muted">
              <i class="fa fa-times mr-2"></i>
              <del>Nam libero tempore</del>
            </li>
            <li class="mb-3 text-muted">
              <i class="fa fa-times mr-2"></i>
              <del>Sed ut perspiciatis</del>
            </li>
            <li class="mb-3 text-muted">
              <i class="fa fa-times mr-2"></i>
              <del>Sed ut perspiciatis</del>
            </li>
          </ul>
          <a href="#" class="btn btn-primary btn-block p-2 shadow rounded-pill">Reservar</a>
        </div>
      </div>
    END -->


      <!-- Pricing Table
      <div class="col-lg-4 mb-5 mb-lg-0">
        <div class="bg-white p-5 rounded-lg shadow">
          <h1 class="h6 text-uppercase font-weight-bold mb-4">4 Sesiones</h1>
          <h2 class="h1 font-weight-bold">$450<span class="text-small font-weight-normal ml-2">/ sesión</span></h2>

          <div class="custom-separator my-4 mx-auto bg-primary"></div>

          <ul class="list-unstyled my-5 text-small text-left font-weight-normal">
            <li class="mb-3">
              <i class="fa fa-check mr-2 text-primary"></i> Lorem ipsum dolor sit amet</li>
            <li class="mb-3">
              <i class="fa fa-check mr-2 text-primary"></i> Sed ut perspiciatis</li>
            <li class="mb-3">
              <i class="fa fa-check mr-2 text-primary"></i> At vero eos et accusamus</li>
            <li class="mb-3">
              <i class="fa fa-check mr-2 text-primary"></i> Nam libero tempore</li>
            <li class="mb-3">
              <i class="fa fa-check mr-2 text-primary"></i> Sed ut perspiciatis</li>
            <li class="mb-3 text-muted">
              <i class="fa fa-times mr-2"></i>
              <del>Sed ut perspiciatis</del>
            </li>
          </ul>
          <a href="#" class="btn btn-primary btn-block p-2 shadow rounded-pill">Reservar</a>
        </div>
      </div>
       END -->


      <!-- Pricing Table-->

      <!-- END -->

    </div>
  </div>
</section>
    <!-- footer part start-->
        <div class="container">
            <div class="row">
                <div class="col-xs-4 col-sm-4 col-xl-4">
                    <a href="#"> <i class="fa fa-facebook"></i> </a>
                    <a href="#"> <i class="fa fa-twitter"></i> </a>
                    <a href="#"> <i class="fa fa-whatsapp"></i> </a>
                    <a href="#"> <i class="fa fa-telegram"></i> </a>
                    <a href="#"> <i class="fa fa-envelope-o"></i> </a>
                </div>
                <div class="col-xs-2 col-sm-2 col-xl-2">
                    <div class="learning_member_text">
                        <h5><i class="fa fa-exclamation-triangle"></i> <b>¡Atención!</b></h5>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-xl-6">
                    <div class="learning_member_text">
                       <p style= "text-align: justify;">Si se encuentra en una crisis que le pone a usted o a otra
                            persona en peligro <b>NO USE ESTE SITIO.</b> Llame al 911 en
                            México o a los números de emergencia de su lugar de
                            residenciao a SAPTEL Sistema Nacional de Apoyo,
                            Consejo Psicológico e Intervención en Crisis (55) 52
                            59-8121 Servicio Gratuito las 24 hrs. <a href="http://www.saptel.org.mx/" class="genric-btn success radius">SAPTEL</a>
                        </p>
                    </div>
                </div>
            </div>
            <br/>
            <br/>
            <br/>
        </div>
    <!-- footer part end-->

    <!-- jquery plugins here-->
    <!-- jquery -->
    <script src="adminlte/plugins/mental/js/jquery-1.12.1.min.js"></script>
    <!-- popper js -->
    <script src="adminlte/plugins/mental/js/popper.min.js"></script>
    <!-- bootstrap js -->
    <script src="adminlte/plugins/mental/js/bootstrap.min.js"></script>
    <!-- easing js -->
    <script src="adminlte/plugins/mental/js/jquery.magnific-popup.js"></script>
    <!-- swiper js -->
    <script src="adminlte/plugins/mental/js/swiper.min.js"></script>
    <!-- swiper js -->
    <script src="adminlte/plugins/mental/js/masonry.pkgd.js"></script>
    <script src="adminlte/plugins/mental/js/jquery.nice-select.min.js"></script>
    <!-- particles js -->
    <script src="adminlte/plugins/mental/js/owl.carousel.min.js"></script>
    <!-- swiper js -->
    <script src="adminlte/plugins/mental/js/slick.min.js"></script>
    <script src="adminlte/plugins/mental/js/jquery.counterup.min.js"></script>
    <script src="adminlte/plugins/mental/js/waypoints.min.js"></script>
    <!-- custom js -->
    <script src="adminlte/plugins/mental/js/custom.js"></script>
</body>

</html>
