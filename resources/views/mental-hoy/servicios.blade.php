<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Etrain</title>
    <link rel="icon" href="adminlte/plugins/mental/img/favicon.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/bootstrap.min.css">
    <!-- animate CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/animate.css">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/owl.carousel.min.css">
    <!-- themify CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/themify-icons.css">
    <!-- flaticon CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/flaticon.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/magnific-popup.css">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/slick.css">
    <!-- style CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <!--::header part start::-->
    <header class="main_menu home_menu">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand" href="/"> <img src="adminlte/plugins/mental/img/logo.png" alt="logo"> </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse main-menu-item justify-content-end"
                            id="navbarSupportedContent">
                            <ul class="navbar-nav align-items-center">
                              <li class="nav-item active">
                                  <a class="nav-link" href="/">Inicio</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="{{route('somos')}}">¿Quiénes somos?</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="{{route('servicios')}}">Clínicas y Servicios</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="{{route('como_funciona')}}">Cómo funciona el Centro</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="{{route('preguntas')}}">Preguntas frecuentes</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="{{route('blog')}}">Blog y Testimonios</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="/">Aviso de Privacidad</a>
                              </li>
                              <li class="d-none d-lg-block">
                                  <a class="btn_1" href="{{route('login')}}">Iniciar Sesión</a>
                              </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- Header part end-->

    <!-- banner part start-->

    <!-- banner part start-->
    <!--::review_part start::-->
    <section class="special_cource padding_top">
            <div class="row justify-content-center">

                <div class="col-lg-12 col-xl-12 col-sm-12">
                    <div class="section_tittle text-center">
                        <h3>Centro para la Detección, Diagnóstico y Tratamiento oportuno de</h3>
                        <h3> Trastornos del Comportamiento, las Emociones y las Cogniciones</h3>
                    </div>
                </div>
            </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-lg-4">
                    <div class="single_special_cource">
                        <img src="adminlte/plugins/mental/img/1.png" class="special_img" alt="">
                        <div class="special_cource_text">
                            <a href="{{route('adicciones')}}"><h3>Clínica de Adicciones</h3></a>
                            <p style= "text-align: justify;">Contamos con expertos para el diagnóstico, tratamiento y orientación familiar del paciente adulto adicto al alcohol y a otras drogas; también atendemos  adicciones al juego o al sexo. Cuando sea necesario le ofreceremos Terapia Familiar o Terapia de Pareja <br/><a href="{{route('adicciones')}}" style = "font-style: oblique;">Conozca esta clínica</a></p>
                            <!--<div class="author_info">
                                <div class="author_img">
                                    <img src="adminlte/plugins/mental/img/author/author_3.png" alt="">
                                    <div class="author_info_text">
                                        <h5><a href="#">Ricardo Labias</a></h5>
                                    </div>
                                </div>
                                <div class="author_rating">
                                    <div class="rating">
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/star.svg" alt=""></a>
                                    </div>
                                    <p>Puntuación 3.8</p>
                                </div>
                                </div>
                                <div class="author_info">
                                <div class="author_img">
                                    <img src="adminlte/plugins/mental/img/author/author_3.png" alt="">
                                    <div class="author_info_text">
                                        <h5><a href="#">Javier Ángeles</a></h5>
                                    </div>
                                </div>
                                <div class="author_rating">
                                    <div class="rating">
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/star.svg" alt=""></a>
                                    </div>
                                    <p>Puntuación 3.8</p>
                                </div>
                                </div>
                                <div class="author_info">
                                <div class="author_img">
                                    <img src="adminlte/plugins/mental/img/author/author_3.png" alt="">
                                    <div class="author_info_text">
                                        <h5><a href="#">Eric</a></h5>
                                    </div>
                                </div>
                                <div class="author_rating">
                                    <div class="rating">
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/star.svg" alt=""></a>
                                    </div>
                                    <p>Puntuación 3.8</p>
                                </div>
                            </div>-->
                        </div>

                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="single_special_cource">
                        <img src="adminlte/plugins/mental/img/2.png" class="special_img" alt="">
                        <div class="special_cource_text">
                            <a href="{{route('adolescentes')}}"><h3>Clínica para Adolescentes</h3></a>
                             <p style= "text-align: justify;">Contamos con expertos para la atención de problemas de la adolescencia tales cómo inicio en el consumo de sustancias; depresión, ansiedad, pensamientos suicidas, problemas de la alimentación; cutting, problemas de conducta y rendimientos escolar, fugaz de casa, etc. Cuando sea necesario le ofreceremos Terapia Familiar o de Pareja<br/><a href="{{route('adolescentes')}}" style = "font-style: oblique;">Conozca esta clínica</a></p>
                            <!--<div class="author_info">
                                <div class="author_img">
                                    <img src="adminlte/plugins/mental/img/author/author_2.png" alt="">
                                    <div class="author_info_text">
                                        <h5><a href="#">Paula</a></h5>
                                    </div>
                                </div>
                                <div class="author_rating">
                                    <div class="rating">
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/star.svg" alt=""></a>
                                    </div>
                                    <p>Puntuación 3.8</p>
                                </div>
                            </div>
                            <div class="author_info">
                                <div class="author_img">
                                    <img src="adminlte/plugins/mental/img/author/author_2.png" alt="">
                                    <div class="author_info_text">
                                        <h5><a href="#">Ruth V. Ruíz</a></h5>
                                    </div>
                                </div>
                                <div class="author_rating">
                                    <div class="rating">
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/star.svg" alt=""></a>
                                    </div>
                                    <p>Puntuación 3.8</p>
                                </div>
                            </div>-->
                        </div>

                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="single_special_cource">
                        <img src="adminlte/plugins/mental/img/3.png" class="special_img" alt="">
                        <div class="special_cource_text">
                            <a href="{{route('emociones')}}"><h3>Clínica de las Emociones</h3></a>
                            <p style= "text-align: justify;">Aquí encontrara a los profesionales que le ayudaran a resolver todas sus dudas acerca de la depresión, ansiedad o trastorno bipolar; podrás recibir atención para diferentes situaciones que pueden afectar su vida: estrés, duelos por muerte o divorcio, violencia, estrés postraumático,  también brindamos atención a personas con ideas, planes o intentos de suicidio. Aquí podrá encontrar, un programa de entrenamiento en Inteligencia Emocional que le permita desarrollar habilidades para la regulación de sus emociones.<br/><a href="{{route('emociones')}}" style = "font-style: oblique;">Conozca esta clínica</a></p>
                            <!--<div class="author_info">
                                <div class="author_img">
                                    <img src="adminlte/plugins/mental/img/author/author_2.png" alt="">
                                    <div class="author_info_text">
                                        <h5><a href="#">Alma</a></h5>
                                    </div>
                                </div>
                                <div class="author_rating">
                                    <div class="rating">
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/star.svg" alt=""></a>
                                    </div>
                                    <p>Puntuación 3.8</p>
                                </div>
                            </div>
                            <div class="author_info">
                                <div class="author_img">
                                    <img src="adminlte/plugins/mental/img/author/author_2.png" alt="">
                                    <div class="author_info_text">
                                        <h5><a href="#">América Román</a></h5>
                                    </div>
                                </div>
                                <div class="author_rating">
                                    <div class="rating">
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/star.svg" alt=""></a>
                                    </div>
                                    <p>Puntuación 3.8</p>
                                </div>
                            </div>
                            <div class="author_info">
                                <div class="author_img">
                                    <img src="adminlte/plugins/mental/img/author/author_2.png" alt="">
                                    <div class="author_info_text">
                                        <h5><a href="#">Nancy Vargas</a></h5>
                                    </div>
                                </div>
                                <div class="author_rating">
                                    <div class="rating">
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/star.svg" alt=""></a>
                                    </div>
                                    <p>Puntuación 3.8</p>
                                </div>
                            </div>
                            <div class="author_info">
                                <div class="author_img">
                                    <img src="adminlte/plugins/mental/img/author/author_3.png" alt="">
                                    <div class="author_info_text">
                                        <h5><a href="#">Javier Ángeles</a></h5>
                                    </div>
                                </div>
                                <div class="author_rating">
                                    <div class="rating">
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/star.svg" alt=""></a>
                                    </div>
                                    <p>Puntuación 3.8</p>
                                </div>
                            </div>
                            <div class="author_info">
                                <div class="author_img">
                                    <img src="adminlte/plugins/mental/img/author/author_3.png" alt="">
                                    <div class="author_info_text">
                                        <h5><a href="#">Jaime A. Dávila</a></h5>
                                    </div>
                                </div>
                                <div class="author_rating">
                                    <div class="rating">
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/star.svg" alt=""></a>
                                    </div>
                                    <p>Puntuación 3.8</p>
                                </div>
                            </div>
                            <div class="author_info">
                                <div class="author_img">
                                    <img src="adminlte/plugins/mental/img/author/author_3.png" alt="">
                                    <div class="author_info_text">
                                        <h5><a href="#">Ricardo Labias</a></h5>
                                    </div>
                                </div>
                                <div class="author_rating">
                                    <div class="rating">
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/star.svg" alt=""></a>
                                    </div>
                                    <p>Puntuación 3.8</p>
                                </div>
                            </div>-->
                        </div>

                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="single_special_cource">
                        <img src="adminlte/plugins/mental/img/4.png" class="special_img" alt="">
                        <div class="special_cource_text">
                            <a href="{{route('tdah')}}"><h3>Clínica para el trastorno por déficit de atención y los problemas de conducta</h3></a>
                            <p style= "text-align: justify;">Muchos niños con un bajo rendimiento escolar y problemas de conducta son castigados injustamente por sus padres y maestros sin saber que tienen un trastorno: se distraen fácilmente, no concluyen las tareas, se mueven constantemente, no obedecen; este trastorno puede ser detectado y diagnosticado a temprana edad. En los adolescentes también se presenta el TDAH. En esta Clínica también encontrara atención para niños y adolescentes con dificultades escolares de comprensión de lectura, escritura y redacción, asesoría en técnicas y hábitos de estudio  y estrategias para memorizar. Cuando sea necesario le ofreceremos Orientación Familiar o de Pareja.<br/><a href="{{route('tdah')}}" style = "font-style: oblique;">Conozca esta clínica</a></p>
                            <!--<div class="author_info">
                                <div class="author_img">
                                    <img src="adminlte/plugins/mental/img/author/author_2.png" alt="">
                                    <div class="author_info_text">
                                        <h5><a href="#">Olga</a></h5>
                                    </div>
                                </div>
                                <div class="author_rating">
                                    <div class="rating">
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/star.svg" alt=""></a>
                                    </div>
                                    <p>Puntuación 3.8</p>
                                </div>
                            </div>
                            <div class="author_info">
                                <div class="author_img">
                                    <img src="adminlte/plugins/mental/img/author/author_3.png" alt="">
                                    <div class="author_info_text">
                                        <h5><a href="#">Javier Ángeles</a></h5>
                                    </div>
                                </div>
                                <div class="author_rating">
                                    <div class="rating">
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/star.svg" alt=""></a>
                                    </div>
                                    <p>Puntuación 3.8</p>
                                </div>
                            </div>
                            <div class="author_info">
                                <div class="author_img">
                                    <img src="adminlte/plugins/mental/img/author/author_2.png" alt="">
                                    <div class="author_info_text">
                                        <h5><a href="#">Nancy Vargas</a></h5>
                                    </div>
                                </div>
                                <div class="author_rating">
                                    <div class="rating">
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/star.svg" alt=""></a>
                                    </div>
                                    <p>Puntuación 3.8</p>
                                </div>
                            </div>-->

                        </div>

                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="single_special_cource">
                        <img src="adminlte/plugins/mental/img/5.png" class="special_img" alt="">
                        <div class="special_cource_text">
                            <a href="{{route('desarrollo_inf')}}"> <h3>Clínica del desarrollo infantil y espectro autista</h3></a>
                            <p style= "text-align: justify;">Algunos niños presentan problemas en su desarrollo desde el nacimiento, otros lo han adquirido al paso de los primeros años. Nuestro grupo de expertos en paidopsiquiatría y neuropsicología atenderán adecuadamente todas sus inquietudes  y brindaran la asesoría que usted y su hijo necesitan; en algunos casos podrá recibir un plan de tratamiento temporal. Brindamos atención para problemas del lenguaje de aprendizaje derivados del retraso en el desarrollo. Cuando sea necesario le ofreceremos Terapia Familiar o Terapia de Pareja<br/><a href="{{route('desarrollo_inf')}}" style = "font-style: oblique;">Conozca esta clínica</a></p>
                            <!--<div class="author_info">
                                <div class="author_img">
                                    <img src="adminlte/plugins/mental/img/author/author_3.png" alt="">
                                    <div class="author_info_text">
                                        <h5><a href="#">Jaime A. Dávila</a></h5>
                                    </div>
                                </div>
                                <div class="author_rating">
                                    <div class="rating">
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/star.svg" alt=""></a>
                                    </div>
                                    <p>Puntuación 3.8</p>
                                </div>
                            </div>-->
                        </div>

                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="single_special_cource">
                        <img src="adminlte/plugins/mental/img/6.png" class="special_img" alt="">
                        <div class="special_cource_text">
                            <a href="{{route('adulto')}}">  <h3>Adulto Mayor</h3> </a>
                            <p style= "text-align: justify;">En la clínica del Adulto mayor, nuestro equipo de profesionales expertos en neuropsiquiatría le ayudara a conocer y comprender el diagnóstico de su familiar; así mismo le indicaran las mejores alternativas de tratamiento y le asesoraran para usted sepa qué hacer para el bienestar de su paciente y el suyo propio<br/><a href="{{route('adulto')}}" style = "font-style: oblique;">Conozca esta clínica</a></p>
                            <!--<div class="author_info">
                                <div class="author_img">
                                    <img src="adminlte/plugins/mental/img/author/author_3.png" alt="">
                                    <div class="author_info_text">
                                        <h5><a href="#">Eric</a></h5>
                                    </div>
                                </div>
                                <div class="author_rating">
                                    <div class="rating">
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/star.svg" alt=""></a>
                                    </div>
                                    <p>Puntuación 3.8</p>
                                </div>
                            </div>-->
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="single_special_cource">
                        <img src="adminlte/plugins/mental/img/7.png" class="special_img" alt="">
                        <div class="special_cource_text">
                            <a href="{{route('esquizofrenia')}}">  <h3>Clínica de Esquizofrenia</h3> </a>
                            <p style= "text-align: justify;">Usted podrá encontrar las respuestas a sus dudas respecto del comportamiento de su familiar; también encontrara una adecuada asesoría psiquiátrica y en muchos casos un tratamiento y un programa para la reeducación de su paciente con diagnóstico de esquizofrenia<br/><a href="{{route('esquizofrenia')}}" style = "font-style: oblique;">Conozca esta clínica</a></p>
                            <!--<div class="author_info">
                                <div class="author_img">
                                    <img src="adminlte/plugins/mental/img/author/author_3.png" alt="">
                                    <div class="author_info_text">
                                        <h5><a href="#">Eric</a></h5>
                                    </div>
                                </div>
                                <div class="author_rating">
                                    <div class="rating">
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/star.svg" alt=""></a>
                                    </div>
                                    <p>Puntuación 3.8</p>
                                </div>
                            </div>
                            <div class="author_info">
                                <div class="author_img">
                                    <img src="adminlte/plugins/mental/img/author/author_2.png" alt="">
                                    <div class="author_info_text">
                                        <h5><a href="#">Alma</a></h5>
                                    </div>
                                </div>
                                <div class="author_rating">
                                    <div class="rating">
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/star.svg" alt=""></a>
                                    </div>
                                    <p>Puntuación 3.8</p>
                                </div>
                            </div>-->
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="single_special_cource">
                        <img src="adminlte/plugins/mental/img/8.png" class="special_img" alt="">
                        <div class="special_cource_text">
                            <a href="{{route('alimenticios')}}">  <h3>Clínica de los trastornos alimenticios</h3> </a>
                            <p style= "text-align: justify;">Contamos con  especialistas en la modificación  de hábitos conductuales que generan sobrepeso, ellos le ayudan a enfrentar el estrés que genera  una dieta, le brindaran estrategias de autocontrol para moderar la ingesta y manejar mejor sus emociones para mejorar significativamente su calidad de vida. También le ayudaremos a identificar si padece anorexia o bulimia y en muchos casos le ofreceremos un plan de tratamiento médico y psicológico para ello<br/><a href="{{route('alimenticios')}}" style = "font-style: oblique;">Conozca esta clínica</a></p>
                            <!--<div class="author_info">
                                <div class="author_img">
                                    <img src="adminlte/plugins/mental/img/author/author_2.png" alt="">
                                    <div class="author_info_text">
                                        <h5><a href="#">Elvia</a></h5>
                                    </div>
                                </div>
                                <div class="author_rating">
                                    <div class="rating">
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/star.svg" alt=""></a>
                                    </div>
                                    <p>Puntuación 3.8</p>
                                </div>-->
                            </div>
                        </div>
                    </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="single_special_cource">
                        <img src="adminlte/plugins/mental/img/enlace.png" class="special_img" alt="">
                        <div class="special_cource_text">
                            <a href="{{route('enlace')}}">  <h3>Servicio de Psiquiatría de Enlace</h3> </a>
                            <p style= "text-align: justify;">La Psiquiatría de Enlace o Psiquiatría de Hospital General está enfocada en identificar y tratar trastornos psiquiátricos asociados a enfermedades crónicas como diabetes, obesidad, enfermedades cardiovasculares, enfermedad renal crónica, enfermedad hepática crónica, enfermedades reumatológicas, entre otras. Las personas que son diagnosticadas con estos padecimientos pueden experimentar síntomas psiquiátricos debido a las dificultades que implica adaptarse a vivir con estas enfermedades, a complicaciones mismas de la enfermedad o al efecto secundario de algunos tratamientos. Por esta razón, la salud mental de personas con padecimiento crónicos requiere la atención de un grupo profesional multidisciplinario<br/><a href="{{route('enlace')}}" style = "font-style: oblique;">Conozca este servicio</a></p>
                            <!--<div class="author_info">
                                <div class="author_img">
                                    <img src="adminlte/plugins/mental/img/author/author_2.png" alt="">
                                    <div class="author_info_text">
                                        <h5><a href="#">Elvia</a></h5>
                                    </div>
                                </div>
                                <div class="author_rating">
                                    <div class="rating">
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/star.svg" alt=""></a>
                                    </div>
                                    <p>Puntuación 3.8</p>
                                </div>-->
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-4">
                    <div class="single_special_cource">
                        <img src="adminlte/plugins/mental/img/sordos.png" class="special_img" alt="">
                        <div class="special_cource_text">
                            <a href="{{route('auditiva')}}">  <h3>Servicio Psicoterapia para Personas con Discapacidad Auditiva y Pacientes Sordos</h3> </a>
                            <p style= "text-align: justify;">La psicóloga Fabiola Ruíz B. es una excelente terapeuta que domina el lenguaje de señas y le podrá ofrecer un diagnóstico, orientación, tratamiento psicológico, así como orientación familiar para el paciente sordo o con discapacidad auditiva<br/><a href="{{route('auditiva')}}" style = "font-style: oblique;">Conozca este servicio</a></p>
                            <!--<div class="author_info">
                                <div class="author_img">
                                    <img src="adminlte/plugins/mental/img/author/author_2.png" alt="">
                                    <div class="author_info_text">
                                        <h5><a href="#">Elvia</a></h5>
                                    </div>
                                </div>
                                <div class="author_rating">
                                    <div class="rating">
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/star.svg" alt=""></a>
                                    </div>
                                    <p>Puntuación 3.8</p>
                                </div>-->
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-4">
                    <div class="single_special_cource">
                        <img src="adminlte/plugins/mental/img/familiar.png" class="special_img" alt="">
                        <div class="special_cource_text">
                            <a href="{{route('familiar')}}">  <h3>Servicio de Terapia Familiar y de Pareja</h3> </a>
                            <p style= "text-align: justify;">Se trata de un método particular de terapia psicológica que ayuda a resolver situaciones que afectan el funcionamiento de la familia y la vida emocional de sus miembros; favorece la comunicación y la solución de conflictos. Usted puede solicitar una terapia familiar cuando las relaciones entre los miembros de su familia son lastimosas o dañinas y no permiten el crecimiento personal; busque ayuda cuando las relaciones familiares generen dolor emocional e incluso la manifestación de síntomas o trastornos en alguno de sus miembros. La terapia familiar está indicada para el tratamiento de los trastornos emocionales y del comportamiento.<br/><a href="{{route('familiar')}}" style = "font-style: oblique;">Conozca este servicio</a></p>
                            <!--<div class="author_info">
                                <div class="author_img">
                                    <img src="adminlte/plugins/mental/img/author/author_2.png" alt="">
                                    <div class="author_info_text">
                                        <h5><a href="#">Elvia</a></h5>
                                    </div>
                                </div>
                                <div class="author_rating">
                                    <div class="rating">
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="adminlte/plugins/mental/img/icon/star.svg" alt=""></a>
                                    </div>
                                    <p>Puntuación 3.8</p>
                                </div>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--::blog_part end::-->

    <!-- footer part start-->
        <div class="container">
            <div class="row">
                <div class="col-xs-4 col-sm-4 col-xl-4">
                    <div class="learning_member_text">
                       <p style= "text-align: justify;">Si usted sufre <b>ideas de suicidio,</b> debe saber que en su primer consulta, el clínico le pedirá un número de contacto de algún familiar como requisito para recibir tratamiento.
                        </p>
                    </div>
                </div>
                <div class="col-xs-2 col-sm-2 col-xl-2">
                    <div class="learning_member_text">
                        <h5><i class="fa fa-exclamation-triangle"></i> <b>¡Atención!</b></h5>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-xl-6">
                    <div class="learning_member_text">
                       <p style= "text-align: justify;">Si se encuentra en una crisis que le pone a usted o a otra
                            persona en peligro <b>NO USE ESTE SITIO.</b> Llame al 911 en
                            México o a los números de emergencia de su lugar de
                            residenciao a SAPTEL Sistema Nacional de Apoyo,
                            Consejo Psicológico e Intervención en Crisis (55) 52
                            59-8121 Servicio Gratuito las 24 hrs. <a href="http://www.saptel.org.mx/" class="genric-btn success radius">SAPTEL</a>
                        </p>
                    </div>
                </div>
            </div>
            <br/>
            <br/>
            <br/>
        </div>
    <!-- footer part end-->

    <!-- jquery plugins here-->
    <!-- jquery -->
    <script src="adminlte/plugins/mental/js/jquery-1.12.1.min.js"></script>
    <!-- popper js -->
    <script src="adminlte/plugins/mental/js/popper.min.js"></script>
    <!-- bootstrap js -->
    <script src="adminlte/plugins/mental/js/bootstrap.min.js"></script>
    <!-- easing js -->
    <script src="adminlte/plugins/mental/js/jquery.magnific-popup.js"></script>
    <!-- swiper js -->
    <script src="adminlte/plugins/mental/js/swiper.min.js"></script>
    <!-- swiper js -->
    <script src="adminlte/plugins/mental/js/masonry.pkgd.js"></script>
    <!-- particles js -->
    <script src="adminlte/plugins/mental/js/owl.carousel.min.js"></script>
    <script src="adminlte/plugins/mental/js/jquery.nice-select.min.js"></script>
    <!-- swiper js -->
    <script src="adminlte/plugins/mental/js/slick.min.js"></script>
    <script src="adminlte/plugins/mental/js/jquery.counterup.min.js"></script>
    <script src="adminlte/plugins/mental/js/waypoints.min.js"></script>
    <!-- custom js -->
    <script src="adminlte/plugins/mental/js/custom.js"></script>
</body>

</html>
