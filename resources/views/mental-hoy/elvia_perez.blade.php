<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Etrain</title>
    <link rel="icon" href="adminlte/plugins/mental/img/favicon.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/bootstrap.min.css">
    <!-- animate CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/animate.css">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/owl.carousel.min.css">
    <!-- themify CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/themify-icons.css">
    <!-- flaticon CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/flaticon.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/magnific-popup.css">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/slick.css">
    <!-- style CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/style.css">
</head>

<body>
    <!--::header part start::-->
    <header class="main_menu home_menu">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand" href="/"> <img src="adminlte/plugins/mental/img/logo.png" alt="logo"> </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse main-menu-item justify-content-end"
                            id="navbarSupportedContent">
                            <ul class="navbar-nav align-items-center">
                              <li class="nav-item active">
                                  <a class="nav-link" href="/">Inicio</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="{{route('somos')}}">¿Quiénes somos?</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="{{route('servicios')}}">Clínicas y Servicios</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="{{route('como_funciona')}}">Cómo funciona el Centro</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="{{route('preguntas')}}">Preguntas frecuentes</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="{{route('blog')}}">Blog y Testimonios</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="/">Aviso de Privacidad</a>
                              </li>
                              <li class="d-none d-lg-block">
                                  <a class="btn_1" href="{{route('login')}}">Iniciar Sesión</a>
                              </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- Header part end-->

    <!-- breadcrumb start-->
<br/>
<br/>
<br/>

    <!--================ Start Course Details Area =================-->
    <section class="course_details_area section_padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 course_details_left">
                    <div class="main_image">
                        <img class="img-fluid" src="adminlte/plugins/mental/img/equipo_2.png" alt="">
                    </div>
                    <div class="content_wrapper">
                        <h4 class="title_top">Elvia Pérez Vázquez<br>Ced. Prof. 7750527</h4>
                        <div class="content">
                            Soy Psicóloga Clínica Egresada hace 10 años del Centro Interdisciplinario de Ciencias de la Salud del Instituto Politécnico Nacional (CICS-IPN).
Cuento con experiencia en intervenciones basadas en el modelo Cognitivo-Conductual para la modificación de hábitos que incrementan el riesgo a la salud, principalmente en obesidad, enfermedades crónico-degenerativas y adicciones, y para el manejo de pacientes que presentan Patología Dual, Depresión y Ansiedad.
Realicé estudios de Maestría en Medicina Conductual por la Universidad Nacional Autónoma de México (UNAM) y tengo la Especialidad para el Tratamiento de las Adicciones en Centros de Integración Juvenil (CIJ)
Actualmente, curso la Especialidad en Promoción de la Salud y Prevención del Comportamiento Adictivo también en la UNAM.
Laboré durante 3 años en la Unidad de Ensayos Clínicos del Instituto Nacional de Psiquiatría “Ramón de la Fuente Muñiz” (INPRFM) en el área de investigación en adicciones. Realicé la residencia durante 2 años en el Hospital General “Manuel Gea González” en la Clínica de Obesidad y Cirugía Baríatrica.
Actualmente soy psicoterapeuta en los Servicios de Salud de Morelos y en la práctica privada.

                        </div>
                    </div>
                </div>


                <div class="col-lg-5 right-contents">
                                    <div class="col-lg-12 col-md-12">
                    <div class="contenedor-video">
<iframe width="480" height="240" src="https://www.youtube.com/embed/vw73j1gd8qA" frameborder="0" allowfullscreen></iframe>
</div>
                </div>
                    <div class="sidebar_top">
                        <ul>
                            <li>
                                <a class="justify-content-between d-flex" href="#">
                                    <p>Egresado</p>
                                    <span class="color">I.P.N.</span>
                                </a>
                            </li>
                            <li>
                                <a class="justify-content-between d-flex" href="#">
                                    <p>Ced. Profr. </p>
                                    <span>7750527</span>
                                </a>
                            </li>
                            <li>
                                <a class="justify-content-between d-flex" href="#">
                                    <p>Especialista en: </p>
                                    <span>Psicología Clínica</span>
                                </a>
                            </li>

                        </ul>
                        <a href="#" class="btn_1 d-block">Haga una cita con Mtra. Elvia</a>
                    </div>

                    <h4 class="title">Opiniones</h4>
                    <div class="content">
                        <div class="review-top row pt-40">
                            <div class="col-lg-12">
                                <h6 class="mb-15">Acerca de Elvia</h6>
                                <div class="d-flex flex-row reviews justify-content-between">
                                    <span>Atención</span>
                                    <div class="rating">
                                            <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="adminlte/plugins/mental/img/icon/star.svg" alt=""></a>
                                        </div>
                                    <span>Excelente</span>
                                </div>
                                <div class="d-flex flex-row reviews justify-content-between">
                                    <span>Resultados</span>
                                    <div class="rating">
                                            <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="adminlte/plugins/mental/img/icon/star.svg" alt=""></a>
                                        </div>
                                    <span>Excelente</span>
                                </div>
                                <div class="d-flex flex-row reviews justify-content-between">
                                    <span>Lo Recomienda</span>
                                    <div class="rating">
                                            <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="adminlte/plugins/mental/img/icon/star.svg" alt=""></a>
                                        </div>
                                    <span>Por supuesto</span>
                                </div>

                            </div>
                        </div>
                        <div class="comments-area mb-30">
                            <div class="comment-list">
                                <div class="single-comment single-reviews justify-content-between d-flex">
                                    <div class="user justify-content-between d-flex">
                                        <div class="thumb">
                                            <img src="adminlte/plugins/mental/img/cource/cource_1.png" alt="">
                                        </div>
                                        <div class="desc">
                                            <h5><a href="#">Juanito Perz</a>
                                            </h5>
                                            <div class="rating">
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/star.svg" alt=""></a>
                                            </div>
                                            <p class="comment">
                                                Excelente médico, me ha ayudado mucho
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="comment-list">
                                <div class="single-comment single-reviews justify-content-between d-flex">
                                    <div class="user justify-content-between d-flex">
                                        <div class="thumb">
                                            <img src="adminlte/plugins/mental/img/cource/cource_2.png" alt="">
                                        </div>
                                        <div class="desc">
                                            <h5><a href="#">Rosario Tijeras</a>
                                            </h5>
                                            <div class="rating">
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/star.svg" alt=""></a>
                                            </div>
                                            <p class="comment">
                                                El mejor, siempre que hablo con el me siento en confianza y siento que me entiende
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="comment-list">
                                <div class="single-comment single-reviews justify-content-between d-flex">
                                    <div class="user justify-content-between d-flex">
                                        <div class="thumb">
                                            <img src="adminlte/plugins/mental/img/cource/cource_3.png" alt="">
                                        </div>
                                        <div class="desc">
                                            <h5><a href="#">Maria Luna</a>
                                            </h5>
                                            <div class="rating">
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/star.svg" alt=""></a>
                                            </div>
                                            <p class="comment">
                                                Me ha ayudado mucho con mis niños, han mejorado mucho en la escuela
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================ End Course Details Area =================-->

    <!-- footer part start-->
    <footer class="footer-area">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-sm-6 col-md-4 col-xl-3">
                    <div class="single-footer-widget footer_1">
                        <a href="index.html"> <img src="adminlte/plugins/mental/img/logo.png" alt=""> </a>
                        <p>But when shot real her. Chamber her one visite removal six
                            sending himself boys scot exquisite existend an </p>
                        <p>But when shot real her hamber her </p>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-xl-4">
                    <div class="single-footer-widget footer_2">
                        <h4>Newsletter</h4>
                        <p>Stay updated with our latest trends Seed heaven so said place winged over given forth fruit.
                        </p>
                        <form action="#">
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control" placeholder='Enter email address'
                                        onfocus="this.placeholder = ''"
                                        onblur="this.placeholder = 'Enter email address'">
                                    <div class="input-group-append">
                                        <button class="btn btn_1" type="button"><i class="ti-angle-right"></i></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="social_icon">
                            <a href="#"> <i class="ti-facebook"></i> </a>
                            <a href="#"> <i class="ti-twitter-alt"></i> </a>
                            <a href="#"> <i class="ti-instagram"></i> </a>
                            <a href="#"> <i class="ti-skype"></i> </a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-sm-6 col-md-4">
                    <div class="single-footer-widget footer_2">
                        <h4>Contact us</h4>
                        <div class="contact_info">
                            <p><span> Address :</span> Hath of it fly signs bear be one blessed after </p>
                            <p><span> Phone :</span> +2 36 265 (8060)</p>
                            <p><span> Email : </span>info@colorlib.com </p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="copyright_part_text text-center">
                        <div class="row">
                            <div class="col-lg-12">
                                <p class="footer-text m-0"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="ti-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- footer part end-->

    <!-- jquery plugins here-->
    <!-- jquery -->
    <script src="adminlte/plugins/mental/js/jquery-1.12.1.min.js"></script>
    <!-- popper js -->
    <script src="adminlte/plugins/mental/js/popper.min.js"></script>
    <!-- bootstrap js -->
    <script src="adminlte/plugins/mental/js/bootstrap.min.js"></script>
    <!-- easing js -->
    <script src="adminlte/plugins/mental/js/jquery.magnific-popup.js"></script>
    <!-- swiper js -->
    <script src="adminlte/plugins/mental/js/swiper.min.js"></script>
    <!-- swiper js -->
    <script src="adminlte/plugins/mental/js/masonry.pkgd.js"></script>
    <script src="adminlte/plugins/mental/js/jquery.nice-select.min.js"></script>
    <!-- particles js -->
    <script src="adminlte/plugins/mental/js/owl.carousel.min.js"></script>
    <!-- swiper js -->
    <script src="adminlte/plugins/mental/js/slick.min.js"></script>
    <script src="adminlte/plugins/mental/js/jquery.counterup.min.js"></script>
    <script src="adminlte/plugins/mental/js/waypoints.min.js"></script>
    <!-- custom js -->
    <script src="adminlte/plugins/mental/js/custom.js"></script>
</body>

</html>
