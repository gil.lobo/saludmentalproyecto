<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Etrain</title>

    <link rel="icon" href="adminlte/plugins/mental/img/favicon.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/bootstrap.min.css">
    <!-- animate CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/animate.css">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/owl.carousel.min.css">
    <!-- themify CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/themify-icons.css">
    <!-- flaticon CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/flaticon.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/magnific-popup.css">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/slick.css">
    <!-- style CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <!--::header part start::-->
    <header class="main_menu home_menu">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand" href="/"> <img src="adminlte/plugins/mental/img/logo.png" alt="logo"> </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse main-menu-item justify-content-end"
                            id="navbarSupportedContent">
                            <ul class="navbar-nav align-items-center">
                                <li class="nav-item active">
                                    <a class="nav-link" href="/">Inicio</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('somos')}}">¿Quiénes somos?</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('servicios')}}">Clínicas y Servicios</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('como_funciona')}}">Cómo funciona el Centro</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('preguntas')}}">Preguntas frecuentes</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('blog')}}">Blog y Testimonios</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/">Aviso de Privacidad</a>
                                </li>
                                <li class="d-none d-lg-block">
                                    <a class="btn_1" href="{{route('login')}}">Iniciar Sesión</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- Header part end-->

    <!-- banner part start-->
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
    <!-- banner part start-->
<section class="testimonial_part">
    <div class="row justify-content-center">

                <div class="col-lg-12 col-xl-12 col-sm-12">
                    <div class="section_tittle text-center">
                        <h3>Centro para la Detección, Diagnóstico y Tratamiento oportuno de</h3>
                        <h3> Trastornos del Comportamiento, las Emociones y las Cogniciones</h3>
                    </div>
                </div>
            </div>
        <div class="container-fluid ">
            <div class="row">
                <div class="col-lg-1 col-md-1">
                    <a href="#"> <i class="fa fa-facebook"></i> </a>
                    <a href="#"> <i class="fa fa-twitter"></i> </a>
                    <a href="#"> <i class="fa fa-whatsapp"></i> </a>
                    <a href="#"> <i class="fa fa-telegram"></i> </a>
                    <a href="{{route('blog')}}"> <i class="fa fa-envelope-o"></i> </a>
                </div>
                <div class="portada col-md-3 col-lg-3">
                    <div class="learning_member_text">
                       <p style= "text-align: justify;">SMH es el primer sitio web que funciona con la modalidad de una Clínica Virtual de Salud Mental en México, que cuenta con un gran equipo de Especialistas:
                        <br/>
                        <br/>
        <br/><b>       8</b> Clínicas
        <br/><b>       3</b> Servicios
        <br/><b>       1</b> Psiquiatra
        <br/><b>       1</b> Neuropsiquiatra
        <br/><b>       1</b> Psiquiatra Infantil
        <br/><b>       2</b> Neuropsicologías
        <br/><b>       5</b> Terapeutas Familiares y de Pareja
        <br/><b>       6</b> Psicólogos Clínicos
        <br/><b>       1</b> Lic. Comunicación Humana
        <br/><b>       1</b> Nutrióloga
        <br/><b>       1</b> Psiquiatría de enlace
</a>
<br/><br/>Todos con un promedio 1000 consultas otorgadas al año en Instituciones de salud Pública.
</p>
                    </div>
                </div>


                <div class="col-md-7 col-lg-7">
                    <div class="learning_member_text">
                        <h2 style= "text-align: center;">CLÍNICAS Y SERVICIOS</h2>
<div class="row">
            <div class="col-md-6 col-lg-6">
                <div class="button-group-area mt-10">
                    <a href="{{route('adulto')}}" class="genric-btn default">1.- Clínica para el Adulto Mayor</a>
                </div>
                <div class="button-group-area mt-10">
                    <a href="{{route('tdah')}}" class="genric-btn default">2.- Clínica para el TDHA, el Aprendizaje y los Problemas de Conducta Infantil</a>
                </div>
                <div class="button-group-area mt-10">
                    <a href="{{route('desarrollo_inf')}}" class="genric-btn default">3.- Clínica del Desarrollo Infantil y el Espectro Autista</a>
                </div>
                <div class="button-group-area mt-10">
                    <a href="{{route('emociones')}}" class="genric-btn default">4.- Clínica de las Emociones</a>
                </div>
                <div class="button-group-area mt-10">
                    <a href="{{route('adolescentes')}}" class="genric-btn default">5.- Clínica para Adolescentes</a>
                </div>
                <div class="button-group-area mt-10">
                    <a href="{{route('alimenticios')}}" class="genric-btn default">6.- Clínica para Trastornos de la Conducta Alimentaria</a>
                </div>
            </div>
            <div class="col-md-6 col-lg-6">
                <div class="button-group-area mt-10">
                    <a href="{{route('esquizofrenia')}}" class="genric-btn default">7.- Clínica de Esquizofrenia</a>
                </div>
                <div class="button-group-area mt-10">
                    <a href="{{route('adicciones')}}" class="genric-btn default">8.- Clínica de las Adicciones</a>
                </div>
                <div class="button-group-area mt-10">
                    <a href="{{route('enlace')}}" class="genric-btn default">A.- Servicio de Psiquiatria de Enlace</a>
                </div>
                <div class="button-group-area mt-10">
                    <a href="{{route('auditiva')}}" class="genric-btn default">B.- Servicio Psicoterapia para Personas con Discapacidad Auditiva y Pacientes Sordos</a>
                </div>
                <div class="button-group-area mt-10">
                    <a href="{{route('familiar')}}" class="genric-btn default">C.- Servicio de Terapia Familiar y de Pareja</a>
                </div>
                 <br/>
                <div class="contenedor-video">
<iframe width="360" height="180" src="https://www.youtube.com/embed/hDhUFttX7ek" frameborder="0" allowfullscreen></iframe>
</div>
            </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<br/>
<br/>


    <!-- member_counter counter start
    <section class="member_counter">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-sm-6">
                    <div class="single_member_counter">
                        <span class="counter">1024</span>
                        <h4>Consultas brindadas</h4>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single_member_counter">
                        <span class="counter">960</span>
                        <h4>Pacientes</h4>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single_member_counter">
                        <span class="counter">15</span>
                        <h4>Expertos Clinicos</h4>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single_member_counter">
                        <span class="counter">8</span>
                        <h4>Especialidades</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
member_counter counter end -->
        <div class="container">
            <div class="row">
                <div class="col-xs-4 col-sm-4 col-xl-4">
                    <div class="learning_member_text">
                       <p style= "text-align: justify;">Si usted sufre <b>ideas de suicidio,</b> debe saber que en su primer consulta, el clínico le pedirá un número de contacto de algún familiar como requisito para recibir tratamiento.
                        </p>
                    </div>
                </div>
                <div class="col-xs-2 col-sm-2 col-xl-2">
                    <div class="learning_member_text">
                        <h5><i class="fa fa-exclamation-triangle"></i> <b>¡Atención!</b></h5>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-xl-6">
                    <div class="learning_member_text">
                       <p style= "text-align: justify;">Si se encuentra en una crisis que le pone a usted o a otra
                            persona en peligro <b>NO USE ESTE SITIO.</b> Llame al 911 en
                            México o a los números de emergencia de su lugar de
                            residenciao a SAPTEL Sistema Nacional de Apoyo,
                            Consejo Psicológico e Intervención en Crisis (55) 52
                            59-8121 Servicio Gratuito las 24 hrs. <a href="http://www.saptel.org.mx/" class="genric-btn success radius">SAPTEL</a>
                        </p>
                    </div>
                </div>
            </div>
            <br/>
            <br/>
            <br/>
        </div>
    <!--::review_part start::-->
    <!-- footer part end-->

    <!-- jquery plugins here-->
    <!-- jquery -->
    <script src="adminlte/plugins/mental/js/jquery-1.12.1.min.js"></script>
    <!-- popper js -->
    <script src="adminlte/plugins/mental/js/popper.min.js"></script>
    <!-- bootstrap js -->
    <script src="adminlte/plugins/mental/js/bootstrap.min.js"></script>
    <!-- easing js -->
    <script src="adminlte/plugins/mental/js/jquery.magnific-popup.js"></script>
    <!-- swiper js -->
    <script src="adminlte/plugins/mental/js/swiper.min.js"></script>
    <!-- swiper js -->
    <script src="adminlte/plugins/mental/js/masonry.pkgd.js"></script>
    <!-- particles js -->
    <script src="adminlte/plugins/mental/js/owl.carousel.min.js"></script>
    <script src="adminlte/plugins/mental/js/jquery.nice-select.min.js"></script>
    <!-- swiper js -->
    <script src="adminlte/plugins/mental/js/slick.min.js"></script>
    <script src="adminlte/plugins/mental/js/jquery.counterup.min.js"></script>
    <script src="adminlte/plugins/mental/js/waypoints.min.js"></script>
    <!-- custom js -->
    <script src="adminlte/plugins/mental/js/custom.js"></script>
</body>

</html>
