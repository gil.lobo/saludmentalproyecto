<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Etrain</title>
    <link rel="icon" href="adminlte/plugins/mental/img/favicon.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/bootstrap.min.css">
    <!-- animate CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/animate.css">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/owl.carousel.min.css">
    <!-- themify CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/themify-icons.css">
    <!-- flaticon CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/flaticon.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/magnific-popup.css">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/slick.css">
    <!-- style CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <!--::header part start::-->
    <header class="main_menu home_menu">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand" href="/"> <img src="adminlte/plugins/mental/img/logo.png" alt="logo"> </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                          <div class="collapse navbar-collapse main-menu-item justify-content-end"
                            id="navbarSupportedContent">
                            <ul class="navbar-nav align-items-center">
                              <li class="nav-item active">
                                  <a class="nav-link" href="/">Inicio</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="{{route('somos')}}">¿Quiénes somos?</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="{{route('servicios')}}">Clínicas y Servicios</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="{{route('como_funciona')}}">Cómo funciona el Centro</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="{{route('preguntas')}}">Preguntas frecuentes</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="{{route('blog')}}">Blog y Testimonios</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="/">Aviso de Privacidad</a>
                              </li>
                              <li class="d-none d-lg-block">
                                  <a class="btn_1" href="{{route('login')}}">Iniciar Sesión</a>
                              </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- Header part end-->

    <!-- breadcrumb start-->
<br/>
<br/>
<br/>

    <!--================ Start Course Details Area =================-->
    <section class="course_details_area section_padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 course_details_left">
                    <div class="content_wrapper">
                        <div class="content">
                          <p style= "text-align: justify;"><img src="adminlte/plugins/mental/img/elvia.png" alt="#" style="width:200px; float:left; padding-right: 15px;">
                            <h3 class="title_top">Elvia Pérez Vázquez<br>Ced. Prof. 7750527</h3>
                            Soy Psicóloga Clínica Egresada hace 10 años del Centro Interdisciplinario de Ciencias de la Salud del Instituto Politécnico Nacional (CICS-IPN).
Realicé estudios de Maestría en Medicina Conductual por la Universidad Nacional Autónoma de México (UNAM) y tengo la Especialidad para el Tratamiento de las Adicciones en Centros de Integración Juvenil (CIJ)
Actualmente, curso la Especialidad en Promoción de la Salud y Prevención del Comportamiento Adictivo también en la UNAM.
Laboré durante 3 años en la Unidad de Ensayos Clínicos del Instituto Nacional de Psiquiatría “Ramón de la Fuente Muñiz” (INPRFM) en el área de investigación en adicciones. Realicé la residencia durante 2 años en el Hospital General “Manuel Gea González” en la Clínica de Obesidad y Cirugía Baríatrica. Actualmente soy psicoterapeuta en los Servicios de Salud de Morelos y en la práctica privada.
Cuento con experiencia en intervenciones Cognitivo-Conductual para el Manejo Conductual de hábitos que llevan al sobrepeso, enfermedades crónico-degenerativas y adicciones y para el manejo de pacientes que presentan Depresión y Ansiedad.
</p>
                        </div>
                    </div>
                    <br/>

                </div>
                <div class="col-lg-6 course_details_left">
                    <br/>
                    <br/>
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h5 class="m-0">Horarios</h5>
                    <br/>
                    <h6 class="m-0">Clínica de las Emociones, Clínica de los Trastornos Alimenticios</h6>
                  </div>
                  <div class="card-body p-0 pb-3 text-center">
                    <table class="table mb-0">
                      <thead class="bg-light">
                        <tr>
                          <th scope="col" class="border-0"></th>
                          <th scope="col" class="border-0">LU</th>
                          <th scope="col" class="border-0">MA</th>
                          <th scope="col" class="border-0">MI</th>
                          <th scope="col" class="border-0">JU</th>
                          <th scope="col" class="border-0">VI</th>
                          <th scope="col" class="border-0">SA</th>
                          <th scope="col" class="border-0">DO</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>7:00</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>8:15</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>9:30</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>10:45</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>12:00</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>13:15</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>14:30</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>15:45</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>17:00</td>
                          <td></td>
                          <td><a class="genric-btn info radius small" href="#">Agendar</a></td>
                          <td></td>
                          <td><a class="genric-btn info radius small" href="#">Agendar</a></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>18:15</td>
                          <td></td>
                          <td><a class="genric-btn info radius small" href="#">Agendar</a></td>
                          <td></td>
                          <td><a class="genric-btn info radius small" href="#">Agendar</a></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>19:30</td>
                          <td></td>
                          <td><a class="genric-btn info radius small" href="#">Agendar</a></td>
                          <td></td>
                          <td><a class="genric-btn info radius small" href="#">Agendar</a></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>20:45</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>22:00</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>

                      </tbody>
                    </table>
                  </div>
                </div>
                </div>
            </div>
        </div>
    </section>
    <!--================ End Course Details Area =================-->
     <div class="container" id="agenda">
<div class="row">
              <div class="col-md-6">

              </div>
            </div>
            </div>

    <!-- footer part start-->
        <div class="container">
            <div class="row">
                <div class="col-xs-4 col-sm-4 col-xl-4">
                    <a href="#"> <i class="fa fa-facebook"></i> </a>
                    <a href="#"> <i class="fa fa-twitter"></i> </a>
                    <a href="#"> <i class="fa fa-whatsapp"></i> </a>
                    <a href="#"> <i class="fa fa-telegram"></i> </a>
                    <a href="#"> <i class="fa fa-envelope-o"></i> </a>
                </div>
                <div class="col-xs-2 col-sm-2 col-xl-2">
                    <div class="learning_member_text">
                        <h5><i class="fa fa-exclamation-triangle"></i> <b>¡Atención!</b></h5>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-xl-6">
                    <div class="learning_member_text">
                       <p style= "text-align: justify;">Si se encuentra en una crisis que le pone a usted o a otra
                            persona en peligro <b>NO USE ESTE SITIO.</b> Llame al 911 en
                            México o a los números de emergencia de su lugar de
                            residenciao a SAPTEL Sistema Nacional de Apoyo,
                            Consejo Psicológico e Intervención en Crisis (55) 52
                            59-8121 Servicio Gratuito las 24 hrs. <a href="http://www.saptel.org.mx/" class="genric-btn success radius">SAPTEL</a>
                        </p>
                    </div>
                </div>
            </div>
            <br/>
            <br/>
            <br/>
        </div>
    <!-- footer part end-->

    <!-- jquery plugins here-->
    <!-- jquery -->
    <script src="adminlte/plugins/mental/js/jquery-1.12.1.min.js"></script>
    <!-- popper js -->
    <script src="adminlte/plugins/mental/js/popper.min.js"></script>
    <!-- bootstrap js -->
    <script src="adminlte/plugins/mental/js/bootstrap.min.js"></script>
    <!-- easing js -->
    <script src="adminlte/plugins/mental/js/jquery.magnific-popup.js"></script>
    <!-- swiper js -->
    <script src="adminlte/plugins/mental/js/swiper.min.js"></script>
    <!-- swiper js -->
    <script src="adminlte/plugins/mental/js/masonry.pkgd.js"></script>
    <script src="adminlte/plugins/mental/js/jquery.nice-select.min.js"></script>
    <!-- particles js -->
    <script src="adminlte/plugins/mental/js/owl.carousel.min.js"></script>
    <!-- swiper js -->
    <script src="adminlte/plugins/mental/js/slick.min.js"></script>
    <script src="adminlte/plugins/mental/js/jquery.counterup.min.js"></script>
    <script src="adminlte/plugins/mental/js/waypoints.min.js"></script>
    <!-- custom js -->
    <script src="adminlte/plugins/mental/js/custom.js"></script>
</body>

</html>
