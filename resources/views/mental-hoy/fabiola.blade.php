<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Etrain</title>
    <link rel="icon" href="adminlte/plugins/mental/img/favicon.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/bootstrap.min.css">
    <!-- animate CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/animate.css">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/owl.carousel.min.css">
    <!-- themify CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/themify-icons.css">
    <!-- flaticon CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/flaticon.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/magnific-popup.css">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/slick.css">
    <!-- style CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <!--::header part start::-->
    <header class="main_menu home_menu">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand" href="/"> <img src="adminlte/plugins/mental/img/logo.png" alt="logo"> </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                          <div class="collapse navbar-collapse main-menu-item justify-content-end"
                            id="navbarSupportedContent">
                            <ul class="navbar-nav align-items-center">
                                <li class="nav-item active">
                                    <a class="nav-link" href="/">Inicio</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('somos')}}">¿Quiénes somos?</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('servicios')}}">Clínicas y Servicios</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('como_funciona')}}">Cómo funciona el Centro</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('preguntas')}}">Preguntas frecuentes</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('blog')}}">Blog y Testimonios</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/">Aviso de Privacidad</a>
                                </li>
                                <li class="d-none d-lg-block">
                                    <a class="btn_1" href="{{route('login')}}">Iniciar Sesión</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- Header part end-->

    <!-- breadcrumb start-->
<br/>
<br/>
<br/>

    <!--================ Start Course Details Area =================-->
    <section class="course_details_area section_padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 course_details_left">
                    <div class="content_wrapper">
                      <a href="javascript: history.go(-1)" class="genric-btn success radius">Regresar</a>
                        <div class="content" style= "text-align: justify;">
                          <p style= "text-align: justify;"><img src="adminlte/plugins/mental/img/fabiola.png" alt="#" style="width:200px; float:left; padding-right: 15px;">
                            <h3 class="title_top">Fabiola Ruiz Bedolla<br>Ced. Prof. 4715089</h3>
                            Soy  Psicóloga Clínica desde hace 23 años por la Universidad Nacional Autónoma de México (UNAM). Fui Responsable del  Servicio de Psicología en el Instituto Pedagógico para Problemas de Lenguaje, I.A.P. Especialista en la atención a pacientes con discapacidad auditiva y sus familias, así como especialista en Comunidad de Sordos y ámbitos educativos.
Soy Maestra en Sensibilización Educativa con Orientación Gestalt y cuento con especialidades en Psicoterapia Gestalt, Orientadora Humanista y especialidad en adolescentes.
Doctorado Honoris Causa por el Claustro Doctoral Iberoamericano en conjunto con otras instituciones como Liderazgo Hoy, Universidad LAMAR y la Comisión Nacional de Derechos Humanos, entre otras.
Cuenta con una experiencia de más de 23 años trabajando desde diferentes ámbitos relacionados a la discapacidad y a la Comunidad de Sordos como parte de la inclusión de este sector a diversos ámbitos como son el familiar, educativo, social y laboral.
En el ámbito de  la psicoterapia tengo experiencia en el trabajo con pacientes que presentan cuadros de ansiedad, depresión, familias disfuncionales, así como el acompañamiento con parejas en coterapia y temas de abuso sexual  y estrés postraumático.
</p>
                        </div>
                    </div>
                    <br/>
<div class="video-responsive">
<iframe src="https://www.youtube.com/embed/WDz8CCPSoXI" frameborder="0" allowfullscreen></iframe>
</div>
                </div>
                <div class="col-lg-6 course_details_left">
                    <br/>
                    <br/>
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h5 class="m-0">Haga una cita con Psic. Fabiola</h5>
                    <!--<br/>
                    <h6 class="m-0">Clínica de las Emociones, Clínica para personas con Discapacidad Auditiva</h6>-->
                  </div>
                  <div class="card-body p-0 pb-3 text-center">
                    <table class="table mb-0">
                      <thead class="bg-light">
                        <tr>
                          <th scope="col" class="border-0"></th>
                          <th scope="col" class="border-0">LU</th>
                          <th scope="col" class="border-0">MA</th>
                          <th scope="col" class="border-0">MI</th>
                          <th scope="col" class="border-0">JU</th>
                          <th scope="col" class="border-0">VI</th>
                          <th scope="col" class="border-0">SA</th>
                          <th scope="col" class="border-0">DO</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>7:00</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>8:15</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td><a class="genric-btn info radius small" href="{{route('pago')}}">Agendar</a></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>9:30</td>
                          <td><a class="genric-btn info radius small" href="{{route('pago')}}">Agendar</a></td>
                          <td><a class="genric-btn info radius small" href="{{route('pago')}}">Agendar</a></td>
                          <td></td>
                          <td></td>
                          <td><a class="genric-btn info radius small" href="{{route('pago')}}">Agendar</a></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>10:45</td>
                          <td><a class="genric-btn info radius small" href="{{route('pago')}}">Agendar</a></td>
                          <td><a class="genric-btn info radius small" href="{{route('pago')}}">Agendar</a></td>
                          <td></td>
                          <td></td>
                          <td><a class="genric-btn info radius small" href="{{route('pago')}}">Agendar</a></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>12:00</td>
                          <td><a class="genric-btn info radius small" href="{{route('pago')}}">Agendar</a></td>
                          <td><a class="genric-btn info radius small" href="{{route('pago')}}">Agendar</a></td>
                          <td></td>
                          <td><a class="genric-btn info radius small" href="{{route('pago')}}">Agendar</a></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>13:15</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>14:30</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>15:45</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>17:00</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>18:15</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>19:30</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>20:45</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
<tr>
                          <td>22:00</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>

                      </tbody>
                    </table>
                  </div>
                </div>
                </div>
            </div>
        </div>
    </section>
    <!--================ End Course Details Area =================-->
     <div class="container" id="agenda">
<div class="row">
              <div class="col-md-6">

              </div>
            </div>
            </div>

    <!-- footer part start-->
    <footer class="footer-area">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-sm-6 col-md-3 col-xl-3">
                    <div class="single-footer-widget footer_1">
                        <a href="index.html"> <img src="adminlte/plugins/mental/img/logo.png" alt=""> </a>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3 col-xl-3">
                    <div class="single-footer-widget footer_2">
                        <h4>Servicio Integral</h4>
                        <div class="contact_info">
                            <p>Funcionamos como una clínica de Salud Mental, todos los Clínicos interactuamos para identificar la mejor opción farmacológica y/o terapéutica para usted.</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3 col-xl-3">
                    <div class="single-footer-widget footer_2">
                        <h4>Servicio Profesional</h4>
                        <div class="contact_info">
                            <p>Los clínicos de SMH cuentan con amplia experiencia, en promedio atienden o han atendido entre 800 y 1000 pacientes al año en Institucionesde Salud Pública.</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3 col-xl-3">
                    <div class="single-footer-widget footer_2">
                        <h4>Servicio Seguro</h4>
                        <div class="contact_info">
                            <p>Estamos profundamente comprometidos con su seguridad y con el manejo responsable de su información, por lo que, en ningún caso solicitaremos la dirección de su domicilio, evitaremos el uso de su teléfono celular y sus datos jamás serán transferidos a otras plataformas.</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="copyright_part_text text-center">
                        <div class="row">
                            <div class="col-lg-12">
                                <p class="footer-text m-0">
Copyright &copy;<script>document.write(new Date().getFullYear());</script> Todos los derechos Reservados | Salud Mental Hoy</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- footer part end-->

    <!-- jquery plugins here-->
    <!-- jquery -->
    <script src="adminlte/plugins/mental/js/jquery-1.12.1.min.js"></script>
    <!-- popper js -->
    <script src="adminlte/plugins/mental/js/popper.min.js"></script>
    <!-- bootstrap js -->
    <script src="adminlte/plugins/mental/js/bootstrap.min.js"></script>
    <!-- easing js -->
    <script src="adminlte/plugins/mental/js/jquery.magnific-popup.js"></script>
    <!-- swiper js -->
    <script src="adminlte/plugins/mental/js/swiper.min.js"></script>
    <!-- swiper js -->
    <script src="adminlte/plugins/mental/js/masonry.pkgd.js"></script>
    <script src="adminlte/plugins/mental/js/jquery.nice-select.min.js"></script>
    <!-- particles js -->
    <script src="adminlte/plugins/mental/js/owl.carousel.min.js"></script>
    <!-- swiper js -->
    <script src="adminlte/plugins/mental/js/slick.min.js"></script>
    <script src="adminlte/plugins/mental/js/jquery.counterup.min.js"></script>
    <script src="adminlte/plugins/mental/js/waypoints.min.js"></script>
    <!-- custom js -->
    <script src="adminlte/plugins/mental/js/custom.js"></script>
</body>

</html>
