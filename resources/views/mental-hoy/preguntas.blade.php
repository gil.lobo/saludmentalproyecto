<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Etrain</title>
    <link rel="icon" href="adminlte/plugins/mental/img/favicon.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/bootstrap.min.css">
    <!-- animate CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/animate.css">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/owl.carousel.min.css">
    <!-- themify CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/themify-icons.css">
    <!-- flaticon CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/flaticon.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/magnific-popup.css">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/slick.css">
    <!-- style CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <!--::header part start::-->
    <header class="main_menu home_menu">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand" href="/"> <img src="adminlte/plugins/mental/img/logo.png" alt="logo"> </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse main-menu-item justify-content-end"
                            id="navbarSupportedContent">
                            <ul class="navbar-nav align-items-center">
                              <li class="nav-item active">
                                  <a class="nav-link" href="/">Inicio</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="{{route('somos')}}">¿Quiénes somos?</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="{{route('servicios')}}">Clínicas y Servicios</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="{{route('como_funciona')}}">Cómo funciona el Centro</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="{{route('preguntas')}}">Preguntas frecuentes</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="{{route('blog')}}">Blog y Testimonios</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="/">Aviso de Privacidad</a>
                              </li>
                              <li class="d-none d-lg-block">
                                  <a class="btn_1" href="{{route('login')}}">Iniciar Sesión</a>
                              </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- Header part end-->
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
    <!-- banner part start-->
        <div class="whole-wrap">
            <div class="container box_1170">
                <div class="section-top-border">
            <h2>Dudas frecuentes</h2>
                    <h4 >¿Qué diferencia hay entre un Psiquiatra y un Psicólogo?</h4>
                    <div class="row">
                        <div class="col-lg-12">
                            <blockquote class="generic-blockquote" style= "text-align: justify;">
                                El Psiquiatra estudio medicina y además una Especialidad en Psiquiatría, algunos también estudian una subespecialidad en Psiquiatría Infantil y del Adolescente (paidopsiquiatría), por ello pueden hacer diagnósticos médicos y prescribir medicamentos. El psicólogo tiene otras características profesionales basadas en los estudios de especialidad o maestría que realiza; estudia los procesos mentales, el comportamiento, las emociones y las cogniciones, conoce además diferentes modelos terapéuticos y no prescribe medicamentos. Los estudios científicos han mostrado que en muchas ocasiones el trabajo conjunto del médico psiquiatra y el psicólogo clínico brinda los mejores resultados para los pacientes
                            </blockquote>
                        </div>
                    </div>
                    <h4 >¿Cuánto dura la Psicoterapia?</h4>
                    <div class="row">
                        <div class="col-lg-12">
                            <blockquote class="generic-blockquote" style= "text-align: justify;">
                                Los procesos terapéuticos, en general, son de duración indefinida; sin embargo, todos los psicólogos de SMH estamos entrenados con modelos de terapia breve, por lo que, le ofreceremos planes terapéutico de 10 a 15 sesiones
                            </blockquote>
                        </div>
                    </div>
                    <h4 >¿Es segura, realmente me pueden ayudar?</h4>
                    <div class="row">
                        <div class="col-lg-12">
                            <blockquote class="generic-blockquote" style= "text-align: justify;">
                                Definitivamente si, la historia de la psicoterapia ha demostrado que hablar de nuestras vivencias o experiencias favorece su comprensión, su resignificación, el cambio de comportamiento, el alivio emocional, tener perspectivas diferentes, mejora el ánimo, brinda esperanza, en concreto si, la psicoterapia es segura y efectiva, aunque es importante que usted sepa que, al menos el 50% del éxito terapéutico depende de usted
                            </blockquote>
                        </div>
                    </div>
                    <h4 >¿Es fácil hacerlo en línea?</h4>
                    <div class="row">
                        <div class="col-lg-12">
                            <blockquote class="generic-blockquote" style= "text-align: justify;">
                                Es muy sencillo, aun si no sabes usar una computadora, al principio tal vez necesites ayuda, pero después de un par de consultas podrás hacerlo solo o sala. Además podrás hacerlo desde la intimidad tu casa; evitaras la inversión de tiempo, el tránsito y los costos de traslado. La literatura académica refiere que las diferencias entre la psicoterapia presencial y en línea no son significativas
                            </blockquote>
                        </div>
                    </div>
                    <h4 >¿Puede hacer cita un menor de edad?</h4>
                    <div class="row">
                        <div class="col-lg-12">
                            <blockquote class="generic-blockquote" style= "text-align: justify;">
                                La cita tendrá que hacerla un adulto, pero la psicoterapia puede tomarla un adolescente; en el caso de los niños, es recomendable que un adulto los acompañe
                            </blockquote>
                        </div>
                    </div>
                    <h4 >¿Qué problemas técnicos puedo tener?</h4>
                    <div class="row">
                        <div class="col-lg-12">
                            <blockquote class="generic-blockquote" style= "text-align: justify;">
                                La señal de internet, tanto del usuario como del clínico puede presentar fallas de manera inesperada, por una caída de la red, una falla eléctrica u otros motivos antes o durante su consulta, en tales casos la consulta continuara, sin costo adicional, en un segundo momento que podrá programarse nuevamente
                            </blockquote>
                        </div>
                    </div>
                    <h4 >¿Qué limitaciones puede tener la atención en línea?</h4>
                    <div class="row">
                        <div class="col-lg-12">
                            <blockquote class="generic-blockquote" style= "text-align: justify;">
                                Si bien las ventajas de un servicio en línea son enormes, este también conlleva algunas limitaciones: Algunos datos clínicos como movimientos del tronco inferior, tonalidad de la piel en rostro o manos; dilatación de las pupilas, gesticulaciones, son imperceptibles con la modalidad on line; por ello, algunos diagnósticos y tratamientos son complejos, imprecisos o imposibles en línea, por ejemplo no podemos ofrecer la estimulación temprana que su hijo puede necesitar. El Médico Psiquíatra podrá solicitarle estudios de laboratorio o gabinete que Salud Mental Hoy no realiza. Por lo anterior, algunos usuarios podrán requerir de manera adicional la atención personalizada de algún otro profesional de la salud ubicado de manera cercana a su lugar de residencia
                            </blockquote>
                        </div>
                    </div>
                    <h4 >¿Cualquier persona adulta puede solicitar consulta?</h4>
                    <div class="row">
                        <div class="col-lg-12">
                            <blockquote class="generic-blockquote" style= "text-align: justify;">
                                Brindamos atención únicamente a mexicanos que residan en cualquier lugar del mundo, debido a que los padecimientos mentales tienen una importante relación con los aspectos social y cultural; los profesionales de Salud Mental Hoy  compartimos la idiosincrasia propia de la cultura mexicana
                            </blockquote>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!--::blog_part end::-->

    <!-- footer part start-->
    <section class="feature_part single_feature_padding">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-xl-4">
                    <div class="single_feature">
                        <div class="single_feature_part">
                            <span class="single_feature_icon"><i class="ti-layers"></i></span>
                            <h4>SERVICIO INTEGRAL</h4>
                            <p>Funcionamos como una clínica de Salud Mental, todos los Clínicos interactuamos para identificar la mejor opción farmacológica y/o terapéutica para usted. Estamos organizados en 8 Clínicas y 3 Servicios</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xl-4">
                    <div class="single_feature">
                        <div class="single_feature_part">
                            <span class="single_feature_icon"><i class="ti-new-window"></i></span>
                            <h4>SERVICIO PROFESIONAL</h4>
                            <p>Los clínicos de SMH cuentan con amplia experiencia, en promedio atienden o han atendido 1000 pacientes al año en Instituciones de Salud Pública</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xl-4">
                    <div class="single_feature">
                        <div class="single_feature_part single_feature_part_2">
                            <span class="single_service_icon style_icon"><i class="ti-light-bulb"></i></span>
                            <h4>SERVICIO SEGURO</h4>
                            <p>Estamos profundamente comprometidos con su seguridad y con el manejo responsable de su información,  por lo que, en ningún caso solicitaremos la dirección de su domicilio, evitaremos hasta donde sea posible el uso de su teléfono celular y sus datos jamás serán transferidos a otras plataformas</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- footer part end-->

    <!-- jquery plugins here-->
    <!-- jquery -->
    <script src="adminlte/plugins/mental/js/jquery-1.12.1.min.js"></script>
    <!-- popper js -->
    <script src="adminlte/plugins/mental/js/popper.min.js"></script>
    <!-- bootstrap js -->
    <script src="adminlte/plugins/mental/js/bootstrap.min.js"></script>
    <!-- easing js -->
    <script src="adminlte/plugins/mental/js/jquery.magnific-popup.js"></script>
    <!-- swiper js -->
    <script src="adminlte/plugins/mental/js/swiper.min.js"></script>
    <!-- swiper js -->
    <script src="adminlte/plugins/mental/js/masonry.pkgd.js"></script>
    <!-- particles js -->
    <script src="adminlte/plugins/mental/js/owl.carousel.min.js"></script>
    <script src="adminlte/plugins/mental/js/jquery.nice-select.min.js"></script>
    <!-- swiper js -->
    <script src="adminlte/plugins/mental/js/slick.min.js"></script>
    <script src="adminlte/plugins/mental/js/jquery.counterup.min.js"></script>
    <script src="adminlte/plugins/mental/js/waypoints.min.js"></script>
    <!-- custom js -->
    <script src="adminlte/plugins/mental/js/custom.js"></script>
</body>

</html>
