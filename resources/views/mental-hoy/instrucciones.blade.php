<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Etrain</title>
    <link rel="icon" href="adminlte/plugins/mental/img/favicon.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/bootstrap.min.css">
    <!-- animate CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/animate.css">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/owl.carousel.min.css">
    <!-- themify CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/themify-icons.css">
    <!-- flaticon CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/flaticon.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/magnific-popup.css">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/slick.css">
    <!-- style CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <!--::header part start::-->
    <header class="main_menu home_menu">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand" href="/"> <img src="adminlte/plugins/mental/img/logo.png" alt="logo"> </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse main-menu-item justify-content-end"
                            id="navbarSupportedContent">
                            <ul class="navbar-nav align-items-center">
                              <li class="nav-item active">
                                  <a class="nav-link" href="/">Inicio</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="{{route('somos')}}">¿Quiénes somos?</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="{{route('servicios')}}">Clínicas y Servicios</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="{{route('como_funciona')}}">Cómo funciona el Centro</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="{{route('preguntas')}}">Preguntas frecuentes</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="{{route('blog')}}">Blog y Testimonios</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="/">Aviso de Privacidad</a>
                              </li>
                              <li class="d-none d-lg-block">
                                  <a class="btn_1" href="{{route('login')}}">Iniciar sesión</a>
                              </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- Header part end-->

    <!-- banner part start-->
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>

        <div class="container box_1170">
                        <h3 class="mb-20">Ha concluido adecuadamente la solicitud de su cita. Lea cuidadosamente las siguientes:</h3>
<br/>

                        <h5 class="mb-20">INSTRUCCIONES PARA EL PACIENTE</h5>

                        <div class="">
                            <ul class="unordered-list">
                                <li style="font-size: 15px;">Colóquese en un lugar tranquilo, ventilado e iluminado. Evite el bullicio..</li>
<br/>
                                <li style="font-size: 15px;">Es muy importante que usted pueda hablar con la confianza de no ser escuchado por terceras personas.</li>
<br/>
                                <li style="font-size: 15px;">Diseñe el escenario para que su consulta sea cómoda para usted.</li>
<br/>
                                <li style="font-size: 15px;">Lo anterior es muy importante para que su consulta tenga la calidad que usted busca.</li>
<br/>
                                <li style="font-size: 15px;">Ingresé al Centro con su usuario y contraseña, presione: "Consultas on line", se desplegará la agenda de ese día en donde podrá encontrar su nombre, hora y Profesional elegido. Presione ahí y habilite su cámara web. Espere algunos minutos en caso necesario.</li>

                            </ul>
                        </div>
                    </div>
            <div class="container box_1170">
<br/>

            <p class="sample-text" style="font-size: 15px;">
               Posterior a su cita usted tiene la posibilidad de contactar nuevamente a su médico SIN COSTO EXTRA, una sola vez a través del chat, para resolver alguna duda que haya olvidado preguntar durante su consulta.
<br/><b>IMPORTANTE</b> Salud Mental Hoy, evitara usar su número telefónico, cualquier contacto con usted lo haremos a través de su correo electrónico, por favor revíselo con frecuencia y en particular el día de su cita.
</p>
        </div>

        </div>
        </div>
<br/>
<br/>

<div class="container box_1170">
            <h3 class="text-heading">Politicas de Cancelación</h3>
            <p class="sample-text" style="font-size: 15px;">
                Los imprevistos son posibles, en el remoto caso que su terapeuta no llegara, por favor presionen aquí "cita cancelada por el Profesional", para que le sea reembolsado su depósito y usted podrá reprogramar su consulta on line.
<br/>
<br/>
Usted tiene 20 minutos de tolerancia, en caso de no llegar a su consulta online, perderá su depósito.
<br/>
<br/>
En caso de que su cita no pueda concluir debido a alguna falla técnica en el inetrnet, por favor solicite la programación de la continuación de su consulta, indicando su nombre, fecha, hora y clínico que la estaba atendiendo.
<br/>
<br/>
Usted puede posponer su consulta on line hasta con 24 horas de anticipación para no perderla y poder reprogramar su cita
</p>
        </div>    <!-- Start Align Area -->
    <div class="whole-wrap">

    </div>
    <!-- End Align Area -->
<br/>
<br/>
<br/>

    <!-- footer part start-->
    <div class="container">
            <div class="row">
                <div class="col-xs-4 col-sm-4 col-xl-4">
                    <div class="learning_member_text">
                       <p style= "text-align: justify;">Si usted sufre <b>ideas de suicidio,</b> debe saber que en su primer consulta, el clínico le pedirá un número de contacto de algún familiar como requisito para recibir tratamiento.
                        </p>
                    </div>
                </div>
                <div class="col-xs-2 col-sm-2 col-xl-2">
                    <div class="learning_member_text">
                        <h5><i class="fa fa-exclamation-triangle"></i> <b>¡Atención!</b></h5>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-xl-6">
                    <div class="learning_member_text">
                       <p style= "text-align: justify;">Si se encuentra en una crisis que le pone a usted o a otra
                            persona en peligro <b>NO USE ESTE SITIO.</b> Llame al 911 en
                            México o a los números de emergencia de su lugar de
                            residenciao a SAPTEL Sistema Nacional de Apoyo,
                            Consejo Psicológico e Intervención en Crisis (55) 52
                            59-8121 Servicio Gratuito las 24 hrs. <a href="http://www.saptel.org.mx/" class="genric-btn success radius">SAPTEL</a>
                        </p>
                    </div>
                </div>
            </div>
            <br/>
            <br/>
            <br/>
        </div>
    <!-- footer part end-->

    <!-- jquery plugins here-->
    <!-- jquery -->
    <script src="adminlte/plugins/mental/js/jquery-1.12.1.min.js"></script>
    <!-- popper js -->
    <script src="adminlte/plugins/mental/js/popper.min.js"></script>
    <!-- bootstrap js -->
    <script src="adminlte/plugins/mental/js/bootstrap.min.js"></script>
    <!-- easing js -->
    <script src="adminlte/plugins/mental/js/jquery.magnific-popup.js"></script>
    <!-- swiper js -->
    <script src="adminlte/plugins/mental/js/swiper.min.js"></script>
    <!-- swiper js -->
    <script src="adminlte/plugins/mental/js/masonry.pkgd.js"></script>
    <!-- particles js -->
    <script src="adminlte/plugins/mental/js/owl.carousel.min.js"></script>
    <script src="adminlte/plugins/mental/js/jquery.nice-select.min.js"></script>
    <!-- swiper js -->
    <script src="adminlte/plugins/mental/js/slick.min.js"></script>
    <script src="adminlte/plugins/mental/js/jquery.counterup.min.js"></script>
    <script src="adminlte/plugins/mental/js/waypoints.min.js"></script>
    <!-- custom js -->
    <script src="adminlte/plugins/mental/js/custom.js"></script>
    <script src="adminlte/plugins/sweetalert2/sweetalert2.all.min.js"></script>
    <script type="text/javascript">
    Swal.fire(
      'Su cita ha sido confirmada gracias por confiar en Salud Menta Hoy',
      'Presione ok para contiuar!',
      'success'
    );
    </script>
</body>

</html>
