<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Etrain</title>
    <link rel="icon" href="adminlte/plugins/mental/img/favicon.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/bootstrap.min.css">
    <!-- animate CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/animate.css">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/owl.carousel.min.css">
    <!-- themify CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/themify-icons.css">
    <!-- flaticon CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/flaticon.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/magnific-popup.css">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/slick.css">
    <!-- style CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <!--::header part start::-->
    <header class="main_menu home_menu">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand" href="/"> <img src="adminlte/plugins/mental/img/logo.png" alt="logo"> </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse main-menu-item justify-content-end"
                            id="navbarSupportedContent">
                            <ul class="navbar-nav align-items-center">
                                <li class="nav-item active">
                                    <a class="nav-link" href="/">Inicio</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('somos')}}">¿Quiénes somos?</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('servicios')}}">Clínicas y Servicios</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('como_funciona')}}">Cómo funciona el Centro</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('preguntas')}}">Preguntas frecuentes</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('blog')}}">Blog y Testimonios</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/">Aviso de Privacidad</a>
                                </li>
                                <li class="d-none d-lg-block">
                                    <a class="btn_1" href="{{route('login')}}">Iniciar Sesión</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- Header part end-->

    <!-- banner part start-->
    <section class="course_details_area section_padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 course_details_left">
                    <a href="javascript: history.go(-1)" class="genric-btn success radius">Regresar</a>
                    <div class="content_wrapper">
                        <h4 class="title_top">Servicio de Terapia Familiar y de Pareja</h4>
                        <div class="content" style= "text-align: justify;">
                            Se trata de un método particular de terapia psicológica que ayuda a resolver situaciones que afectan el funcionamiento de la familia y la vida emocional de sus miembros; favorece la comunicación y la solución de conflictos. Usted puede solicitar una terapia familiar cuando las relaciones entre los miembros de su familia son lastimosas o dañinas y no permiten el crecimiento personal; busque ayuda cuando las relaciones familiares generen dolor emocional e incluso la manifestación de síntomas o trastornos en alguno de sus miembros.
La terapia familiar está indicada para el tratamiento de los trastornos emocionales y del comportamiento.

                        </div>
                    </div>
                    <br>
                    <div class="content_wrapper">

                        <div class="content" style= "text-align: justify;">
                          <p><img src="adminlte/plugins/mental/img/familiar.png" alt="#" style="width:200px; float:left; padding-right: 15px;"><h5>¿Cuándo acude una pareja a terapia? </h5>
Cuando alguno de sus miembros se siente infeliz o insatisfecho, molesto o decepcionado y ya no encuentra formas de solucionar el problema, incluso no encuentra forma de comunicarse con el otro. Lo anterior puede ser detonado por problemas financieros, relaciones con las familias de origen, falta de acuerdos, expectativas diferentes, formas de pensar y actuar, crisis derivadas de enfermedades, muerte de una persona cercana, infidelidad, entre muchas otras. Por ello, es muy  conveniente acudir a terapia de pareja antes de que una situación se convierta en un problema  y antes que un problema se convierta en una crisis, que pueda derivar en un trastorno emocional o del comportamiento

                          </p></div>
                </div>
                                <div class="row">

                <div class=" comments-area mb-30 col-lg-6 col-md-6 col-xs-12">
                    <div class="comment-list">
                                <div class="single-comment single-reviews justify-content-between d-flex">
                                    <div class="user justify-content-between d-flex">
                                        <div class="thumb">
                                            <img src="adminlte/plugins/mental/img/nancy.png" alt="">
                                        </div>
                                        <div class="desc">
                                            <h5><a href="{{route('nancy')}}">Nancy Verónica Vargas Gama</a>
                                            </h5>
                                            <div class="rating">
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/star.svg" alt=""></a>
                                            </div>
                                            <p class="comment">
                                                Ranking: 4.1
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                </div>

                </div>
                </div>


                <div class="col-lg-4 right-contents">

                    <h4 class="title">Clínicos</h4>
                    <div class="content">
                        <div class="comments-area mb-30">
                            <div class="comment-list">
                                <div class="single-comment single-reviews justify-content-between d-flex">
                                    <div class="user justify-content-between d-flex">
                                        <div class="thumb">
                                            <img src="adminlte/plugins/mental/img/liliana.png" alt="">
                                        </div>
                                        <div class="desc">
                                            <h5><a href="{{route('liliana')}}">Dra. Alma Liliana González Pulido</a>
                                            </h5>
                                            <div class="rating">
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/star.svg" alt=""></a>
                                            </div>
                                            <p class="comment">
                                                Ranking: 4.1
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="comment-list">
                                <div class="single-comment single-reviews justify-content-between d-flex">
                                    <div class="user justify-content-between d-flex">
                                        <div class="thumb">
                                            <img src="adminlte/plugins/mental/img/america.png" alt="">
                                        </div>
                                        <div class="desc">
                                            <h5><a href="{{route('america')}}">Psic. Lizette América Roman Toledo</a>
                                            </h5>
                                            <div class="rating">
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/star.svg" alt=""></a>
                                            </div>
                                            <p class="comment">
                                                Ranking: 4.1
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="comment-list">
                                <div class="single-comment single-reviews justify-content-between d-flex">
                                    <div class="user justify-content-between d-flex">
                                        <div class="thumb">
                                            <img src="adminlte/plugins/mental/img/ruth.png" alt="">
                                        </div>
                                        <div class="desc">
                                            <h5><a href="{{route('ruth')}}">Psic. Ruth Violeta Ruiz Hurtado</a>
                                            </h5>
                                            <div class="rating">
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/star.svg" alt=""></a>
                                            </div>
                                            <p class="comment">
                                                Ranking: 4.1
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="comment-list">
                                <div class="single-comment single-reviews justify-content-between d-flex">
                                    <div class="user justify-content-between d-flex">
                                        <div class="thumb">
                                            <img src="adminlte/plugins/mental/img/paula.png" alt="">
                                        </div>
                                        <div class="desc">
                                            <h5><a href="{{route('paula')}}">Psic. Paula Catalina Pérez Carrillo</a>
                                            </h5>
                                            <div class="rating">
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/color_star.svg" alt=""></a>
                                                <a href="#"><img src="adminlte/plugins/mental/img/icon/star.svg" alt=""></a>
                                            </div>
                                            <p class="comment">
                                                Ranking: 4.1
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!--::blog_part end::-->

    <!-- footer part start-->
    <section class="feature_part single_feature_padding">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-xl-4">
                    <div class="single_feature">
                        <div class="single_feature_part">
                            <span class="single_feature_icon"><i class="ti-layers"></i></span>
                            <h4>SERVICIO INTEGRAL</h4>
                            <p>Funcionamos como una clínica de Salud Mental, todos los Clínicos interactuamos para identificar la mejor opción farmacológica y/o terapéutica para usted. Estamos organizados en 8 Clínicas y 3 Servicios</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xl-4">
                    <div class="single_feature">
                        <div class="single_feature_part">
                            <span class="single_feature_icon"><i class="ti-new-window"></i></span>
                            <h4>SERVICIO PROFESIONAL</h4>
                            <p>Los clínicos de SMH cuentan con amplia experiencia, en promedio atienden o han atendido 1000 pacientes al año en Instituciones de Salud Pública</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xl-4">
                    <div class="single_feature">
                        <div class="single_feature_part single_feature_part_2">
                            <span class="single_service_icon style_icon"><i class="ti-light-bulb"></i></span>
                            <h4>SERVICIO SEGURO</h4>
                            <p>Estamos profundamente comprometidos con su seguridad y con el manejo responsable de su información,  por lo que, en ningún caso solicitaremos la dirección de su domicilio, evitaremos hasta donde sea posible el uso de su teléfono celular y sus datos jamás serán transferidos a otras plataformas</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- footer part end-->

    <!-- jquery plugins here-->
    <!-- jquery -->
    <script src="adminlte/plugins/mental/js/jquery-1.12.1.min.js"></script>
    <!-- popper js -->
    <script src="adminlte/plugins/mental/js/popper.min.js"></script>
    <!-- bootstrap js -->
    <script src="adminlte/plugins/mental/js/bootstrap.min.js"></script>
    <!-- easing js -->
    <script src="adminlte/plugins/mental/js/jquery.magnific-popup.js"></script>
    <!-- swiper js -->
    <script src="adminlte/plugins/mental/js/swiper.min.js"></script>
    <!-- swiper js -->
    <script src="adminlte/plugins/mental/js/masonry.pkgd.js"></script>
    <!-- particles js -->
    <script src="adminlte/plugins/mental/js/owl.carousel.min.js"></script>
    <script src="adminlte/plugins/mental/js/jquery.nice-select.min.js"></script>
    <!-- swiper js -->
    <script src="adminlte/plugins/mental/js/slick.min.js"></script>
    <script src="adminlte/plugins/mental/js/jquery.counterup.min.js"></script>
    <script src="adminlte/plugins/mental/js/waypoints.min.js"></script>
    <!-- custom js -->
    <script src="adminlte/plugins/mental/js/custom.js"></script>
</body>

</html>
