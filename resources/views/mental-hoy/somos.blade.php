<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Etrain</title>
    <link rel="icon" href="adminlte/plugins/mental/img/favicon.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/bootstrap.min.css">
    <!-- animate CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/animate.css">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/owl.carousel.min.css">
    <!-- themify CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/themify-icons.css">
    <!-- flaticon CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/flaticon.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/magnific-popup.css">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/slick.css">
    <!-- style CSS -->
    <link rel="stylesheet" href="adminlte/plugins/mental/css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <!--::header part start::-->
    <header class="main_menu home_menu">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand" href="/"> <img src="adminlte/plugins/mental/img/logo.png" alt="logo"> </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse main-menu-item justify-content-end"
                            id="navbarSupportedContent">
                            <ul class="navbar-nav align-items-center">
                              <li class="nav-item active">
                                  <a class="nav-link" href="/">Inicio</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="{{route('somos')}}">¿Quiénes somos?</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="{{route('servicios')}}">Clínicas y Servicios</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="{{route('como_funciona')}}">Cómo funciona el Centro</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="{{route('preguntas')}}">Preguntas frecuentes</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="{{route('blog')}}">Blog y Testimonios</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="/">Aviso de Privacidad</a>
                              </li>
                              <li class="d-none d-lg-block">
                                  <a class="btn_1" href="{{route('login')}}">Iniciar Sesión</a>
                              </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- Header part end-->

    <!-- banner part start-->
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <div class="whole-wrap">
        <div class="container box_1170">
            <div class="section-top-border">
                <h3 class="mb-30">¿Qué es Salud Mental Hoy?</h3>
                <div class="row">
                    <div class="col-lg-12">
                        <blockquote class="generic-blockquote">
                            SMH es el primer sitio web que funciona con la modalidad de una <b>Clínica Virtual de Salud Mental,</b> para mexicanos que necesitan una opinión, diagnóstico y/o tratamiento  médico o psicológico rápido, cómodo y seguro en español en cualquier lugar del mundo en que se encuentren.
                        </blockquote>
                    </div>
                </div>
            </div>
    <!-- End Sample Area -->
            <div class="section-top-border">
                <div class="row">
                    <div class="col-md-4">
                        <div class="single-defination" style= "text-align: justify;">
                            <h4 class="mb-20">Propósito</h4>
                            <p>Acercar servicios profesionales de calidad en materia de salud mental a los mexicanos en cualquier parte del mundo.</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="single-defination" style= "text-align: justify;">
                            <h4 class="mb-20">Misión</h4>
                            <p>Trabajar para ser un servicio altamente calificado y recomendado en México,  brindando atención clínica de calidad a personas previamente diagnosticadas con un trastorno mental o con dudas acerca de su comportamiento, manifestaciones emocionales o cognitivas.</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="single-defination" style= "text-align: justify;">
                            <h4 class="mb-20">Visión</h4>
                            <p>Contribuir en el bienestar de los mexicanos favoreciendo estados de salud mental, independientemente del lugar en que se encuentren, por medio de un sistema de consultas on line sencillo, eficiente y disponible.</p>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            </div>
                <section class="sample-text-area">
        <div class="container box_1170">
            <h3 class="text-heading">¿Quiénes somos los clínicos?</h3>
            <p class="sample-text">
                Un grupo de expertos en salud mental que conocemos el problema de salud pública que representan los trastornos mentales, que sabemos de la dificultad que tiene la población para encontrar de manera accesible a Psiquiatras y Psicólogos Especialistas y que estamos comprometidos para contribuir a mejorar la calidad de vida de los mexicanos.</p>
        </div>
    </section>
    <!-- Start Align Area -->
    <div class="whole-wrap">
        <div class="container box_1170">
            <div class="section-top-border">
                <h3 class="mb-30">Nuestro Objetivo</h3>
                <div class="row">
                    <div class="col-md-3">
                        <img src="adminlte/plugins/mental/img/elements/objetivo.jpg" alt="" class="img-fluid">
                    </div>
                    <div class="col-md-9 mt-sm-20">
                        <p>Ayudar a las personas a resolver sus dudas acerca de los trastornos mentales más comunes: Depresión, Ansiedad, Adicciones, Anorexia, Problemas de la Adolescencia y la Infancia, Problemas de Aprendizaje, Alzheimer, Esquizofrenia, de manera pronta y en tiempo real en cualquier lugar del mundo en que se encuentren y cuando el sistema on line lo permita, ofrecer un tratamiento farmacológico y psicológico.</p>
                    </div>
                </div>
            </div>
            <div class="section-top-border text-right">
                <h3 class="mb-30">Nuestros Alcances</h3>
                <div class="row">
                    <div class="col-md-9">
                        <p class="text-right">Un grupo de expertos en Salud Mental acreditados estará a su alcance en tiempo real por medio de una consulta on line, en todos los casos le ofrecerán una hipótesis diagnóstica y la asesoría necesaria y en muchos otros, cuando el sistema on line lo permita, podrá recibir un tratamiento farmacológico y psicológico.</p>
                    </div>
                    <div class="col-md-3">
                        <img src="adminlte/plugins/mental/img/elements/alcances.jpg" alt="" class="img-fluid">
                    </div>
                </div>
            </div>
            <div class="section-top-border">
                <div class="row">
                    <div class="col-md-4">
                        <div class="single-defination" style= "text-align: justify;">
                            <h4 class="mb-20">Honestidad</h4>
                            <p>Un grupo de expertos conocedores del problema de Salud Pública, de los costos y la discapacidad que generan los trastornos mentales mal atendidos o no atendidos a tiempo, le ayudara a comprender la diferencia entre los problemas normales de la vida cotidiana y una enfermedad mental, le ofrecerá sus servicios estrictamente solo cuando usted los necesite y le otorgara la mejor opción para su padecimiento de acuerdo a las posibilidades on line.</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="single-defination" style= "text-align: justify;">
                            <h4 class="mb-20">Accesibilidad</h4>
                            <p>Ya no tendrá que hacer traslados de una o dos horas para recibir atención, desde su hogar podrá encontrar el servicio que requiere de acuerdo a los síntomas o problema que tenga; nuestra plataforma está disponible para que agende su cita los 365 días del año.</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="single-defination" style= "text-align: justify;">
                            <h4 class="mb-20">Calidad</h4>
                            <p>El equipo de SMH está acreditado, tiene amplia experiencia clínica, en promedio atienden o han atendido entre 800 y 1000 pacientes al año en Instituciones de Salud Pública y se capacita continuamente para ofrecerle la atención profesional que usted necesita. Puede conocer su semblanza curricular antes de hacer una cita.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Align Area -->

    <!-- footer part start-->
    <div class="container">
            <div class="row">
                <div class="col-xs-4 col-sm-4 col-xl-4">
                    <div class="learning_member_text">
                       <p style= "text-align: justify;">Si usted sufre <b>ideas de suicidio,</b> debe saber que en su primer consulta, el clínico le pedirá un número de contacto de algún familiar como requisito para recibir tratamiento.
                        </p>
                    </div>
                </div>
                <div class="col-xs-2 col-sm-2 col-xl-2">
                    <div class="learning_member_text">
                        <h5><i class="fa fa-exclamation-triangle"></i> <b>¡Atención!</b></h5>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-xl-6">
                    <div class="learning_member_text">
                       <p style= "text-align: justify;">Si se encuentra en una crisis que le pone a usted o a otra
                            persona en peligro <b>NO USE ESTE SITIO.</b> Llame al 911 en
                            México o a los números de emergencia de su lugar de
                            residenciao a SAPTEL Sistema Nacional de Apoyo,
                            Consejo Psicológico e Intervención en Crisis (55) 52
                            59-8121 Servicio Gratuito las 24 hrs. <a href="http://www.saptel.org.mx/" class="genric-btn success radius">SAPTEL</a>
                        </p>
                    </div>
                </div>
            </div>
            <br/>
            <br/>
            <br/>
        </div>
    <!-- footer part end-->

    <!-- jquery plugins here-->
    <!-- jquery -->
    <script src="adminlte/plugins/mental/js/jquery-1.12.1.min.js"></script>
    <!-- popper js -->
    <script src="adminlte/plugins/mental/js/popper.min.js"></script>
    <!-- bootstrap js -->
    <script src="adminlte/plugins/mental/js/bootstrap.min.js"></script>
    <!-- easing js -->
    <script src="adminlte/plugins/mental/js/jquery.magnific-popup.js"></script>
    <!-- swiper js -->
    <script src="adminlte/plugins/mental/js/swiper.min.js"></script>
    <!-- swiper js -->
    <script src="adminlte/plugins/mental/js/masonry.pkgd.js"></script>
    <!-- particles js -->
    <script src="adminlte/plugins/mental/js/owl.carousel.min.js"></script>
    <script src="adminlte/plugins/mental/js/jquery.nice-select.min.js"></script>
    <!-- swiper js -->
    <script src="adminlte/plugins/mental/js/slick.min.js"></script>
    <script src="adminlte/plugins/mental/js/jquery.counterup.min.js"></script>
    <script src="adminlte/plugins/mental/js/waypoints.min.js"></script>
    <!-- custom js -->
    <script src="adminlte/plugins/mental/js/custom.js"></script>
</body>

</html>
