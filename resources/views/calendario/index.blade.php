@extends('admin.layout')
@push('stylesheets')
  <link rel="stylesheet" href="adminlte/fullcalendar/css/fullcalendar.min.css">
@endpush
@section('header-text')
  <h1>Dashboard</h1>
@endsection
@section('main')

  <div class="col-md-12">
              <div class="card card-primary">
                <div class="card-body p-0">
                  <!-- THE CALENDAR -->
                  <div id="calendar"></div>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
@endsection
@push('scripts')
  <script src="adminlte/fullcalendar/js/moment.min.js"></script>
  <script src="adminlte/fullcalendar/js/fullcalendar.min.js"></script>
  <script src="adminlte/fullcalendar/js/locale-all.js"></script>

@endpush
@section('script_page')
<script type="text/javascript">
$('#calendar').fullCalendar({
  dayClick: function(date, jsEvent, view) {
    $('#calendar').fullCalendar('gotoDate',date);
    $('#calendar').fullCalendar('changeView','agendaDay');
  },
          // put your options and callbacks here
          events : [

          ]
      })
</script>
@endsection
